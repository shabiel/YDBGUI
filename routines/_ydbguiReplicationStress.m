%ydbguiReplicationStress ; 
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	quit
	;
backlogStress(upperRange,repeat,repeatHangMs)
	new x,y
	;
	set repeat=$get(repeat,1)
	set repeatHangMs=$get(repeatHangMs,100)
	set upperRange=$get(upperRange,1E6)
	;
	for y=1:1:repeat do
	. for x=1:1:upperRange set ^test(x)=x
	. hang repeatHangMs
	;
	quit
	;
	;
