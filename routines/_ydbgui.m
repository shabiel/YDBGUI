%ydbgui ; Start-up code
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
start	; [Public, Default] Blocking entry point
	do setzd
	do start^%ydbwebreq(.options)
	quit
	;
stop	; [Public] Stop Web Server
	do stop^%ydbwebreq(.options)
	quit
	;
setzd	; [Private] Set default directory for static files
	; If current directory has an index.html, use that; otherwise,
	; switch to $ydb_dist/plugin/etc/ydbgui
	if $zsearch("index.html")'="" quit  ; Leave $zdirectory to the current directory
	new runtimeDir set runtimeDir=$zsearch("$ydb_dist/plugin/etc/ydbgui")
	if runtimeDir="" write "Static files not found; contact your YottaDB support channel",! set $ecode=",U999,"
	set options("directory")=runtimeDir
	quit
	;
