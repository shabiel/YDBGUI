%ydbguiWsServer ; Octo code
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	;
	quit
	;
start(logging)
	new res,ret,shellResult,PID,ix,ydbDist,nodePath,port
	new device,command,return,counter,string,currentDevice,counter
	new timeout,level,hangTime,tlsInfo
	new tmpPath,outLog
	;
	set level=$zlevel,device=""
	; first check if it is already running
	set ret=$$runShell^%ydbguiUtils("ps -C ydbgui-ws-server -F",.shellResult)
	if ret'=0,ret'=1 do  goto startQuit
	. set res("result")="ERROR"
	. set res("error","description")="Could not execute the find_node_process. Error is: "_ret
	. set res("error","code")=-1
	;
	if $find($get(shellResult(2)),"ydbgui-ws-server")'=0 do  goto startQuit
	. set res("result")="WARNING"
	. set res("error","description")="Already running"
	. set res("error","code")=1
	;
	set ydbDist=$ztrnlnm("ydb_dist")
	set nodePath=httpoptions("directory")
	if $zextract(nodePath,$zlength(nodePath))'="/" set nodePath=nodePath_"/"
	;
	set port=+httpoptions("port")+1
	if httpoptions("ws-port")>0 set port=httpoptions("ws-port")
	;
	; get the TLS info if needed
	set tlsInfo=""
	if httpoptions("tlsconfig")'="" do
	. set tlsInfo=$$getTlsInfo()
	. view "setenv":"ydbguiServerInfo":$zpiece(tlsInfo,"~",1)
	. set tlsInfo=$zpiece(tlsInfo,"~",2,3)
	;
	; compute path and filename for log files
	set tmpPath=$ZTRNLNM("ydb_tmp")
	set:tmpPath="" tmpPath="/tmp/"
	if $zextract(tmpPath,$zlength(tmpPath),$zlength(tmpPath))'="/" set tmpPath=tmpPath_"/"
	set outLog=tmpPath_"ydbgui-ws-server_"_$zut_".out"
	;
	; run the web socket server
	set currentDevice=$io
	set device="runshellcommmandpipe"_$job
	set command="node "_nodePath_"node/startup --tls_info="_tlsInfo_" --port="_port_" --ydb_dist="_ydbDist_$select(logging>0:" --logging="_tmpPath,1:"")_" > "_outLog
	open device:(shell="/bin/sh":command=command:readonly:independent)::"pipe"
	use device
	set PID=$key
	close device
	use currentDevice
	;
	set device=outLog
	set timeout=5000	; five seconds timeout
	set hangTime=50		; 50 ms hang
	;
tryAgain
	hang hangTime/1000
	set timeout=timeout-hangTime
	; detect timeout
	if timeout<=0 do  goto startQuit
	. ; timeout error
	. set res("result")="ERROR"
	. set res("error","description")="Timeout: could not get a response in a timely fashion from the server. See dump for details."
	. merge res("error","dump")=shellResult
	;
	; Read out file
	kill shellResult
	set counter=0
	open device use device
	for  quit:$zeof=1  read string set shellResult($increment(counter))=string
	close device
	;
	; Parse result
	if $data(shellResult)>9 do
	. ; all good
	. set shellResult=$get(shellResult($order(shellResult(""),-1)-1))
	. if $zfind(shellResult,"socket created") do  zgoto level:startQuit
	. . set res("result")="OK"
	. . set res("data","PID")=PID
	. . set res("data","wsPort")=port
	. ; port already in use
	. if $zfind(shellResult,"address already in use") do  zgoto level:startQuit
	. . set res("result")="ERROR"
	. . set res("error","description")="TCP port already in use."
	;
	goto tryAgain
	;
startQuit
	; delete the .out file
	if device'="" open device use device close device:delete
	;
	view "setenv":"ydbguiServerInfo":""
	;
	quit *res
	;
	;
getTlsInfo()
	new offpwd,binobfpwd,clrpwds,tls,ret
	new cfgFile,config,configCnt,sectionName,line,sectionFound
	;
	; get password
	view "setenv":"ydb_xc_gpgagent":$zsearch("$ydb_dist")_"/plugin/gpgagent.tab"
	;
	set (binobfpwd,clrpwds)=""
	set obfpwd=$ZTRNLNM("ydb_tls_passwd_ydbgui")
	for i=1:2:$zlength(obfpwd) do
	. set msb=$zfind("0123456789ABCDEF",$zextract(obfpwd,i))-2
	. set lsb=$zfind("0123456789ABCDEF",$zextract(obfpwd,i+1))-2
	. set binobfpwd=binobfpwd_$zchar(16*msb+lsb)
	if $&gpgagent.unmaskpwd(binobfpwd,.clrpwds) set tls("status")="error" goto getTlsInfoQuit
	set tls("password")=clrpwds
	;
	; find config file and read it
	set cfgFile=$ZTRNLNM("ydb_crypt_config"),configCnt=0
	open cfgFile:readonly
	use cfgFile
	for  read config($increment(configCnt)) quit:$zeof
	close cfgFile
	;
	; parse it to extract the files
	set sectionName=httpoptions("tlsconfig"),sectionFound=0
	set configCnt="" for  set configCnt=$order(config(configCnt)) quit:configCnt=""  do
	. set line=config(configCnt)
	. if $zfind(line,sectionName_":")=0,sectionFound=0 quit
	. else  set sectionFound=1
	. if $zfind(line,"cert:") set tls("cert")=$zpiece(line," """,2),tls("cert")=$zextract(tls("cert"),1,$zlength(tls("cert"))-2) quit
	. if $zfind(line,"key:") set tls("key")=$zpiece(line," """,2),tls("key")=$zextract(tls("key"),1,$zlength(tls("key"))-2) quit
	;
getTlsInfoQuit
	quit tls("password")_"~"_tls("cert")_"~"_tls("key")
