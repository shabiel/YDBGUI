#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of          #
#    the license, please stop and do not read further.          #
#                                                               #
#################################################################
name: 'yottadb-replication'

services:
  ####################
  # MELBOURNE
  ####################
  melbourne:
    profiles:
      - bc1
      - bc2
      - bc7
      - bc2bc1
      - bc2bc4
      - bc2si1si1
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/melbourne/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'melbourne'
    container_name: 'melbourne'
    expose:
      - "3000"
    ports:
      - "8089:8089"
      - "8090:8090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.2
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # PARIS
  ####################
  paris:
    profiles:
      - bc1
      - bc2
      - bc7
      - bc2bc1
      - bc2bc4
      - bc2si1si1
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/paris/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'paris'
    container_name: 'paris'
    depends_on:
      - melbourne
    expose:
      - "3001"
    ports:
      - "9089:9089"
      - "9090:9090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.3
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # SANTIAGO
  ####################
  santiago:
    profiles:
      - bc2
      - bc7
      - bc2bc1
      - bc2bc4
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/santiago/Dockerfile
    image: 'santiago'
    container_name: 'santiago'
    expose:
      - "3002"
    ports:
      - "10089:10089"
      - "10090:10090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.4
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # ROME
  ####################
  rome:
    profiles:
      - bc7
      - bc2bc1
      - bc2bc4
      - bc2si1si1
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/rome/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'rome'
    container_name: 'rome'
    depends_on:
      - paris
    expose:
      - "3003"
    ports:
      - "11089:11089"
      - "11090:11090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.5
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # AMSTERDAM
  ####################
  amsterdam:
    profiles:
      - bc7
      - bc2bc4
      - bc2si1si1
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/amsterdam/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'amsterdam'
    container_name: 'amsterdam'
    depends_on:
      - paris
    expose:
      - "3004"
    ports:
      - "12089:12089"
      - "12090:12090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.6
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # LONDON
  ####################
  london:
    profiles:
      - bc7
      - bc2bc4
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/london/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'london'
    depends_on:
      - paris
    container_name: 'london'
    expose:
      - "3005"
    ports:
      - "13089:13089"
      - "13090:13090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.7
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # TOKIO
  ####################
  tokio:
    profiles:
      - bc7
      - bc2bc4
      - bc2si4
    build:
      context: ./
      dockerfile: ./replication/tokio/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'tokio'
    depends_on:
      - paris
    container_name: 'tokio'
    expose:
      - "3006"
    ports:
      - "14089:14089"
      - "14090:14090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.8
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

  ####################
  # MADRID
  ####################
  madrid:
    profiles:
      - bc7
    build:
      context: ./
      dockerfile: ./replication/madrid/Dockerfile
      args:
        profile: ${profile}
        no_cache: true
    image: 'madrid'
    container_name: 'madrid'
    expose:
      - "3007"
    ports:
      - "15089:15089"
      - "15090:15090"
    networks:
      replNetwork:
        ipv4_address: 10.5.0.9
    volumes:
      - ./wwwroot:/YDBGUI/wwwroot:rw
      - ./routines:/YDBGUI/routines:rw
      - ./replication/backups:/repl/backups:rw

networks:
  replNetwork:
    ipam:
      driver: default
      config:
        - subnet: 10.5.0.0/16
          gateway: 10.5.0.1

# to run this file, use YDBGUI/replication/repl
