<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# REST Endpoints 

## Overview

| Method | Endpoint                                                                           | Params           | Description                                                                                       |
|--------|------------------------------------------------------------------------------------|------------------|---------------------------------------------------------------------------------------------------|
| GET    | [api/dashbboard/get-all](#get-apidashbboardgetall)                                 |                  | Retrieves full info on all regions, devices and system                                            |
| GET    | [api/regions/{region}](#get-apiregionsregion)                                      |                  | Retieves info about a specific region                                                             |
| DELETE | [api/regions/{region}](#get-apiregionsregion)                                      | deleteFiles=true | Deletes a specific region                                                                         |
| POST   | [api/regions/{region}/create-db](#post-apiregionsregioncreatedb)                   |                  | Creates a database in the specified region                                                        |
| POST   | [api/regions/{region}/extend](#post-apiregionsregionextend)                        | blocks=_nnn_     | Extends the specified region for the given amount of blocks                                       |
| POST   | [api/regions/{region}/journal-switch](#post-apiregionsregionjournalswitch)         | mode=_on_  _off_ | Switches the journal `on` or `off` in the given region                                            |
| GET    | [api/dashboard/get-templates](#get-apidashbboardgettemplates)                      |                  | Returns the GDE templates and their numeric limits                                                |
| POST   | [api/regions/parse-namespace](#post-apiregionsparsenamespace)                      |                  | Verify a namespace                                                                                |
| POST   | [api/regions/validate-path](#post-apiregionsvalidatepath)                          |                  | Validate a filepath                                                                               |
| POST   | [api/regions/add](#post-apiregionsadd)                                             |                  | Creates a region                                                                                  |
| POST   | [api/regions/{region}/edit](#post-apiregionsregionedit)                            |                  | Edits a region                                                                                    |
| GET    | [api/test/error](#get-apitesterror)                                                |                  | Triggers an M error and returns the error description in the status 500 response                  |
| POST   | [api/test/error](#post-apitesterror)                                               |                  | Triggers an M error and returns the error description in the status 500 response                  |
| DELETE | [api/test/error](#delete-apitesterror)                                             |                  | Triggers an M error and returns the error description in the status 500 response                  |
| GET    | [api/regions/locks/get-all](#get-apiregionslocksgetall)                            |                  | Returns all the locks and waiters across the all regions                                          |
| POST   | [api/regions/locks/clear](#post-apiregionslocksclear)                              | namespace=       | Clear the lock specified in the "namespace" parameter                                             |
| POST   | [api/os/processes/{pid}/terminate](#post-apiosprocessespidterminate)               | {pid}            | Terminate the process specified in the "pid" parameter using the SIGINT signal                    |
| GET    | [api/globals/dollar-data](#get-apiglobalsdollardata)                               | globalName=      | Returns the $data value of a global                                                               |
| POST   | [api/globals/get-data](#get-apiglobalsgetdata)                                     |                  | Returns the value of a global                                                                     |
| GET    | [api/globals/find](#get-apiglobalsfind)                                            | mask= &size=     | Query the globals against a mask                                                                  |
| POST   | [api/globals/traverse](#post-apiglobalstraverse)                                   |                  | Traverse a global with paramteres passed in the body section                                      |
| POST   | [api/routines/find](#get-apiroutinesfind)                                          |                  | Query the routines against a mask                                                                 |
| POST   | [api/routines/{routine}](#get-routinesroutine)                                     |                  | Gets a routine                                                                                    |
| GET    | [api/server/mode](#get-servermode)                                                 |                  | Returns the current server mode (RO or RW)                                                        |
| POST   | [api/regions/maintenance/backup](#post-regionsmaintenancebackup)                   |                  | Performs a BACKUP operation on one or more regions                                                |
| POST   | [api/regions/maintenance/defrag](#post-regionsmaintenancedefrag)                   |                  | Performs a DEFRAG operation on one or more regions                                                |
| POST   | [api/regions/maintenance/integ](#post-regionsmaintenanceinteg)                     |                  | Performs an INTEG operation on one or more regions                                                |
| POST   | [api/fs/ls](#post-apifsls)                                                         |                  | Lists directory, optinally using a mask                                                           |
| POST   | [api/fs/pathExpand](#post-apifspathexpand)                                         |                  | Expands a path containg env vars and, optionally, verify if it is a dir or a file                 |
| GET    | [api/regions/{region}/getJournal-info](#get-apiregionsregiongetjournalinfos)       |                  | Retrieves infos a bout a specific journal file                                                    |
| POST   | [api/regions/{region}/journal-file-switch](#get-apiregionsregionjournalfileswitch) |                  | Switches the journal file                                                                         |
| GET    | [api/globals/by-region/{region}](#apiglobalsby-regionregion})                      |                  | Returns a list of globals out of a specific region, including zombies                             |
| GET    | [api/globals/size](#apiglobalssize)                                                |                  | Retrieves full size infomrations about globals in a specific region                               |
| GET    | [api/octo/tables/{table}/get-struct](#apioctotablestableget-struct)                |                  | Retrieves the structure of the provided table                                                     |
| GET    | [api/octo/get-objects](#apioctoget-objects)                                        |                  | Retrieves the list of tables and functions                                                        |
| POST   | [api/octo/execute](#apioctoexecute)                                                |                  | Executes a query                                                                                  |
| POST   | [api/regions/gld/validate](#apiregionsgldvalidate)                                 |                  | Validates a gld file                                                                              |
| POST   | [api/ws/start](#apiwsstart)                                                        |                  | Starts the web socket server                                                                      |
| GET    | [api/ygbl/get-pids](#apiygblgetpids)                                               |                  | Returns a list of PIDs for the specified region                                                   |
| POST   | [api/replication/topology](#apireplicationtopology)                                |                  | Scans the replication topology and collects data                                                  |
| GET    | [api/replication/full-info](#apireplicationfull-info)                              |                  | Returns full replication information (beside log files, only pointers) about the current instance |
| GET    | [api/replication/backlog](#apireplicationbacklog)                                  |                  | Returns backlog information for the current instance                                              |
| GET    | [api/dashbaord/debug-mode](#apidashbaorddebug-mode)                                |                  | Returns the debug-mode status, used to fetch the alternate userSettings.                          |

### `GET` api/dashboard/get-all

Retrieves information about all regions, all used disk devices, system information and soft3are version

Parameters:
`none`

Returns:

SUCCESS
````js
res = {
    result: 'OK',
    data: {         // The dashboard data 
        devices: [],
        gld: {},
        regions: {
            regionName: {
                dbAccess: {
                    data: []
                },
                dbFile: {
                    data: [],
                    flags: {
                        
                    },
                    usage: {}
                },
                journal: {
                    data:[],
                    flags: {}
                },
                locks: {},
                replication: {},
                stats: {
                    csa: {},
                    journal: {},
                    locks: {},
                    logicalOperations: {},
                    transactions: {}
                }
            }
        },
        systemInfo: {},
        ydb_version: ''
    } 
}
````

ERROR

````js
res = {
    result: 'ERROR',
    error : {description: ''} // The error description
}
````

WITH WARNINGS

````js
res = {
    result: 'WARNING',
    data : {
        warnings: [
            // The warning(s) description(s)
        ],
        // ... same as SUCCESS
    } 
}
````

---


### `GET` api/regions/{region}

Retrieves information about a specific region

Parameters: 

`none`

Returns:

SUCCESS
````js
res = {
    result: 'OK',
    data: {          
        dbAccess: {
            data: []
        },
        dbFile: {
            data: [],
            flags: {
                
            },
            usage: {}
        },
        journal: {
            data:[],
            flags: {}
        },
        locks: {},
        replication: {},
        stats: {
            csa: {},
            journal: {},
            locks: {},
            logicalOperations: {},
            transactions: {}
        }
    }
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: ''} // The error description
}
````

WITH WARNINGS

````js
res = {
    result: 'WARNING',
    data : {
        warnings: [
            // The warning(s) description(s)
        ],
        // ... same as SUCCESS
    } 
}
````
---

### `DELETE` api/regions/{region}

Deletes the specified region

Parameters: 

`deleteFiles=true`

If deleteFiles=true also journal and database files are deleted.

Body: 

`none`

---

### `POST` api/regions/{region}/create-db

Creates the database file specified in the given region

Parameters: 

`none`

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK'
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {
        description: '',    // The error description
        dump : []           // Optional response from the shell
     }
}
````

---

### `POST` api/regions/{region}/extend

Extends the database file in the specified region by the passed amount of blocks

Parameters: 

`number` blocks=_nnn_

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK'
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: ''} // The error description
}
````

---

### `POST` api/regions/{region}/journal-switch

Switches the journaling on or off in the specified region


Parameters: 

`string` turn=on|off

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK'
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: ''} // The error description
}
````

---

### `GET` api/dashbboard/get-templates

Retrieves all the GDE templates and their associated min/max values (when applicable)

Parameters: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: {
        region: {           // each entry is an object:
                            // { min: 0, max: 0, value: 0} 
        },
        segment: {
            BG: {
                
            },
            MM: {
                
            }
        }
    }
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: ''} // The error description
}
````

---

### `POST` api/regions/parse-namespace

Validates a namespace in the GDE.

Parameters: 

`none`

Body: 
````js
res = {
    namespace: ''
}
````

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: {
        parseResult: ''             // literal 'OK' or the error string returned by the GDE
    }
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: ''} // The error description
}
````


---

### `POST` api/regions/validate-path

Validates a path and optionally expand the directories if Env Vars are used.
It will additionally check for filename existence and return OK if it doesn't exist and ERROR if it exists.
The additional parameter fileType indicates if the provided path is referring to a directory or a file.

Parameters:

`none`

Body: 
````js
res = {
    path: ''
}
````
Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: {
        validation: '',             // an (optionally expanded) path if path is valid, 
                                    // empty string if invalid
        fileExist: '',              // the full (optionally expanded) path if the file already exists
                                    // empty string if file does NOT exists
        fileType: ''                // file or dir
    }
}
````

ERROR

````js
res = {
    result: 'ERROR',
    error: {description: ''} // The error description
}
````


---

### `POST` api/regions/{region}/edit

Edits a region to the system. 

Parameters: 

`none`

Body: 
````js
body = {
    changeAccessMethod: false,              
    changeJournal: true,                     
    journalFilename: '',
    journalUpdateGde: true,
    journalUpdateMupip: false,
    dbAccess: {
        region: [],
        journal: []
    },
    newAccessMethod: '',
    names: [],
    newJournalEnabled: true,
    regionName: '',
    segmentData: [],
    segmentFilename: '',
    updateGde: false
}
````
Returns:

SUCCESS

````js
res = {
    result: 'OK'
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {
        description: '',    // The error description
        dump: []            // Array of string with the dump of the shell response
    } 
}
````

WITH WARNINGS

````js
res = {
    result: 'WARNING',
    data : {
        warnings: [
            {
                description: '',        // The warning(s) description(s)
                dump: []                // Array of string with the dump of the shell response
            }
            
        ]
    } 
}
````


---

### `POST` api/regions/add

Adds a new region to the system. 

Parameters: 

`none`

Body: 
````js
body = {
    regionName: '',
    segmentTypeBg: true,
    journalEnabled: true,
    segmentData: [],
    dbAccess: {
        region: [],
        journal: []
    },
    templates: {
        updateTemplateDb: true,
        updateTemplateJournal: false
    },
    names: [],
    segmentFilename: '',
    journalFilename: '',
    postProcessing: {
        createDbFile: true,
        switchJournalOn: true
    }
}
````
Returns:

SUCCESS

````js
res = {
    result: 'OK'
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: ''} // The error description
}
````

WITH WARNINGS

````js
res = {
    result: 'WARNING',
    data : {
        warnings: [
            {
                description: '',        // The warning(s) description(s)
                dump: []                // array with shell responses for debugging
            }
            
        ]
    } 
}
````


---

### `GET` api/test/error

Triggers an M error and returns the error description in the status 500 response

Parameters: 

`none`

Returns:

ERROR

````js
res = {
    apiVersion: "1.0",
    error: {
        code: 500,
        errors: [
            {
                logID: 99999,
                mcode: " s a=1/0",
                message: "150373210,error+1^%ydbguiRest,%YDB-E-DIVZERO, Attempt to divide by zero",
                place: "error+1^%ydbguiRest",
                reason: ",M9,Z150373210,"
            }
        ],
        message: "Internal Server Error",
        request: "DELETE /api/test/error/ "
    }
}
````

---

### `POST` api/test/error

Triggers an M error and returns the error description in the status 500 response

Parameters: 

`none`

Body: 

`none`

Returns:

ERROR

````js
res = {
    apiVersion: "1.0",
    error: {
        code: 500,
        errors: [
            {
                logID: 99999,
                mcode: " s a=1/0",
                message: "150373210,error+1^%ydbguiRest,%YDB-E-DIVZERO, Attempt to divide by zero",
                place: "error+1^%ydbguiRest",
                reason: ",M9,Z150373210,"
            }
        ],
        message: "Internal Server Error",
        request: "DELETE /api/test/error/ "
    }
}
````

---

### `DELETE` api/test/error

Triggers an M error and returns the error description in the status 500 response

Parameters: 

`none`

Body: 

`none`

Returns:

ERROR

````js
res = {
    apiVersion: "1.0",
    error: {
        code: 500,
        errors: [
            {
                logID: 99999,
                mcode: " s a=1/0",
                message: "150373210,error+1^%ydbguiRest,%YDB-E-DIVZERO, Attempt to divide by zero",
                place: "error+1^%ydbguiRest",
                reason: ",M9,Z150373210,"
            }
        ],
        message: "Internal Server Error",
        request: "DELETE /api/test/error/ "
    }
}
````

---

### `GET` api/regions/locks/get-all

Returns all the locks, waiters and related processes information

Parameters: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    locks: [
        {
            namespace: 'TEST1',
            region: 'YDBOCTO',
            pid: 1,
            waiters: [
                11,
                12,
                13,
            ]
        },
        {
            namespace: 'TEST2',
            region: 'DEFAULT',
            pid: 2,
            waiters: [
                21,
                22
            ]
        },
        {
            namespace: 'TEST3',
            region: 'YDBAIM',
            pid: 3,
            waiters: [
                31,
                32,
                33
            ]
        },
    ],
    pids: [
        {
            pid: 1,
            userId: 'pid1',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 2,
            userId: 'pid2',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 3,
            userId: 'pid3',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 11,
            userId: 'pid11',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 12,
            userId: 'pid12',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 13,
            userId: 'pid13',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 21,
            userId: 'pid21',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 22,
            userId: 'pid22',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 23,
            userId: 'pid23',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 31,
            userId: 'pid31',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 32,
            userId: 'pid32',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
        {
            pid: 33,
            userId: 'pid33',
            processName: 'yottadb',
            PPID: 0,
            time: '00:00:00'
        },
    ],
    regions: [
        {
            name: 'DEFAULT',
            estimatedFreeLockSpace: '100% of 220 pages',
            processesOnQueue: '0/880',
            slotsInUse: '0/597',
            slotsBytesInUse: '0/28080'
        },
        {
            name: 'YDBOCTO',
            estimatedFreeLockSpace: '100% of 220 pages',
            processesOnQueue: '0/880',
            slotsInUse: '0/597',
            slotsBytesInUse: '0/28080'
        },
        {
            name: 'YDBAIM',
            estimatedFreeLockSpace: '100% of 220 pages',
            processesOnQueue: '0/880',
            slotsInUse: '0/597',
            slotsBytesInUse: '0/28080'
        }
    ]
}
````

ERROR

````js
res = {
    result: 'ERR',
    error : {description: '',   // The error description
    dump: []                    // Eventual dump of shell response
} 
}
````

---

### `POST` api/regions/locks/clear

Clears the lock identified in the namepsace parameter.

Parameters: 

`namespace=`

Specify the namespace to clear.

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK'
}
````

ERROR 

````js
res = {
    result: 'ERROR',
    error : {description: ''} // The error description
}
````


---

### `POST` api/os/processes/{pid}/terminate

Terminates the process identified by the `pid` parameter. 

Note: the process must be a yottadb process or it won't be terminated and an error will be returned.

Parameters: 

`none`

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK',
}
````


ERROR

````js
res = {
    result: 'ERROR',
    error : {description: ''} // The error description
}
````

---

### `GET` api/globals/find

Searches globals in the global directory.

Parameters: 

`mask`
The search string WITHOUT the ^ char (no * or ? allowed, just the search string). It can be empty (to start from the beginning).

`size`
The number of records returned by the call. If omitted or 0, it will return all the records.

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: []            //An array of strings with the global names
}
````


ERROR

````js
res = {
    result: 'ERROR',
    error : {description: ''} // The error description
}
````

---

### `POST` api/globals/traverse

Traverse a global and returns the entries and values.

It is possible to use the * and the comma (,) to format / restrict the data returned.\

It is possible to use ranges, with or without start/end params

Each row in the returned list includes:
- The global name
- An array of subscripts
- The(eventual) value of the node
- The $data of the node (always > 0)

Parameters: 

`none`

Body: 

````js
body = {
    namespace: {
        global: '^MYGLOBAL',            // The global name prefixed by the ^ char
        subscripts: [
            {
                type: '',               // The type of subscript: (always present)
                                        // V for value: node value must be set
                                        // , (comma) for placeholder : no other node needs to be set
                                        // * (star) for all children (MUST be the last subscript : no other node needs to be set
                                        // :range both value and valueMax can be empty

                value1: 'S',			// S  or N  data type for left param if type is :
                value2: 'N',			// S or N  data type for second param if type is :
                value: '',              // The value. It can be empty in a range, comma or star types
                                        // Strings are ALWAYS quoted

                valueEnd: ''            // The end value of a range. Can be empty if value is not empty
                                        // Strings are ALWAYS quoted 
            }
        ]
    },
    size: 0,                            // The number of records to return. If missing, it will default to 100
    offset: '',                         // If going DOWN, it is the string path of the last record
                                        // IMPORTANT: if you are re-submitting the call, you MUST RESEND ALSO the same namespace you used in the previous call or failure may occur 
    upOffset: 0,                        // If going UP, it is the numeric record to start with
                                        // IMPORTANT: if you are re-submitting the call, you MUST RESEND ALSO the same namespace you used in the previous call or failure may occur 
    direction: '',                      // 'up' OR 'down' OR 'end'. If omitted or empty defaults to 'down'
    dataSize: 512,                     // The maximum size of the returned data. If omitted, default is 512 chars
}
````

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: []            //An array of strings with the global names
}
````


ERROR

````js
res = {
    result: 'ERROR',
    error : {description: ''} // The error description
}
````

---

### `GET` api/globals/dollar-data

Returns the $data value of a global.
If name is not valid, it returns -1.

Parameters: 

`globalName`
The name of the global, including the ^ character.

Body: 

`none`

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: value            //The $data value OR -1 if syntax error
}
````
---

### `POST` api/globals/get-data

Returns the entire node value of the passed parameter PATH

Parameters:

`path`
The path you wish to retrieve the value from.

Body:

````js
body = {
    path: path
}
````

Returns:

SUCCESS

````js
res = {
    result: 'OK',
    data: value            //The value
}
````

---

### `POST` api/routines/find

Returns a list of routines found, using the provided mask to filter it

Parameters:

Body:

`mask`
The mask to filter the list. It must have at least 2 chars (excluding wildcards). % (which is _y) may be one of the
character.
> Note: the char _ may be used instead of the %

Wildcards are: * and _ ?

`zroutines`  (optional)

The `zoutines` parameter allows you to set a new zroutines path list

Returns:

SUCCESS

No routines found:

````js
res = {
    result: 'OK',
    data: 0
}
````

Routines found:

````js
res = {
    result: 'OK',
    data: [
        {
            location: '/YDBGUI/objects/',
            routine: 'testrou',
            source: true
        },
        {
            location: '/YDBGUI/objects/',
            routine: 'testrou2',
            source: true
        }
    ]
}
````

It returns an array of objects.

Each object has the following notation:

`location`: The path of the file / object / so that contains the code

`routine`:  The routine name

`source`:   Boolean true if source code is available, false if only object code is available


---

### `POST` api/routines/routine

Returns a routine

Parameters:

Body:

`routine`
The name of the routine.
> Note: the char _ may be used instead of the %

`zroutines`  (optional)

The `zoutines` parameter allows you to set a new zroutines path list

Returns:

SUCCESS

````js
res = {
    "data": {
        "file": [
            "%ydbgui ; Start-up code",
            "..."
        ],
              "object": {
                  "props": {
                      "accessRightsHRF": "-rw-r--r--",
                      "accessRightsOctal": 644,
                      "birth": "-",
                      "filename": "_ydbgui.o",
                      "lastAccess": "2022-10-29 18:58:36.541483713 +0000",
                      "lastModified": "2022-10-29 18:58:36.541483713 +0000",
                      "lastStatusChange": "2022-10-29 18:58:36.541483713 +0000",
                      "ownerGroupName": "root",
                      "ownerUserName": "root",
                      "path": "/YDBGUI/objects/",
                      "size": 12544
                  }
              },
              "props": {
                  "accessRightsHRF": "-rwxrwxrwx",
                  "accessRightsOctal": 777,
                  "birth": "-",
                  "filename": "_ydbgui.m",
                  "lastAccess": "2022-10-25 09:48:23.211996500 +0000",
                  "lastModified": "2022-10-25 09:16:41.130358300 +0000",
                  "lastStatusChange": "2022-10-25 09:16:41.130358300 +0000",
                  "ownerGroupName": "root",
                  "ownerUserName": "root",
                  "path": "/YDBGUI/routines/",
                  "size": 5039
              }
          },
    "result": "OK"
}
````

---

### `GET` api/server/mode

Returns the current server mode (RO or RW)

Parameters:

`none`

Body:

`none`

Returns:

SUCCESS

````js
res = {
    serverMode: 'RO'
}
````

---

### `POST` api/regions/maintenance/defrag

Performs a DEFRAG operation on one or more regions.

Parameters:

`none`

Body:

````js
body = {
    fillFactor: 0,                      // the fillFactor parameter, between 0 and 100
    indexFillFactor: 0,                 // the indexFillFactor parameter, between 0 and 100
    regions: [                          // the region list, an array of strings
        'default'
    ],
    recover: false,                     // the `recover` parameter, boolean, default is false
    resume: false                       // the `resume` parameter, boolean, default is false
}
````

Returns:

SUCCESS

````js
res = {
    command: 'string',                  // the command that has been executed in the server
    result: 'OK',
    data: {                             // one key per region
        DEFAULT: {
            byGlobal: {                 // one key per global
                a: {
                    coalesced: 0,
                    extended: 0,
                    freed: 0,
                    processed: 0,
                    reused: 0,
                    split: 0,
                    swapped: 0
                }
            },
            incompleteGlobalList: false, // used in resume, to indicate that the global list is incomplete due to earlier processing
            totals: {
                coalescend: 0,
                extended: 0,
                freed: 0,
                processed: 0,
                reused: 0,
                split: 0,
                swapped: 0
            }
        }
    },
    payloadRegions: []                  // internally used by the async executor
}
````

ERROR

````js
res = {
    result: 'ERROR',
    error: {description: ''}           // The error description
}
````

---

### `POST` api/regions/maintenance/integ

Performs an INTEG operation on one or more regions.

Parameters:

`none`

Body:

````js
body = {
    regions: [                          // the region list, an array of strings
        'default'
    ],
    type: 'fast',                       // the `type` parameter, which can be `fast` or `complete`. Default is `complete`.
    reporting: 'full',                  // the `reporting` parameter, which can be `summary` or `full`. Default is `summary`.
    keyRange: true,                     // the `keyRange` parameter, boolean. Default is `true`.
    stat: false,                        // the `stat` parameter, boolean. Default is `false`.
    mapErrors: 10,                      // the `mapErrors` limit. Value is from 0 (unlimited) to 1,000,000. Default is 10.
    keySizeErrors: 10,                  // the `keySizeErrors` limit. Value is from 0 (unlimited) to 1,000,000. Default is 10.
    transactionErrors: 10,              // the `transactionErrors` limit. Value is from 0 (unlimited) to 1,000,000. Default is 10.
}
````

Returns:

SUCCESS

````js
res = {
    command: 'string',                   // the command that has been executed in the server
    result: 'OK',
    data: []                            // an array of strings with the result dump
}
````

ERROR

````js
res = {
    result: 'ERROR',
    error: {description: ''}           // The error description
}
````

### `POST` api/fs/ls

Lists a directory

Parameters:

Body:

`path` (optional)
The path. It can be either relative or absolute and can include env vars.

> If a path is not supplied, it will default at root /

`type`  (optional)
Specify if you want to return files, directories or both.

`"F"` will return files

`"D"` will return directories

`"FD"` will return both files and directories

> The defaul value is `"FD"`

`mask` (optional)
An eventual mask to filter the files.

> It defaults to `*`

Returns:

SUCCESS

````js
res = {
    "data": [
        {
            path: 'libs',
            type: 'dir'
        },
        {
            path: 'test.txt',
            type: 'file'
        }
    ],
    "result": "OK"
}
````

---

### `POST` api/fs/path-expand

Expands a path containing env vars and, optionally, verify that the path points to a directory or a file

Parameters:

Body:

`path` (optional)
The path. It can be either relative or absolute and can include env vars.

`dirsOnly`  (optional)
A boolean to specify if you want to check if the target is a file or a dir
> The default value is `false`

Returns:

SUCCESS

````js
res = {
    data: '/the/returned/path',
    result: "OK"
}
````

ERROR

````js
res = {
    error: {
        code: -2,
        description: 'The target path is not a directory'
    },
    result: "ERROR"
}
````

````js
res = {
    error: {
        code: -3,
        description: 'The target path is not a file'
    },
    result: "ERROR"
}
````

````js
res = {
    error: {
        code: number,
        description: 'The error description'
    },
    result: "ERROR"
}
````

---

### `GET` api/regions/{region}/get-journal-info

Retrieve information about the journal files associated with the specified region

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    data: [
        {
            chainCnt: nn,                   // the sequence number, from 1 upwards. If out of chain, 99999
            filename: journalFilename,      // the journal filename
            params: [],                     // the journal file header parameters
            status: 'OK'                    // OK or ERROR if file is ok
        }
    ],
    result: "OK"
}
````

ERROR

````js

````
js
res = {
    error: {
        code: number,
        description: 'The error description'
    },
    result: "ERROR"
}
````

---

### `POST` api/regions/maintenance/backup

Performs a backup

Parameters:

Body:

`regions` ARRAY OF STRINGS

`targetPath`  STRING

`replace` BOOLEAN

`disableJournaling` BOOLEAN

`record` BOOLEAN

`createNewJournalFiles` STRING (`link`, `nolink`, `no`)

`syncIo` BOOLEAN (can be omitted if createNewJournalFiles = no, if present will be ignored)

`includeReplicatedInstances` BOOLEAN

`replUseSamePath` BOOLEAN

`replTargetPath` STRING (mandatory if includeReplicatedInstances = true)

Returns:

SUCCESS

````js
res = {
    command: stringValue,            // the MUPIP command used to execute the backup                                         
    data: [],                        // array of strings with the MUPIP result
    result: "OK"
}
````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `POST` api/regions/{region}/journal-file-switch

Switches the journal file

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    data: [],                        // array of strings with the MUPIP result
    result: "OK"
}
````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `GET` api/globals/by-region/{region}

Returns a list of globals out of a specific region, including zombies

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    data: [                         // array of strings with the globals
        {
            name: '^test',
            status: 'zombie'
        },
        {
            name: '^test2',
            status: 'ok'
        }
    ],
    result: "OK"
}
````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `GET `api/globals/size

Retrieves full size infomrations about globals in a specific region

Parameters:

`region`
The (not case sensitive) name of the region

`type`
The algorithm used:

It can be either: `scan`, `arsample` or `impsample`

`param`

The parameter, specific for the selected `type`. See the documentation for the correct value.

Body:

Returns:

SUCCESS

````js
res = {
    data: {
        dump: [                         // an array with the MUPIP output
        ]
    },
    result: "OK"
}
````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `GET ` api/octo/tables/{table}/get-struct

Retrieves the structure information of the provided table.

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    data: {
        columns: [                         // an array with all the columns
            {
                collation: '',
                default: '',
                name: 'FIELDNAME', type: 'VARCHAR'
            }
        ],
        global: 'globalName',
        name: 'thetablename',
        type: 'READONLY',
        indexes: [
            {
                columns: 'PK',
                global: 'globalname',
                name: 'theindexname',
                type: 'PRIMARY KEY CONSTRAINT'
            }
        ]
    },
    result: "OK"
}

````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `GET ` api/octo/get-objects

Retrieves the list of tables and functions.

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    data: {
        tables: [                         // an array with all the tables
            {
                name: 'CATEGOERIES',
            }
        ],
        functions: [                       // an array with all the functions and their details
            {
                name: 'ABS',
                args: ['arg1'],
                numArgs: 1,
                returnType: 'int4',
                source: '"$$ABS^%ydboctosqlfunctions"'
            }
        ]
    },
    result: "OK"
}

````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `POST ` api/octo/execute

Retrieves the list of tables and functions.

Parameters:

Body:

 ````js
 body = {
    query: '\d',        // the query, with or without ; terminator
    timeout: 500        // the timeout, in seconds
}

````

Returns:

SUCCESS

````js
res = {
    data: [                             // the response from the query engine AS IT IS.
        'string', 'string'
    ],
    parseError: true,                   // true if query couldn't be parsed. Data node contains the error
    result: "OK"
}

````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the MUPIP result
        description: ''
    },
    result: "ERROR"
}
````

---

### `POST ` api/regions/gld/validate

Validates a gld file, optionally supporting custom Current Working Directory and Environment Variables needed to satisfy
the databases file path.

The code will first validate the gld file using the GDE.
If the file gets validated, we extract the list of filenames associated with each segment.
For each filename, we extract a list of unique environment variables and the absolute path flag.
The list of environment variables in then populated with values from the env vars already set in the current image. If
the env var doesn't exists, is set to an empty string.

We now proceed with path validation:
If the request contains a Current Working Directory, we set it.
If the request contains a list of environment variables, we replace the env vars in the path with their value.
We then check for file existence and, if not found, push it into the return object: pathsInError

Parameters:

Body:

 ````js
 body = {
    path: '',       // the path of the gld file
    cwd: '',        // the (optional) Current Working Directory
    enVars: []      // the (optional) Environment Variables. It is an array of objects witht he following signature: { envvar: 'env var value' }
                    // note that the env vars name must be prefixed by a $
}

````

Returns:

SUCCESS

````js
res = {
    result: "OK",
    envVars: {},                // An array of objects: {$envVar: 'envVarValue} Note: the value may be empty (missing)
                                // if present, this is the list of env vars needed to satisfy the databases paths.
    envVarsFlag: true,          // if true, it means that the request passed environment variables in the request
    absoluteFlag: true,         // If present, it means that at least one database path has a relative path
    pathsInError: {}            // list of paths that couldn't be satisfied using the passed parameters 
                                // Object with nodes: { filename: ''}
}

````

ERROR

````js
res = {
    error: {
        dump: [],                   // arra y of strings with the GDE result
        description: ''
    },
    result: "ERROR"
}
````

---

### `POST ` api/ws/start

Starts the web server and returns the PID or returns the server status

Parameters:

Returns:

SUCCESS

````js
res = {
    result: "OK",                   // can be either: 
                                    // OK               Server got started
                                    // WARNING          Already running
                                    // ERROR            Could not start the server
    data: {
        PID: 0000                   // If status OK, the PID of the server
    }
}

````

ERROR

````js
res = {
    error: {
        description: '',                // the error description
        code: 0                         //  1:  Already running
                                        // -1:  Could not execute the find_node_process    
                                        // -2:  Server did NOT start correctly. Dump available
                                        // -3:
    },
    result: "ERROR"
}
````

---

### `GET ` api/ygbl/get-pids

Returns a list of PIDs for the specified region

Parameters:

region

Returns:

SUCCESS

````js
res = {
    result: "OK",
    data: {
        processes: [                // If status OK, the PIDs found
            1234,
            4321
        ]
    }
}

````

---

### `POST ` api/replication/topology

Scans the replication topology and collects data

It returns a tree structure of the server's data. It will return data only for the servers listed in the request.

It can return different information:

- (F)ull will return repl file, health, logs files list and backlog
- (B)acklog will return backlog information
- (H)ealth will return health information
- (L)og will return logs for the files listed in the body.discoveryService.logFiles.files array. The body.discoveryService.logFiles.tail number indicates how many records we want to return, 0 for all.

> When selecting (L)ogs, you can add additional info to restrict the data returned.
> You can pass either a list of files you want to have or the number of lines of the tail (it must be a negative number)
> If you omit one of both parameters, the default is all files, full file content


> The body.discoveryService.timeouts object is used to control the recursive call.

> The https protocol is not implemented yet

Parameters:

Body:

 ````js
 body = {
    discoveryService: {
        responseType: 'F',                  // F | B | H | L:
                                            // (F)ull, will get everything and ignore other parameters
                                            // (B)acklogs
                                            // (H)ealth
                                            // (L:)og files Format: L:instanceName
                                            // It will crawl until it finds the machine, fetch the files and return

        servers: [                          // An array with all the machines to look for
            {
                instance: 'melbourne',      // instance name      
                host: '10.5.0.2',           // IP address or hostname
                port: 8089,                 // GUI port
                statsPort: 8090,            // (optional) stats port
                protocol: 'http',           // currently only http supported
                tlsFileLocation: '',        // an (optional) location of the TLS foreign key
                verifyCertificates: true,   // true if you want to verify the certificates, false for self-signed
            },
            {
                instance: 'santiago',
                host: '10.5.0.4',
                port: 10089,
                statsPort: 10090,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: true,
            },
        ],
        timeouts: {
            onConnect: 500,                 // crowler connection timeout in milliseconds
            onConnectType: 'M',             // how to handle the timeout. It can be M, CURL or none
            onResponse: 5000,               // crowler response timeout in milliseconds
        },
        logFiles: {                         // this (optional)section only when fetching log files
            tail: -50,                      // optional tail, full file if missing
            files: [                        // optional file list, all files if missing

            ]
        }
    }
}

````

Returns:

SUCCESS

````js
res = {
    result: "OK",                   // can be either: 
}

````

ERROR

````js
res = {
    error: {
        description: '',                // the error description
    },
    result: "ERROR"
}
````

---

---

### `GET` api/replication/get-all

Gets full information from the current instance.

Parameters:

Returns:

SUCCESS

````js
res = {
    result: "OK",                   // can be either: 
}

````

ERROR

````js
res = {
    error: {
        description: '',                // the error description
    },
    result: "ERROR"
}
````

---

### `GET` api/replication/backlog

It returns the backlog information of the current instance.

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    result: "OK",                   // can be either: 
}

````

ERROR

````js
res = {
    error: {
        description: '',                // the error description
    },
    result: "ERROR"
}

````

---

### `GET` api/dashboard/debug-mode

Returns the debug-mode status, used to fetch the alternate userSettings.

Parameters:

Body:

Returns:

SUCCESS

````js
res = {
    debugMode: true,                   // can be either: true or false 
}

````

ERROR

````js
res = {
    error: {
        description: '',                // the error description
    },
    result: "ERROR"
}
````
