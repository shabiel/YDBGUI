<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# Statistics

### The MUMPS Statistics server

The MUMPS statistics server is a very light MUMPS application that receive commands, execute them, respond to the caller
with a
response, then wait for
new commands to be executed, until it receives the "suicide" command to terminate.

Commands are received as a JSON string and parsed into a M array, for proper parsing.
A single command can contain multiple data-collection operation, each with different method and parameters, and each
response will be all pushed into the response array.

Currently, we have implemented the ^%ygblstats only, but to add a new collection method will be very simple and
straight-forward.

