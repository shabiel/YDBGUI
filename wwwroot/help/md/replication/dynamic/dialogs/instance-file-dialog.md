<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## <a href="#" onclick="app.link('replication/dynamic/index')">Dynamic mode: monitoring the Replication instances</a>

## Instance file dialog

---

<img src="md/replication/dynamic/dialogs/img/instance-file-dialog.png" width="700">

The Instance File dialog displays information about the Instance file.

The dialog has 3 tabs, to display the different sections of the Instance file:

- Header
- Slots
- History

---

### Header

The Header section groups all the entries, to make it easier to read.

<img src="md/replication/dynamic/dialogs/img/instance-file-dialog.png" width="400">



---

### Slots

The Slot section displays all the allocated Slots.

<img src="md/replication/dynamic/dialogs/img/instance-file-slots.png" width="400">

---

### History

The History displays the history records that get appended, as the replication gets used.

<img src="md/replication/dynamic/dialogs/img/instance-file-history.png" width="400">

