<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## <a href="#" onclick="app.link('replication/modify-topology')">Modifying, saving and recalling the Topology interactive chart</a>

## Save dialog

---

<img src="md/replication/img/edit-save-dialog.png" width="600">

The Save dialog is used to save the current topology.

The following information is saved:

- The interactive chart layout
- The current zoom factor
- The current position on the paper

You can provide the following information:

##### Name

The name can contain any characters / symbols.

##### Description

The description is a long text used to provide more information about this layout (like "Staging farm in Las Vegas").

##### Autoload

By setting the autoload switch on, you instruct the GUI to automatically load this template when the Replication Topology tab gets opened. You can have only one template with autoload; by setting it you will unset any other template with the autoload property set.
