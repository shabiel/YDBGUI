<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Load dialog

---

<img src="md/stats/img/load-dialog.png" width="600"> 

In the Load dialog you can perform the following operation:

- Load a report stored in your browser storage
- Import a report stored on a file
- Delete a report from your browser storage

The dialog displays a list with all report names found in the browser storage.

By clicking on a name, the associated description will be displayed in the right pane.

### Load a report

You load a report by clicking on the <img src="md/stats/img/load-ok-btn.png" width="50"> button.

This will replace the report currently loaded (if present).

If the current report has been modified, a dialog will appear, asking you if you want to save it first.

> The statistics tab caption will change to show you at all times the name of the loaded report between square brackets.

### Import a report

You can load a report by clicking on the <img src="md/stats/img/load-import-btn.png" width="100"> button.

It will then display a file selection dialog where you can select your file.

> Statistics file have a .ysr extension, which stands for `YottaDB Statistics Report`.

This will replace the report currently loaded (if present).

If the current report has been modified, a dialog will appear, asking you if you want to save it first.

### Delete a report

You can delete a report by clicking on the <img src="md/stats/img/load-delete-btn.png" width="80"> button.

The currently selected report will be deleted and the list refreshed to reflect the changes.
