<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Save dialog

---

<img src="md/stats/img/save-dialog.png" width="600"> 

In the Save dialog you can perform the following operations:

- Save a report in your browser storage and provide as (optional) description
- Export a report to a file

### Save a report

You save a report in the browser storage by entering a name and an optional description and clicking on the <img src="md/stats/img/save-ok-btn.png" width="50"> button.

The description can be up to 1M long. For easy editing of long text, the text area can be expanded by dragging the lower right corner with the mouse.

<img src="md/stats/img/save-report-enlarge.png" width="300"> 

If a report with the same name already exists in your browser's storage, an input box will prompt you to replace it or cancel the operation.

### Export a report

You can export a report to a file by clicking on the <img src="md/stats/img/save-export-btn.png" width="80"> button.

A standard Save dialog will be displayed, where you can enter the file name.

The file is saved with the default extension `.ysr`, which stands for YottaDB Statistics Report and its content is JSON.
