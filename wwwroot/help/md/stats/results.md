<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Viewing and exporting the collected data

---

The view and export tab pane is available only after the data collection is stopped and data has been collected.

> If no data could be collected, a message will appear to alert the user that no data can be displayed.

The Results pane will display:

- A table for each data source filled with the (paged) collected data
- A new toolbar with dedicated tools to filter and export data

<img src="md/stats/img/result-overview.png" width="400"> 

---

## The results pane

In the result pane we can view the data in pages of 100 records each. Using the Navigation pane we can navigate through the pages.

The table headers display the data fields of the data.

<img src="md/stats/img/result-table-overview.png" width="600">

The headers content may change, depending on the following:

- The Data Source properties (regions, processes)
- The current filter ( [regions], [processes], samples and values)

> For data sources using the TOP process type, processes that didn't collect any data will not be displayed.

---

## The result toolbar

The result toolbar has the following tools:

- `Data source selector`<br>  <img src="md/stats/img/result-toolbar-sources.png" width="200"><br><br>
- `Filters`, used to restrict the data displayed on the table. Their enabled status depends on the source parameters <br> <img src="md/stats/img/result-toolbar-filters.png" width="400"><br><br>
- `Data source properties`, <a href="#" onclick="app.link('stats/source-props')">a dialog</a> to display full information about the currently selected data source <img src="md/stats/img/result-toolbar-properties.png" width="40"><br><br>
- `Zoom in` `Zoom out`, to increase or decrease the font size in the data results table <img src="md/stats/img/result-toolbar-zoom.png" width="80"><br><br>
- `Export`, <a href="#" onclick="app.link('stats/export')">a dialog</a> to collect the final information needed to export data to a csv file <img src="md/stats/img/result-toolbar-export.png" width="40"><br><br>

---

## Using the filters

With the help of the filters, we can restrict the view to data we are interested in. The software will automatically re-generate the new headers and fill them with the corresponding data.

> The export dialog will export the data using the current filter, if any.

With the ``Data source selector`` we can flip between the data sources used in the acquisition.

> If only one data source was used, this selector will be disabled



Depending on the selected Data Source, filters will be automatically enabled / disabled:

For a YGBLSTAT type data source:

- If only one region is selected, the Regions filter will be disabled
- If process mode is Aggregate, the Processes filter will be disabled
- In all other cases, filters will be enabled and populated with Regions and Processes for you to select / deselect

The Samples filter is always enabled and its list is populated with the samples specified in the Source definition.

The Values filter is always enabled and its list is populated with the following fields:

- Abs: the absolute value returned by the server
- Delta: the difference between the current sample and the previous sample
- Delta Min: the lowest Delta value we experienced during the data acquisition
- Delta Max: the highest Delta value we experienced during the data acquisition
- Delta Avg: the average Delta value computed between all the collected samples

Filters are made up of a list of fields, each with a checkbox next to it. By selecting or deselecting an item, we change the filtering.

> On each filter, at least one item must be selected and the software will prevent you from deselecting all the items.

<img src="md/stats/img/result-filter-zoom-2.png" width="400"> 

<img src="md/stats/img/result-filter-zoom.png" width="200"> 

As soon as you select / deselect an item, the table gets re-processed: the headers are recomputed and the content is populated with the data.
