<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## Sources

---

<img src="md/stats/img/sources.png" width="600"> 

The Sources dialog allows you to create, edit and delete sampling sources. You can have as many sources as you want, and each source can have a different Sample time.

For each source, we will create statistics data (delta, average, min, max) for each sample, which can be then individually selected for reporting.

Sources can be enabled or disabled, to allow you to quickly switch between samples.

By disabling a source, the related graphs and tables will not be active.

At the moment we have three different data connectors available:

- PEEKBYNAME
- FHEAD
- YGBLSTAT

<a href="#" onclick="app.link('stats/data-connectors')">You can find here all the details</a>

---

#### Creating a source

You create a source by clicking on the <img src="md/stats/img/sources-add-btn.png" width="100"> button.

Depending on the choice, it will display one of the following dialogs:

<a href="#" onclick="app.link('stats/ygblstats-manage')">YGBLSTAT Add entry</a> dialog.

<a href="#" onclick="app.link('stats/fhead-manage')">FHEAD Add entry</a> dialog.

<a href="#" onclick="app.link('stats/peekbyname-manage')">PEEKBYNAME Add entry</a> dialog.


---

#### Editing a source

In order to edit a source, you need to select it in the list and then click the <img src="md/stats/img/sources-edit-btn.png" width="100"> button.

---

#### Deleting a source

In order to delete a source, you need to select it in the list and then click the <img src="md/stats/img/sources-delete-btn.png" width="100"> button.

---

#### Deleting all sources

In order to delete all the sources, you need to click the <img src="md/stats/img/sources-delete-all-btn.png" width="100"> button.

---

#### Enabling / disabling a Source

In order to enable or disable sources, click the <img src="md/stats/img/sources-enabled-check.png" width="50"> button.
