<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/spark-chart/index')">Report definition</a> / <a href="#" onclick="app.link('stats/spark-chart/report-line')">Table dialog</a> / Highlights dialog

---

<img src="md/stats/spark-chart/img/report-line-highlights-dialog.png" width="600">

The highlights dialog allows you to configure the way the background gets painted while the data acquisition is running.

---

#### `Normal` mode

In Normal mode, the background color is the one defined in the settings.

---

#### `Range` mode

In Range mode the background color changes according to the cell value.

You can set the values in this section of the dialog:

<img src="md/stats/spark-chart/img/report-line-highlights-range.png" width="400">

The colors can be changed for each table by clicking the `Settings` button in
the <a href="#" onclick="app.link('stats/spark-chart/report-line')">Report Line dialog</a>.

<img src="md/stats/spark-chart/img/report-line-preferences-highlighters.png" width="500">

---

#### `Max of processes` mode

When Max of processes mode is selected, the background color change on the process with the highest value.

The color can be changed for each table by clicking the `Settings` button in
the <a href="#" onclick="app.link('stats/spark-chart/report-line')">Report Line dialog</a>.

<img src="md/stats/spark-chart/img/report-line-preferences-highlighters.png" width="500">

> This option is available ONLY when the processes mode of the Source is `Top`.
