<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/sources')">Sources</a>

## Data connectors

---

There are three different data connectors available:

- `PEEKBYNAME` (real-time)
- `FHEAD` (real-time by process)
- `YGBLSTAT` (database file header)

Each data connector has different properties:

|            | Requires process opt-in | Per-process monitoring | Opens database | Provides Replication info |
|------------|-------------------------|:----------------------:|:--------------:|:-------------------------:|
| PEEKBYNAME | N                       |           N            |       Y        |             Y             |
| YGBLSTAT   | Y                       |           Y            |       N        |             N             |
| FHEAD      | N                       |          N.A.          |       N        |             Y             |

---

### PEEKBYNAME

<img src="md/stats/img/add-entry-dialog-peekbyname.png" width="400"> 

PEEKBYNAME() provides a read-only mechanism to access selected fields in selected control structures in the address space of a process, including process private memory, database shared memory segments and Journal Pools.

A full description of PEEKBYNAME can be found <a href="https://docs.yottadb.com/ProgrammersGuide/utility.html#peekbyname">here</a>

<a href="#" onclick="app.link('stats/peekbyname-manage')">PEEKBYNAME dialog</a>


> Using PEEKBYNAME we need to get information from the shared memory, meaning the process opens the database file, which can perturb the data ever so slightly.

---

### FHEAD

<img src="md/stats/img/add-entry-dialog-fhead.png" width="400"> 

The FHEAD fields are read from the Database file header

<a href="#" onclick="app.link('stats/fhead-manage')">FHEAD dialog</a>

> Using FHEAD you get information that gets updated only when data is flushed to the filesystem

---

### YGBLSTAT

<img src="md/stats/img/add-entry-dialog-ygbl.png" width="400"> 


YGBLSTAT collects statistics `by process`, meaning you can monitor individual processes running on your server.

The YGBLSTAT dialog allows you to select between:

- The cumulative value between all registered processes
- Specific PIDs of running processes
- A total of maximum _n processes_ that have the highest value of a specific sample (like SET, KIL, etc)

<a href="#" onclick="app.link('stats/ygblstats-manage')">YGBLSTAT Source dialog</a>


> In order to perform statistics acquisition, you need to opt in specifying the target region(s) or all regions.
> In M code, you can use the <a href="https://docs.yottadb.net/ProgrammersGuide/commands.html#no-statshare-region-list" target="blank">VIEW "[NO] STATSSHARE" command</a>
> Alternately, you can set an <a href="https://docs.yottadb.net/AdminOpsGuide/basicops.html#ydb-statshare" target="blank"> environment variable</a>

---
