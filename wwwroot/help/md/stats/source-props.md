<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

# <a href="#" onclick="app.link('stats/results')">Viewing and exporting the collected data</a>

## Source properties

---

<img src="md/stats/img/result-source-props.png" width="600"> 

The Source properties dialog shows you information about the current Data source in view.
