<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

# <a href="#" onclick="app.link('stats/results')">Viewing and exporting the collected data</a>

## Export dialog

---
<img src="md/stats/img/result-export-dialog-plain.png" width="600"> 

With the export dialog you actually export the data to a CSV file, located on your own computer.

After setting up details about your file, you can press the Ok button to display a File Selector dialog where you can set the destination and file name.

Several options are available:

- `Add source information`
- `Timestamp format`
- `Add fields header row`
- `Row terminator`

## Add source information

It will insert, at the very beginning, a few lines of information about the Data source used.

## Timestamp format

It allows you to choose between different formats for the timestamp. The default (ISO 8601) is accepted by Excel to import the timestamp as Datetime format.

## Add fields header row

It will populate the first row of the file with the field names. Many CSV import programs will automatically detect it and assign headers to them.

When dealing with sources with multiple regions or process type different from Aggregate it will display additional switches for you to optionally include Region name and process number in the header field name.

<img src="md/stats/img/result-export-dialog.png" width="300"> 

## Row terminator

The row terminator sequence to use to go to the next line.

> The software will detect your platform and automatically pre-select the option for you. You can override the option at any time.
