<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## The statistics report

---

The statistics report is the report that displays the sampled data.

It is divided in two sections:

- The `Graph` section: displays sampled data as series on a graph.
- The `Table` section: display sampled data as 'fields' that get updated on each new sample.

Each report can have a minimum of 0 and a maximum of two graphs and an unlimited number of tables.

Each report can have an unlimited number of sources, also each having a different sample rate.

> Seeing the ability to map fields on multiple Graphs and Tables, we encourage users to use as few Sources as possible,
> as it will be better for the server, as we will perform one fetch only instead of many

The height of the graph can be changed using the splitter located at the bottom of the graph.

The Table section will, if necessary, display a scroll bar if the report size exceeds the browser size.

<img src="md/stats/img/report-drawing-2.png" width="800">

---

The Graph

On the top right of the graph, there are icon to perform additional actions.

The timescale tool allow you to change the number of seconds you display in the graph.

<img src="md/stats/img/report-graph-tools-timescale.png" width="400">

The y-axis tools allow you to customize the way each series is displayed.

<img src="md/stats/img/report-graph-tools-y-axis.png" width="400">


---

The Tables

In the Table you can change the header text by simply clicking on it and typing a new text.

<img src="md/stats/img/report-report-line-text.png" width="600">
