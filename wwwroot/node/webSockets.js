/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const scheduler = require('./scheduler')
const mumpsClient = require('./mumpsClient')
const keepAlive = require('./keepAliveSession')
const WebSocket = require('../libs/node/ws.7.4.6')
const http = require('http')
const https = require('https')
const fs = require('fs')
const process = require('process')
const LOGGER = require('./logger')

let wss;
let mumpsServerStarted = false

const createServer = (port, ydb_dist, tls_info) => {
    let server

    if (tls_info !== '') {
        const tls = tls_info.split('~')

        server = new https.createServer({
            cert: fs.readFileSync(tls[0]),
            key: fs.readFileSync(tls[1]),
            passphrase: process.env.ydbguiServerInfo
        }).listen(port);

        process.env.ydbguiServerInfo = ''

    } else {
        server = new http.createServer({}).listen(port);
    }

    try {
        wss = new WebSocket.Server({server});

        LOGGER.log((tls_info === '' ? 'HTTP' : 'HTTPS') + " socket created on port: " + port, LOGGER.SUCCESS, true)

        setTimeout(() => {
            if (clientCount === 0) setInterval(() => process.exit(), 100)
        }, 3000)

    } catch (err) {

    }
    let clientCount = 0;

    wss.on('connection', async ws => {

        LOGGER.log('A new user attempting to connect...', LOGGER.INFO)

        if (clientCount > 0) {
            ws.terminate()

            LOGGER.log('Connection refused', LOGGER.ERROR)

            return
        }

        LOGGER.log('A new user just connected...', LOGGER.SUCCESS)

        clientCount += 1;

        ws.on('message', async function incoming(dataAsString) {
            if (ws.readyState === WebSocket.OPEN) {
                const req = JSON.parse(dataAsString)

                if (req.opCode === undefined) {
                    const errorResponse = JSON.stringify({
                        status: 'ERROR',
                        description: 'No opCode found'
                    })

                    ws.send(errorResponse)

                    LOGGER.log('No opCode found on request', LOGGER.ERROR)

                } else {
                    try {
                        // process the request
                        const res = await processRequest(req, ws, ydb_dist)

                        // and send the response back to the client
                        ws.send(JSON.stringify(res))

                    } catch (err) {
                        LOGGER.log('Error occurred while processing: ' + req.opCode, LOGGER.ERROR)
                    }
                }
            }
        });

        ws.on('close', () => {
            LOGGER.log('client got disconnected: Terminating server', LOGGER.INFO)

            clientCount -= 1;

            setInterval(() => process.exit(), 100)
        })

        ws.on('error', err => {
            LOGGER.log('Generic error occurred: ' + err.message + '. Terminating server.', LOGGER.ERROR)

            setInterval(() => process.exit(1), 100)
        })

        // setup the keepAlive
        keepAlive.start(ws, process, LOGGER)

    })

    wss.on('error', err => {
        LOGGER.log('Fatal error occurred in socket server: ' + err.message + '. Terminating server.', LOGGER.FATAL, true)

        setInterval(() => process.exit(1), 100)
    })
}

const processRequest = async (req, ws, ydb_dist) => {
    return new Promise(async function (resolve, reject) {
        switch (req.opCode) {
            case 'start': {
                try {
                    await mumpsClient.connect(ydb_dist)

                } catch (err) {
                    LOGGER.log('Can not start the MUMPS statsServer', LOGGER.FATAL)

                    return
                }
                resolve(scheduler.start(req.data, ws))

                break
            }
            case 'stop': {
                resolve(scheduler.stop())

                break
            }
            case 'pause': {
                resolve(scheduler.pause())

                break
            }
            case 'resume': {
                resolve(scheduler.resume(ws))

                break
            }
            case 'ping': {
                resolve({opCode: 'ping', data: 'pong', status: 'ACK'})

                break
            }
            case 'mping': {
                const res = JSON.parse(await mumpsClient.execute({opCode: 'mping'}))
                resolve(res)

                break
            }
            case 'pong': {

                break
            }

            case 'keepAlive': {
                keepAlive.ackStatus = true

                break
            }

            default: {
                LOGGER.log('opCode unknown: ' + req.opCode, LOGGER.ERROR)

                resolve({
                    opCode: req.opCode,
                    status: 'NAK',
                    error: {description: 'opCode unknown'}
                })
            }
        }
    })
}

module.exports.createServer = createServer;
