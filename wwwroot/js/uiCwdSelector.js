/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.cwdSelector = {
    init: function () {
        $('#btnCwdSelectorPathSelect').on('click', () => app.ui.cwdSelector.dirSelectClicked())

        $('#lblCwdSelectorPath')
            .on('keyup', e => app.ui.cwdSelector.pathChanged(e))
            .on('paste', e => app.ui.cwdSelector.pathChanged(e))

        $('#btnCwdSelectorOk').on('click', () => app.ui.cwdSelector.okPressed())
        $('#btnCwdSelectorValidate').on('click', () => app.ui.cwdSelector.validatePressed())

        app.ui.setupDialogForTest('modalCwdSelector')
    },

    show: function () {
        this.validated = false

        $('#lblCwdSelectorPath').val(app.ui.envSettings.currentCwd)
        app.ui.cwdSelector.currentCwd = app.ui.envSettings.currentCwd

        $('#modalCwdSelector').modal({show: true, backdrop: 'static'})
    },

    dirSelectClicked: function () {
        app.ui.dirSelect.show(this.currentCwd, this.dirSelectReturn)
    },

    dirSelectReturn: function (path) {
        app.ui.cwdSelector.currentCwd = path

        $('#lblCwdSelectorPath')
            .removeClass('is-invalid')
            .addClass('is-valid')
            .val(path)

        this.validated = true
        app.ui.button.enable($('#btnCwdSelectorOk'))
        app.ui.button.disable($('#btnCwdSelectorValidate'))

        app.ui.button.enable($('#btnCwdSelectorOk'))
    },

    pathChanged: function (e) {
        if ($('#lblCwdSelectorPath').val() === '') {
            if (e.originalEvent.type !== 'paste') {
                app.ui.button.enable($('#btnCwdSelectorOk'))
                app.ui.button.disable($('#btnCwdSelectorValidate'))

            } else {
                app.ui.button.disable($('#btnCwdSelectorOk'))
                app.ui.button.enable($('#btnCwdSelectorValidate'))
            }
        } else {
            app.ui.button.enable($('#btnCwdSelectorValidate'))
            app.ui.button.disable($('#btnCwdSelectorOk'))
        }

        $('#lblCwdSelectorPath').removeClass('is-valid is-invalid')

        const char = e.which || e.originalEvent.which

        if (char === 13) this.validatePressed()
    },

    validatePressed: async function () {
        try {
            const res = await app.REST._validatePath($('#lblCwdSelectorPath').val())

            if (res.result === 'OK' || (res.result === 'ERROR' && res.data.fileType === 'file')) {
                app.ui.msgbox.show('The path could not be validated...', 'WARNING')

                app.ui.button.disable($('#btnCwdSelectorOk'))

                $('#lblCwdSelectorPath')
                    .removeClass('is-valid')
                    .addClass('is-invalid')

            } else {
                app.ui.msgbox.show('The path has been validated...', 'Success')

                app.ui.button.enable($('#btnCwdSelectorOk'))
                app.ui.button.disable($('#btnCwdSelectorValidate'))

                $('#lblCwdSelectorPath')
                    .removeClass('is-invalid')
                    .addClass('is-valid')
            }

        } catch (err) {
            $('#lblCwdSelectorPath')
                .removeClass('is-valid')
                .addClass('is-invalid')

            app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')
        }
    },

    okPressed: function () {
        app.ui.envSettings.currentCwd = $('#lblCwdSelectorPath').val()
        $('#lblEnvSettingsCwd')
            .val(app.ui.envSettings.currentCwd)
            .removeClass('is-valid')
            .addClass('is-invalid')
        $('#valEnvSettingsCwdInvalid').text(app.ui.envSettings.gldFileSelectResponse.relativeFlag === false
            ? 'The current working directory has been set.'
            : 'You changed the current working directory.'
        )

        app.ui.button.disable($('#btnEnvSettingsSet'))
        app.ui.button.enable($('#btnEnvSettingsValidate'))

        $('#modalCwdSelector').modal('hide')

        app.ui.envSettings.buttonValidatePressed()
    },

    currentCwd: '',
    validated: false
}
