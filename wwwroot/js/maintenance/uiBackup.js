/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.backup.init = () => {
    $('#inpBackupTargetDir')
        .on('change', () => app.ui.backup.targetPathChanged())
        .on('keyup', () => app.ui.backup.targetPathChanged())
    $('#btnBackupSelectTarget').on('click', () => app.ui.backup.targetPathSelect())
    $('#btnBackupValidateTarget').on('click', () => app.ui.backup.targetPathValidate())

    $('#swBackupReplication').on('click', () => app.ui.backup.replicationClicked())

    $('#inpBackupReplicationTargetDir')
        .on('change', () => app.ui.backup.replTargetPathChanged())
        .on('keyup', () => app.ui.backup.replTargetPathChanged())
    $('#btnBackupSelectReplicationTarget').on('click', () => app.ui.backup.replTargetPathSelect())
    $('#btnBackupValidateReplicationTarget').on('click', () => app.ui.backup.replTargetPathValidate())

    $('#optBackupReplSamePath').on('click', () => app.ui.backup.replTargetModeSameChanged(true))
    $('#optBackupReplNewPath').on('click', () => app.ui.backup.replTargetModeSameChanged(false))

    $('#optBackupJournalLink').on('click', () => app.ui.backup.journalModeClicked('yesLink'))
    $('#optBackupJournalNoLink').on('click', () => app.ui.backup.journalModeClicked('noLink'))
    $('#optBackupJournalNone').on('click', () => app.ui.backup.journalModeClicked('no'))

    $('#btnBackupResultCopyToClipboard').on('click', () => app.ui.backup.copyToClipboardResult())
    $('#btnBackupOk').on('click', () => app.ui.backup.okPressed())

    app.ui.setupDialogForTest('modalBackup')
};

app.ui.backup.show = () => {
    let regions = [];

    // determine replication status
    Object.keys(app.system.regions).forEach((region) => {
        if (app.system.regions[region].dbFile) {
            const regionData = {};
            regionData.id = region;
            regionData.data = app.system.regions[region].replication.flags.status === 0 ? '' : 'REPL'
            //regionData.data = 'REPL'
            regionData.text = region + (regionData.data === '' ? '' : ' (' + regionData.data + ")");
            regionData.icon = false;
            regionData.state = {selected: false};

            regions.push(regionData)
        }
    });

    // populate the region list
    const tree = $("#treeBackupRegions");

    tree.jstree("destroy");
    tree.jstree({
        "checkbox": {
            "keep_selected_style": false
        },
        "plugins": ["checkbox"],
        'core': {
            'data': regions,
            'themes': {
                'name': 'proton',
            }
        },
    });
    tree.on("changed.jstree", (e, data) => {
        app.ui.backup.treeClicked();
    });

    // defaults
    const defaults = app.ui.storage.get('defaults')
    $('#inpBackupTargetDir')
        .removeClass('is-invalid')
        .removeClass('is-valid')
        .val(defaults.backup.target)
    if (defaults.backup.target === '') {
        $('#btnBackupValidateTarget').prop('disabled', true)
    } else {
        app.ui.backup.targetPathValidate()
    }

    $('#swBackupExistingFilesRename').prop('checked', true)

    $('#swBackupDisableJournaling').prop('checked', false)
    $('#swBackupRecord').prop('checked', false)

    $('#optBackupJournalLink').prop('checked', true)
    $('#swBackupSyncIo')
        .prop('checked', false)
        .prop('disabled', false)

    $('#inpBackupReplicationTargetDir').val(defaults.backup.replTarget)
    if (defaults.backup.replTarget === '') {
        $('#btnBackupValidateReplicationTarget').prop('disabled', true)
    } else {
        app.ui.backup.replTargetPathValidate()
    }
    $('#divBackupReplication').css('display', 'none')
    $('#swBackupReplication').prop('checked', true)
    $('#optBackupReplSamePath').prop('checked', true)
    $('#divBackupReplicationOptions').collapse('show')
    $('#divBackupReplicationTargetDir').collapse('hide')

    $('#btnBackupOk')
        .addClass('disabled')
        .prop('disabled', true)

    // display the dialog
    $('#modalBackup').modal({show: true, backdrop: 'static'})
}

app.ui.backup.journalModeClicked = type => {
    $('#swBackupSyncIo')
        .prop('disabled', type === 'no' ? true : false)
        .prop('checked', type === 'no' ? false : app.ui.backup.syncIo)
}

app.ui.backup.targetPathChanged = () => {
    const btnBackupValidateTarget = $('#btnBackupValidateTarget')
    const inpBackupTargetDir = $('#inpBackupTargetDir')

    inpBackupTargetDir
        .removeClass('is-valid')
        .removeClass('is-invalid')

    if (inpBackupTargetDir.val() !== '') {
        btnBackupValidateTarget.removeClass('disabled');
        btnBackupValidateTarget.prop("disabled", false)

    } else {
        btnBackupValidateTarget.addClass('disabled');
        btnBackupValidateTarget.prop("disabled", true)
    }

    app.ui.backup.checkOkStatus()
}

app.ui.backup.targetPathSelect = async () => {
    const inpBackupTargetDir = $('#inpBackupTargetDir')
    let path = inpBackupTargetDir.val() === '' ? '/' : inpBackupTargetDir.val()

    // validate the path first
    if (path !== '') {
        try {
            const res = await app.REST._pathExpand(path, true)

            if (res.result === 'OK') {
                path = res.data

            } else {
                if (res.error.code === -1) {
                    app.ui.msgbox.show('The path: ' + path + ' is not a valid path.', 'ERROR')

                } else {
                    app.ui.msgbox.show('The path: ' + path + ' is a file and not a directory.', 'ERROR')
                }

                return
            }

        } catch (err) {
            app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')

        }
    }

    app.ui.dirSelect.show(path, app.ui.dirSelectOk)
}

// callback
app.ui.dirSelectOk = path => {
    $('#inpBackupTargetDir').val(path)

    $('#inpBackupTargetDir')
        .removeClass('is-invalid')
        .addClass('is-valid')
    $('#btnBackupValidateTarget')
        .addClass('disabled')
        .prop('disabled', true)

    app.ui.backup.checkOkStatus()
}

app.ui.backup.targetPathValidate = async () => {
    const inpBackupTargetDir = $('#inpBackupTargetDir')
    const btnBackupValidateTarget = $('#btnBackupValidateTarget')

    try {
        const res = await app.REST._pathExpand(inpBackupTargetDir.val(), true)

        if (res.result === 'OK') {
            inpBackupTargetDir
                .removeClass('is-invalid')
                .addClass('is-valid')

            btnBackupValidateTarget
                .addClass('disabled')
                .prop('disabled', true)

        } else {
            if (res.error.code === -1) {
                app.ui.msgbox.show('The path: ' + inpBackupTargetDir.val() + ' is not a valid path.', 'ERROR')

            } else {
                app.ui.msgbox.show('The path: ' + inpBackupTargetDir.val() + ' is a file and not a directory.', 'ERROR')
            }

            inpBackupTargetDir
                .removeClass('is-valid')
                .addClass('is-invalid')

            return
        }

    } catch (err) {
        app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')
    }

    app.ui.backup.checkOkStatus()
}

app.ui.backup.validateOk = () => {
    const tree = $("#treeBackupRegions");
    const btn = $('#btnBackupOk');

    if (tree.jstree("get_checked").length !== 0) {
        btn.removeClass('disabled');
        btn.prop("disabled", false)
    } else {
        btn.addClass('disabled');
        btn.prop("disabled", true)
    }
}

app.ui.backup.syncIo = false
app.ui.backup.treeClicked = async () => {
    const selElem = $("#treeBackupRegions").jstree("get_checked", true);
    const divBackupReplication = $('#divBackupReplication')
    let replFound = false;
    let envVarFound = ''

    selElem.forEach(el => {
        if (el.data === 'REPL') replFound = true

        // get syncIO status
        app.ui.backup.syncIo = app.system.regions[el.id].journal.data[4].SYNC_IO
    });

    $('#swBackupSyncIo').prop('checked', app.ui.backup.syncIo)

    if (replFound === true) {
        // check if env var exist
        app.system.systemInfo.envVars.forEach(envVar => {
            if ((envVar.name === 'ydb_repl_instance' || envVar.name === 'gtm_repl_instance') && envVar.value !== '') {
                envVarFound = envVar.value
            }
        })

        // and verify that file exists
        if (envVarFound !== '') {
            try {
                const res = await app.REST._pathExpand(envVarFound, false)

                if (res.result === 'ERROR') {
                    divBackupReplication.css('display', 'none')
                    app.ui.msgbox.show('The path: ' + envVarFound + ' is not valid.<br>Replication options will be disabled.', 'WARNING')

                } else {
                    // file exists, all good
                    divBackupReplication.css('display', 'block')

                }
            } catch (err) {
                divBackupReplication.css('display', 'none')
                app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')
            }

        } else {
            divBackupReplication.css('display', 'none')
            app.ui.msgbox.show('Replication is not properly configured. The env var $ydb_repl_instance or $gtm_repl_instance is missing<br>Replication options will be disabled.', 'WARNING')
        }
    } else {
        divBackupReplication.css('display', 'none')
    }

    app.ui.backup.checkOkStatus()
}

app.ui.backup.existingFilesClick = () => {
    const lblBackupExistingFiles = $('#lblBackupExistingFiles')

    if ($('#swBackupExistingFiles').prop('checked') === true) {
        lblBackupExistingFiles.html('&nbsp;&nbsp;Overwrite')

    } else {
        lblBackupExistingFiles.html('&nbsp;&nbsp;Rename')
    }
}

app.ui.backup.checkOkStatus = () => {
    const btnBackupOk = $('#btnBackupOk')

    // at least one region selected and a validated target path
    if ($("#treeBackupRegions").jstree("get_checked").length !== 0
        && $('#inpBackupTargetDir').hasClass('is-valid')
        && ($('#divBackupReplication').css('display') === 'none'
            || ($('#divBackupReplication').css('display') === 'block'
                && $('#swBackupReplication').prop('checked') === false)
            || ($('#divBackupReplication').css('display') === 'block'
                && $('#swBackupReplication').prop('checked') === true
                && $('#optBackupReplSamePath').prop('checked') === true)
            || ($('#divBackupReplication').css('display') === 'block'
                && $('#swBackupReplication').prop('checked') === true
                && $('#optBackupReplNewPath').prop('checked') === true
                && $('#inpBackupReplicationTargetDir').hasClass('is-valid'))
        )
    ) {
        btnBackupOk.removeClass('disabled');
        btnBackupOk.prop("disabled", false)

    } else {
        btnBackupOk.addClass('disabled');
        btnBackupOk.prop("disabled", true)
    }
}

app.ui.backup.replicationClicked = () => {
    $('#divBackupReplicationOptions').collapse($('#swBackupReplication').prop('checked') === true ? 'show' : 'hide')
    $('#optBackupReplSamePath').prop('checked', true)
    $('#divBackupReplicationTargetDir').collapse('hide')

    app.ui.backup.checkOkStatus()
}

app.ui.backup.replTargetModeSameChanged = val => {
    $('#divBackupReplicationTargetDir').collapse(val === true ? 'hide' : 'show')

    app.ui.backup.checkOkStatus()

}

app.ui.backup.replTargetPathChanged = () => {
    const btnBackupValidateReplicationTarget = $('#btnBackupValidateReplicationTarget')
    const inpBackupReplicationTargetDir = $('#inpBackupReplicationTargetDir')

    inpBackupReplicationTargetDir
        .removeClass('is-valid')
        .removeClass('is-invalid')

    if (inpBackupReplicationTargetDir.val() !== '') {
        btnBackupValidateReplicationTarget.removeClass('disabled');
        btnBackupValidateReplicationTarget.prop("disabled", false)

    } else {
        btnBackupValidateReplicationTarget.addClass('disabled');
        btnBackupValidateReplicationTarget.prop("disabled", true)
    }

    app.ui.backup.checkOkStatus()
}

app.ui.backup.replTargetPathSelect = async () => {
    const inpBackupReplicationTargetDir = $('#inpBackupReplicationTargetDir')
    let path = inpBackupReplicationTargetDir.val() === '' ? '/' : inpBackupReplicationTargetDir.val()

    // validate the path first
    if (path !== '') {
        try {
            const res = await app.REST._pathExpand(path)

            if (res.result === 'OK') {
                path = res.data

            } else {
                if (res.error.code === -1) {
                    app.ui.msgbox.show('The path: ' + path + ' is not a valid path / filename.', 'ERROR')

                } else {
                    app.ui.msgbox.show('The path: ' + path + ' is a file and not a directory.', 'ERROR')
                }

                return
            }

        } catch (err) {
            app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')

        }
    }

    app.ui.dirSelect.show(path, app.ui.replDirSelectOk)
}

// callback
app.ui.replDirSelectOk = path => {
    $('#inpBackupReplicationTargetDir').val(path)

    $('#inpBackupReplicationTargetDir')
        .removeClass('is-invalid')
        .addClass('is-valid')
    $('#btnBackupValidateReplicationTarget')
        .addClass('disabled')
        .prop('disabled', true)

    app.ui.backup.checkOkStatus()
}

app.ui.backup.replTargetPathValidate = async () => {
    const inpBackupReplicationTargetDir = $('#inpBackupReplicationTargetDir')
    const btnBackupValidateReplicationTarget = $('#btnBackupValidateReplicationTarget')

    try {
        const res = await app.REST._pathExpand(inpBackupReplicationTargetDir.val(), true)

        if (res.result === 'OK') {
            inpBackupReplicationTargetDir
                .removeClass('is-invalid')
                .addClass('is-valid')

            btnBackupValidateReplicationTarget
                .addClass('disabled')
                .prop('disabled', true)

        } else {
            if (res.error.code === -1) {
                app.ui.msgbox.show('The path: ' + inpBackupReplicationTargetDir.val() + ' is not a valid path.', 'ERROR')

            } else {
                app.ui.msgbox.show('The path: ' + inpBackupReplicationTargetDir.val() + ' is a file and not a directory.', 'ERROR')
            }

            inpBackupReplicationTargetDir
                .removeClass('is-valid')
                .addClass('is-invalid')

            return
        }

    } catch (err) {
        app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')
    }

    app.ui.backup.checkOkStatus()
}

app.ui.backup.okPressed = () => {
    let backupText = 'The system wil backup the regions: '

    const selElem = $("#treeBackupRegions").jstree("get_checked", true);
    selElem.forEach(el => {
        backupText += el.id + ' '
    });

    backupText += '<br>to the path: ' + $('#inpBackupTargetDir').val() + '<br><br>'

    const runBackground = $('#swBackupRunBackground').is(':checked');
    const bgndText = runBackground ? 'You have chosen to run the backup in the background. When it will complete it will alert you with the result.<br>' : '';

    app.ui.inputbox.show(backupText + bgndText + 'Do you want to proceed with the backup operation ?', 'Backup', async (opt) => {
        if (opt === 'YES') {
            const payload = {}
            const selRegions = [];

            const selElem = $("#treeBackupRegions").jstree("get_checked", true);
            selElem.forEach(el => {
                selRegions.push(el.id)
            });
            payload.regions = selRegions;

            payload.targetPath = $('#inpBackupTargetDir').val()
            payload.replace = $('#swBackupExistingFilesReplace').prop('checked')
            payload.disableJournaling = $('#swBackupDisableJournaling').prop('checked')
            payload.record = $('#swBackupRecord').prop('checked')
            payload.createNewJournalFiles = $('#optBackupJournalLink').prop('checked') === true ? 'link' : $('#optBackupJournalNoLink').prop('checked') === true ? 'nolink' : 'no'
            payload.syncIo = $('#swBackupSyncIo').prop('checked')

            if ($('#divBackupReplication').css('display') === 'block') {
                payload.includeReplicatedInstances = $('#swBackupReplication').prop('checked')

                if (payload.includeReplicatedInstances === true) {
                    if ($('#optBackupReplSamePath').prop('checked') === true) {
                        payload.replUseSamePath = true

                    } else {
                        payload.replUseSamePath = false
                        payload.replTargetPath = $('#inpBackupReplicationTargetDir').val()
                    }
                }

            } else {
                payload.includeReplicatedInstances = false
            }

            // proceed with backup
            console.log(payload)
            $('#modalBackup').modal('hide');

            if (runBackground === false) {
                app.ui.wait.show("Backing up the selected regions...");

            } else {
                app.ui.toaster.startInterval('Backup')
            }

            try {
                const res = await app.REST._backup(payload)

                if (runBackground) {
                    if (res.result !== undefined && res.result === 'OK') {
                        app.ui.toaster.createToast('backup', 'Backup', 'Click to display the report...', 'success', res);

                    } else {
                        app.ui.toaster.createToast('backup', 'Backup', 'Click to display the report...', 'fatal', res);
                    }
                } else {
                    app.ui.wait.hide();

                    if (res.result === 'OK') {
                        // display the report
                        app.ui.backup.displayResult(res)
                    } else {
                        let msg = res.error.description

                        if (res.error.dump !== undefined) {
                            msg += '<br>'

                            res.error.dump.forEach(el => {
                                if (el !== '') {
                                    msg += '<br>' + el
                                }
                            })

                            app.ui.msgbox.show(msg, 'ERROR')
                        }
                    }
                }

            } catch (err) {
                app.ui.wait.hide();
                console.log(err)
                app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')
            }
        }
    })
}

app.ui.backup.displayResult = res => {
    $('#divBackupResult')
        .empty()
        .append('' + res.data.join('<br><br>'))

    // display the dialog
    $('#modalBackupResult').modal({show: true, backdrop: 'static'})
}

app.ui.backup.copyToClipboardResult = () => {
    let clipboard = $("#divBackupResult").html().replace(/<br>/g, '\n')

    app.ui.copyToClipboard(clipboard, $('#btnBackupResultCopyToClipboard'))
}
