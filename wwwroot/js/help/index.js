/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// *******************************
// Initialization
// *******************************
window.onload = () => {
    const page = window.location.search.split('?')[1]
    const path = './md/' + page + '.md'

    app.init()

    app.displayPage(path)

    $('body').on('keydown', e => app.mainKeyHandler(e))
}

app = {
    init: function () {
        this.converter = new showdown.Converter({
            emoji: true,
            tables: true,
            parseImgDimensions: true,
            ghCompatibleHeaderId: true,
            tablesHeaderId: true,
            tasklists: true,
            smoothLivePreview: true,
            underline: true,
        });

        this.converter.setFlavor('github');
    },

    displayPage: async function (page) {
        const md = await this.loadServerFile(page)
        let mdHtml = this.converter.makeHtml(md);

        $('#divHelpContent').html(mdHtml);
    },

    loadServerFile: async function (filePath) {
        return new Promise(function (resolve, reject) {
            try {
                fetch(filePath).then(response => {
                    return response.text();
                }).then(data => {
                    resolve(data)
                })

            } catch (err) {
                console.log('File not found: ' + filePath)
                reject()
            }
        })
    },

    link: function (page) {
        const path = window.location.origin + '/help/index.html?'
        window.open(path + page, 'ydbgui_help')
    },

    mainKeyHandler: function (e) {
        if (e.code === 'Backspace') {
            // needs two history.back() to navigate back
            window.history.back()
            window.history.back()
        }
    },

    converter: {}
}
