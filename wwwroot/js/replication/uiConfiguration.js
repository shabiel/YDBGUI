/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.topology.configuration = {
    import: async function (e) {
        const file = e.target.files[0]

        try {
            const res = await (this.fileReader(file))

            app.userSettings.defaults.replication.discoveryService.servers = res
            app.ui.storage.save('defaults', app.userSettings.defaults)

            app.ui.msgbox.show('The new Replication settings have been loaded and saved', 'Success')

        } catch (err) {
            app.ui.msgbox.show(app.REST.parseError(err), 'Error')
        }

        e.target.value = ''
    },

    fileReader: function (file) {
        return new Promise(function (resolve, reject) {
            const reader = new FileReader()

            reader.addEventListener('load', (event) => {
                try {
                    const data = JSON.parse(reader.result)

                    if (Array.isArray(data) === false ||
                        data.length !== 16 ||
                        data[0].instance === undefined) {
                        reject('Not a valid Replication configuration file...')

                    } else resolve(data)

                } catch (err) {
                    reject(err)
                }
            });

            reader.addEventListener('error', (event) => {
                reject(reader.error)
            });

            reader.readAsText(file)
        })
    },

    export: async function () {
        let found = false

        app.userSettings.defaults.replication.discoveryService.servers.forEach(server => {
            if (server.instance !== '' && server.host !== '' && server.port !== 0) found = true
        })

        if (found === false) {
            app.ui.msgbox.show('No configuration found...', 'Error')

            return
        }

        const data = JSON.stringify(app.userSettings.defaults.replication.discoveryService.servers)

        if (navigator.userAgent.indexOf("Firefox") != -1) {
            // *************************************
            // firefox
            // *************************************
            try {
                let blob = new Blob([data], {
                    type: "script"
                })
                let a = document.createElement("a")
                a.href = URL.createObjectURL(blob)
                a.download = 'ydb_replication_settings.json'
                a.hidden = true
                document.body.appendChild(a)
                a.innerHTML =
                    ""
                a.click()
                a.remove()
            } catch (err) {
                app.ui.msgbox.show(app.REST.parseError(err), 'Warning')
            }

        } else if (navigator.userAgent.indexOf("Chrome") != -1) {
            // *************************************
            // chrome
            // *************************************
            try {
                const handle = await showSaveFilePicker({
                    suggestedName: 'ydb_replication_settings.json',
                    types: [{
                        description: 'YottaDB Replication Configuration',
                        accept: {'text/json': ['.json']},
                    }],
                });

                const blob = new Blob([data]);

                const writableStream = await handle.createWritable();
                await writableStream.write(blob);
                await writableStream.close();

            } catch (err) {
                if (err.toString().indexOf('aborted') > -1) return

                const errorMessage = app.REST.parseError(err)

                app.ui.msgbox.show(errorMessage.indexOf('showSaveFilePicker') > -1 ? 'This option is available only when using HTTP. Consider using Firefox for this operation' : errorMessage, 'Warning')
            }

        } else {
            app.ui.msgbox.show('Your browser is not supported for this operation.<br><br>Please report your browser type and vendor to YDB support.', 'Warning')
        }
    }
}
