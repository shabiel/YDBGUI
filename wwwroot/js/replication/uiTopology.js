/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.topology = {
    showTab: function () {
        // verify that we have a valid configuration, otherwise display message with instructions
        let found = false

        app.userSettings.defaults.replication.discoveryService.servers.forEach(server => {
            if (server.instance !== '' && server.host !== '' && server.port !== 0) found = true
        })

        if (found === false) {
            let msg = 'No Topology configuration was found.<br><br>Please, configure your Topology and try again...<br><br>'

            msg += 'You can follow the instructions on this ' +
                '<span class="repl-help-link" onclick="window.open(window.location.origin + \'/help/index.html?replication/configuration/configuration\')">' +
                'help page</span>'

            app.ui.msgbox.show(msg, 'Configuration error')

            return
        }

        app.ui.tabs.select('-T');
    },

    createInstance: function (type) {
        const newViewer = $('#tabTopology').clone();
        const newTabId = 'tab' + type + 'Div';

        $('#tabsDivsContainer').append(newViewer);

        $('#tabsDivsContainer > #tabTopology').prop('id', newTabId);
        const newTabDiv = $('#' + newTabId);

        newTabDiv
            .attr('aria-labelledby', 'tab' + type)
            .css('display', 'block')

        // regenerate id's
        newTabDiv.find('#divTopologyPaperTab').attr('id', 'divTopologyPaper');

        // mount handlers
        // toolbar switch
        $('#btnTopologyToolbarSwitchToRun').on('click', () => this.toolbar.switchToRun())
        $('#btnTopologyToolbarSwitchToDesign').on('click', () => this.toolbar.switchToDesign())

        // toolbar DESIGN
        $('#btnTopologyToolbarUndo').on('click', () => app.ui.replication.topology.joint.undoRedo.undo())
        $('#btnTopologyToolbarRedo').on('click', () => app.ui.replication.topology.joint.undoRedo.redo())
        $('#btnTopologyToolbarZoomOut').on('click', () => this.toolbar.zoomOut())
        $('#btnTopologyToolbarZoomIn').on('click', () => this.toolbar.zoomIn())
        $('#btnTopologyToolbarAutoArrangeButton').on('click', () => this.toolbar.autoArrange())
        $('#optReplLayoutArrangeLr').on('click', () => this.toolbar.autoArrange('LR'))
        $('#optReplLayoutArrangeTb').on('click', () => this.toolbar.autoArrange('TB'))
        $('#dmReplArrangeMenu').on('click', e => e.stopPropagation())
        $('#btnReplLayoutArrangeRankSepDown').on('click', () => this.toolbar.rankSeparation('down'))
        $('#btnReplLayoutArrangeRankSepUp').on('click', () => this.toolbar.rankSeparation('up'))
        $('#btnReplLayoutArrangeNodeSepDown').on('click', () => this.toolbar.nodeSeparation('down'))
        $('#btnReplLayoutArrangeNodeSepUp').on('click', () => this.toolbar.nodeSeparation('up'))
        $('#lblReplLayoutArrangeRankSep').text(app.ui.replication.topology.joint.actions.arrangeData.rankSep)
        $('#lblReplLayoutArrangeNodeSep').text(app.ui.replication.topology.joint.actions.arrangeData.nodeSep)
        $('#btnSTopologyHelp1').on('click', () => this.toolbar.help(1))
        $('#btnSTopologyHelp2').on('click', () => this.toolbar.help(2))

        if (app.userSettings.defaults.replication.topology.layout === 'LR') {
            $('#optReplLayoutArrangeLr').prop("checked", true)
        } else {
            $('#optReplLayoutArrangeTb').prop("checked", true)
        }

        $('#btnTopologyToolbarAutoArrangeButton')
            .on('click', () => this.toolbar.autoArrange($('#btnTopologyToolbarAutoArrangeButton').text()))
            .text(app.userSettings.defaults.replication.topology.layout)

        $('#btnTopologyToolbarFitToContent').on('click', () => this.toolbar.fitToContent())

        $('#selTopologyToolbarRouter').on('change', () => this.toolbar.linkRouterChange())
        $('#selTopologyToolbarConnector').on('change', () => this.toolbar.linkConnectorChange())

        // toolbar RUN
        $('#btnTopologyToolbarLoad').on('click', () => this.toolbar.load())
        $('#btnTopologyToolbarSave').on('click', () => this.toolbar.save())

        $('#selTopologyToolbarRefresh')
            .on('change', () => this.toolbar.refresh())
            .val(app.userSettings.defaults.replication.topology.refresh)
        if (app.userSettings.defaults.replication.topology.refresh === '0') $('#ledTopologyToolbarRefresh').addClass('led-gray')

        $('#divTopologyPaper')
            .css('height', 'calc(100vh - 150px)')
            .css('width', '100%')

        this.openTab()

        app.ui.replication.topology.closed = false
    },

    openTab: async function () {
        try {
            app.ui.wait.show('Discovering replication topology...', () => {
                app.ui.tabs.close('tab-T')
            })

            let res = {}
            try {
                app.userSettings.defaults.replication.discoveryService.responseType = 'F'
                res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

            } catch (err) {
                console.log(err)
                app.ui.wait.hide()

                app.ui.msgbox.show('Can not connect to the main server... <br>Aborting...', 'Fatal')
                app.ui.tabs.close('tab-T')

                return
            }

            delete res.result

            // initialize the toolbar objects
            // refresh
            this.refresh.value = app.userSettings.defaults.replication.topology.refresh

            // router
            $('#selTopologyToolbarRouter option[value="' + app.userSettings.defaults.replication.topology.links.router + '"]').prop('selected', true)

            // connector
            $('#selTopologyToolbarConnector option[value="' + app.userSettings.defaults.replication.topology.links.connector + '"]').prop('selected', true)

            // initialize the joint.js graph
            await app.ui.replication.topology.joint.init(res)

            // start the timers if needed
            if (this.refresh.value !== '0') {
                this.refresh.start()
            }

            app.ui.wait.hide()

        } catch (err) {
            console.log(err)
            app.ui.wait.hide()
        }

        app.ui.setTestClosure('tab-TDiv')
    },

    closeTab: function () {
        // terminate eventual running timers
        this.refresh.stop()

        app.ui.replication.topology.closed = true
    },

    // toolbar actions
    toolbar: {
        // switch between toolbars
        switchToDesign: function () {
            $('#divTopologyToolbarDesign').css('display', 'block')
            $('#divTopologyToolbarRun').css('display', 'none')
        },

        switchToRun: function () {
            $('#divTopologyToolbarDesign').css('display', 'none')
            $('#divTopologyToolbarRun').css('display', 'block')

        },

        // Toolbar DESIGN
        zoomIn: function (step = 0.1) {
            const currentScale = app.ui.replication.topology.joint.paper.scale().sx

            app.ui.replication.topology.joint.paper.scale(currentScale + step)

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        zoomOut: function (step = 0.1) {
            const currentScale = app.ui.replication.topology.joint.paper.scale().sx

            app.ui.replication.topology.joint.paper.scale(currentScale - step)

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        rankSeparation: function (direction) {
            const arrangeData = app.ui.replication.topology.joint.actions.arrangeData

            if ((arrangeData.rankSep === 50 && direction === 'down') || (arrangeData.rankSep === 2000 && direction === 'up')) return

            arrangeData.rankSep = direction === 'up' ? arrangeData.rankSep + 10 : arrangeData.rankSep - 10
            this.resetAllLinks()
            app.ui.replication.topology.joint.actions.arrange()

            $('#lblReplLayoutArrangeRankSep').text(arrangeData.rankSep)

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        nodeSeparation: function (direction) {
            const arrangeData = app.ui.replication.topology.joint.actions.arrangeData

            if ((arrangeData.nodeSep === 50 && direction === 'down') || (arrangeData.nodeSep === 2000 && direction === 'up')) return

            arrangeData.nodeSep = direction === 'up' ? arrangeData.nodeSep + 10 : arrangeData.nodeSep - 10
            this.resetAllLinks()
            app.ui.replication.topology.joint.actions.arrange()

            $('#lblReplLayoutArrangeNodeSep').text(arrangeData.nodeSep)

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        autoArrange: function (type) {
            if (type) {
                app.ui.replication.topology.joint.actions.arrangeData.rankDir = type
                this.resetAllLinks()
                app.ui.replication.topology.joint.actions.arrange()

                $('#btnTopologyToolbarAutoArrangeButton').text(type)

                app.ui.replication.topology.joint.undoRedo.saveEntry()
            }
        },

        fitToContent: function () {
            app.ui.replication.topology.joint.actions.fitToContent()

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        linkRouterChange: function () {
            const router = $('#selTopologyToolbarRouter')
            const connector = $('#selTopologyToolbarConnector')
            app.ui.replication.topology.joint.graph.getLinks().forEach(link => {
                link.router(router.val())
            })

            switch (router.val()) {
                case 'manhattan':
                case 'orthogonal':
                case 'metro': {
                    connector.val('straight')
                    this.linkConnectorChange()
                }
            }

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        linkConnectorChange: function () {
            app.ui.replication.topology.joint.graph.getLinks().forEach(link => {
                link.connector($('#selTopologyToolbarConnector').val())
            })

            app.ui.replication.topology.joint.undoRedo.saveEntry()
        },

        // Toolbar RUN
        load: function () {
            app.ui.replication.storage.load.show()
        },

        save: function () {
            app.ui.replication.storage.save.show()
        },

        refresh: function () {
            const time = parseInt($('#selTopologyToolbarRefresh').val())

            if (time > 0) {
                app.ui.replication.topology.refresh.stop()
                app.ui.replication.topology.refresh.value = time
                app.ui.replication.topology.refresh.start()

            } else {
                app.ui.replication.topology.refresh.stop()
            }
        },

        resetAllLinks: function () {
            const graph = app.ui.replication.topology.joint.graph.toJSON()

            graph.cells.forEach(cell => {
                if (cell.type === 'replLink') {
                    const link = app.ui.replication.topology.joint.paper.findViewByModel((app.ui.replication.topology.joint.findLinkById(cell.id)))
                    for (let cnt = 0; cnt < link.path.segments.length + 2; cnt++) {
                        link.model.removeVertex(-1)
                    }
                }
            })
        },

        help: function (type) {
            const page = 'replication/toolbars/' + (type === 1 ? 'toolbar-design' : 'toolbar-run')
            const path = window.location.origin + '/help/index.html?'

            window.open(path + page, 'ydbgui_help')
        }
    },

    // this section manages the Backlog refresh timers.
    // at initialization, this.value gets populated with the default value.
    // when user changes the value, it will set the this.value, then call the start.
    // the timer handle is stored in this.handle to be used for timer reset.
    // An implicit stop() is executed when closing the Topology tab.
    refresh: {
        start: function () {
            this.handle = setInterval(async () => {
                // stop eventual dashboard backlog refresh
                if (parseInt(app.userSettings.defaults.replication.dashboard.refreshBacklog) > 0) {
                    app.ui.dashboard.refreshBacklog.stop()
                }

                try {
                    app.userSettings.defaults.replication.discoveryService.responseType = 'HB'
                    const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

                    app.ui.replication.topology.joint.refresh(res)

                    // led activity
                    $('#ledTopologyToolbarRefresh').removeClass('led-gray')
                    $('#ledTopologyToolbarRefresh').addClass('led-green')
                    setTimeout(() => {
                        $('#ledTopologyToolbarRefresh').removeClass('led-green')

                    }, 50)

                } catch (err) {
                    let res = {}
                    res[app.system.replication.instanceFile.flags.instanceName] = {
                        ERROR: 'server_not_reachable'
                    }
                    app.ui.replication.topology.joint.refresh(res)
                }

            }, parseInt(this.value) * 1000)
        },

        stop: function () {
            if (this.handle === 0) return

            // restart eventual dashboard backlog refresh
            if (parseInt(app.userSettings.defaults.replication.dashboard.refreshBacklog) > 0) {
                app.ui.dashboard.refreshBacklog.start()
            }

            clearInterval(this.handle)

            $('#ledTopologyToolbarRefresh').addClass('led-gray')

            this.handle = 0
        },

        handle: 0,
        value: 0
    },

    closed: true
}
