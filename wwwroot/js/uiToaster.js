/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

let Toast = class {
    constructor() {
        this.type = '';
        this.title = '';
        this.level = '';
        this.payload = null;
    }
};

let Task = class {
    constructor() {
        this.title = '';
    }
};

app.ui.toaster.tasksList = [];

app.ui.toaster.toasts = [];
app.ui.toaster.toastIx = -1;
app.ui.toaster.tasks = 0

// **********************************************
// create toast
// **********************************************
app.ui.toaster.createToast = (type = '', title, msg, level = '', payload) => {
// type: defrag, repair, backup, restore or empty string or null
// title: the toast title
// msg: the msg to be displayed. Can be inline HTML
// class: the string describing the color to be displayed: info, success, warning, error, fatal
    // if omitted or wrong value, info will be used
// payload: object, string or null

    app.ui.toaster.toastIx += 1;

    let background;
    let color = 'black';
    let lTitle = '';

    switch (level) {
        case 'success': {
            background = '#e4f5e5';
            color = 'var(--ydb-status-green)';
            lTitle = 'Success: ';
            break
        }
        case 'warning': {
            background = '#f3dada';
            color = 'var(--ydb-status-red)';
            lTitle = 'Warning: ';
            break
        }
        case 'error': {
            background = '#f3dada';
            color = 'var(--ydb-status-red)';
            lTitle = 'Error: ';
            break
        }
        case 'fatal': {
            background = '#f3dada';
            color = 'var(--ydb-status-red)';
            lTitle = 'Fatal: ';
            break
        }
        case 'info':
        case '': {
            background = '#e4f5e5';
            color = 'var(--ydb-status-green)';
            lTitle = 'Info: ';
            break
        }
        default:
            background = '#e4f5e5'
            color = 'var(--ydb-status-green)';
    }

    // Create new instance of data
    app.ui.toaster.toasts[app.ui.toaster.toastIx] = new Toast;

    app.ui.toaster.toasts[app.ui.toaster.toastIx].type = type
    app.ui.toaster.toasts[app.ui.toaster.toastIx].title = title

    // and HTML
    const newToast = $('#toastMaster').clone();
    const newId = app.ui.toaster.toastIx.toString();

    $('#toastContainer').append(newToast);

    $('#toastContainer > #toastMaster').prop('id', 'toastMaster-' + newId);
    const newToastDiv = $('#' + 'toastMaster-' + newId);
    const toastHeader = $('#toastHeader-' + newId);

    // rename the entries
    newToastDiv.find('#toastHeader').attr('id', 'toastHeader-' + newId);
    newToastDiv.find('#toastTitle').attr('id', 'toastTitle-' + newId);
    newToastDiv.find('#toastBody').attr('id', 'toastBody-' + newId);
    newToastDiv.find('#toastCloseButton').attr('id', 'toastCloseButton-' + newId);
    newToastDiv.find('#toastTimestamp').attr('id', 'toastTimestamp-' + newId);

    toastHeader
        .css('background', background)
        .css('color', color);
    $('#toastTitle-' + newId).text(lTitle + title);
    $('#toastTimestamp-' + newId).text(new Date().toLocaleTimeString('en-US', {hour12: false}));

    $('#toastCloseButton-' + newId)
        .css('color', color)
        .on('click', () => app.ui.toaster.deleteToast(newId));

    app.ui.toaster.toasts[app.ui.toaster.toastIx].payload = payload || {};
    switch (type) {
        case 'integ': {
            $('#toastBody-' + newId).append(
                $('<a/>', {
                    id: 'btnToastBody-' + newId,
                    style: 'color: var(--ydb-purple)!important;',
                    href: '#',
                    onclick: 'app.ui.integ.displayResult( app.ui.toaster.toasts[' + newId + '].payload)',
                    html: msg
                })
            );
            break
        }
        case 'defrag': {
            $('#toastBody-' + newId).append(
                $('<a/>', {
                    id: 'btnToastBody-' + newId,
                    style: 'color: var(--ydb-purple)!important;',
                    href: '#',
                    onclick: 'app.ui.defrag.displayResult( app.ui.toaster.toasts[' + newId + '].payload)',
                    html: msg
                })
            );
            break
        }
        case 'backup': {
            $('#toastBody-' + newId).append(
                $('<a/>', {
                    id: 'btnToastBody-' + newId,
                    style: 'color: var(--ydb-purple)!important;',
                    href: '#',
                    onclick: 'app.ui.backup.displayResult( app.ui.toaster.toasts[' + newId + '].payload)',
                    html: msg
                })
            );
            break
        }

        default: {
            $('#toastBody-' + newId).append(
                $('<a/>', {
                    style: 'color: var(--ydb-purple)!important;',
                    href: '#',
                    html: msg
                })
            )
        }
    }

    app.ui.toaster.tasks--

    // delete task from list
    app.ui.toaster.tasksList.forEach((task, ix) => {
        if (task.title === title) {
            delete app.ui.toaster.tasksList[ix]
        }
    })

    if (app.ui.toaster.tasks === 0) {
        app.ui.toaster.stopInterval()

    } else {
        $('#puAsync').attr('data-content', app.ui.toaster.buildTaskList())

        app.ui.regeneratePopups()
    }
};

app.ui.toaster.deleteToast = (id) => {
    app.ui.toaster.toasts.splice([parseInt(id)], 1);

    $('#toastMaster-' + id.toString()).remove();
};

app.ui.toaster.timerId = 0;
app.ui.toaster.startInterval = (title = '') => {
    app.ui.toaster.tasksList[app.ui.toaster.tasksList.length] = new Task;
    app.ui.toaster.tasksList[app.ui.toaster.tasksList.length - 1].title = title
    $('#puAsync')
        .css('display', 'block')
        .attr('data-original-title', 'Task list')
        .attr('data-content', app.ui.toaster.buildTaskList())

    app.ui.regeneratePopups()

    app.ui.toaster.tasks++

    if (app.ui.toaster.timerId === 0) {
        let toggle = false
        app.ui.toaster.timerId = setInterval(() => {
            const lblAsync = $('#lblAsync')
            if (toggle === true) {
                toggle = false
                lblAsync
                    .css('background-color', 'white')
                    .css('color', 'var(--ydb-purple)')

            } else {
                toggle = true

                lblAsync.css('background-color', 'var(--ydb-status-red)')
                lblAsync[0].style.removeProperty('color')
                lblAsync[0].style.setProperty('color', 'white', 'important')
            }
        }, 500)
    }
}

app.ui.toaster.stopInterval = () => {
    clearInterval(app.ui.toaster.timerId)
    app.ui.toaster.timerId = 0

    $('#puAsync')
        .css('display', 'none')
        .attr('data-original-title', '')
}

app.ui.toaster.buildTaskList = () => {
    let taskList = ''

    app.ui.toaster.tasksList.forEach((toast, ix) => {
        taskList += (ix + 1) + ' - <strong>' + toast.title + '</strong><br>'
    })
    return taskList
}
