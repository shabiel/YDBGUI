/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.dirSelect.init = () => {
    $('#selDirSelectList')
        .on('click', () => app.ui.dirSelect.listClick())
        .on('dblclick', () => app.ui.dirSelect.listDblClick())
    $('#btnDirSelectOk').on('click', () => app.ui.dirSelect.okPressed())

    app.ui.setupDialogForTest('modalDirSelect')
}

app.ui.dirSelect.path = ''
app.ui.dirSelect.pathFinal = false
app.ui.dirSelect.callback = null;

app.ui.dirSelect.show = (path, callback) => {

    app.ui.dirSelect.callback = callback

    app.ui.dirSelect.path = path
    app.ui.dirSelect.pathFinal = false
    app.ui.dirSelect.populate()

    $('#modalDirSelect').modal({show: true, backdrop: 'static'})

}

app.ui.dirSelect.populate = async () => {
    let dirList;

    $('#txtDirSelectPath').text(app.ui.dirSelect.path)

    try {
        const dir = await app.REST._ls(app.ui.dirSelect.path)

        if (dir.result === 'OK') {
            if (app.ui.dirSelect.path !== '/' && app.ui.dirSelect.path !== '') dirList = '<option>..</option>'
            if (dir.data === undefined) {
                app.ui.dirSelect.pathFinal = true

            } else {
                dir.data.forEach(el => {
                    dirList += '<option>' + el.path + '</option>'
                });

                app.ui.dirSelect.pathFinal = false
            }

            $('#selDirSelectList')
                .empty()
                .append(dirList)

        } else {
            app.ui.dirSelect.pathFinal = true
        }
    } catch (err) {
        app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')

    }
}
app.ui.dirSelect.listDblClick = () => {
    const el = $('#selDirSelectList').children("option:selected").text()

    if (el === '..') {
        app.ui.dirSelect.path = app.ui.dirSelect.path.substring(0, app.ui.dirSelect.path.lastIndexOf('/', app.ui.dirSelect.path.length - 2) + 1);
    } else {
        if (app.ui.dirSelect.pathFinal === true) {
            app.ui.dirSelect.path = app.ui.dirSelect.path.substring(0, app.ui.dirSelect.path.lastIndexOf('/', app.ui.dirSelect.path.length - 2) + 1);
            app.ui.dirSelect.path += el

        } else {
            app.ui.dirSelect.path += (app.ui.dirSelect.path.slice(-1) === '/' ? '' : '/') + el
        }
    }

    app.ui.dirSelect.populate()
}

app.ui.dirSelect.listClick = () => {

}

app.ui.dirSelect.okPressed = () => {
    $('#modalDirSelect').modal('hide')

    app.ui.dirSelect.callback(app.ui.dirSelect.path)
}
