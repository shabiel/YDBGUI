/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.octoTree.init = () => {
}

app.ui.octoTree.shown = []
app.ui.octoTree.loaded = []
app.ui.octoTree.width = []

app.ui.octoTree.dragStart = {}

app.ui.octoTree.refresh = async (type, res = null) => {
    if (res === null) {
        app.ui.wait.show("Fetching tables data...");

        try {
            res = await app.REST._fetchOctoTree()

            app.ui.wait.hide()

            const tables = {}
            const views = {}

            res.data.tables.forEach(table => {
                tables[table.name] = []
            })

            if (res.data.views) {
                res.data.views.forEach(view => {
                    views[view.name] = []
                })
            }

            app.ui.octoViewer.cm[type].setOption('hintOptions', {tables: tables})

        } catch (err) {
            app.ui.wait.hide()

            app.ui.msgbox.show(app.REST.parseError(err), 'WARNING')

            return
        }
    }

    // tables first
    const tables = res.data.tables
    let treeData = []

    tables.forEach(table => {
        treeData.push({
                id: 'table-' + table.name,
                data: {selection: ['table', table.name], type: type},
                text: table.name,
                icon: 'bi-table ydb-purple-fore',
                children: ['dummy']
            }
        )
    })

    // then views
    const views = res.data.views
    let viewsData = []

    if (Array.isArray(views)) {
        views.forEach(view => {
            viewsData.push({
                    id: 'view-' + view.name,
                    data: {selection: ['view', view.name], type: type},
                    text: view.name,
                    icon: 'bi-table ydb-purple-fore',
                    children: ['dummy']
                }
            )
        })
    }

    // then functions
    let functionsData = app.ui.octoTree.createFunctionsNode(res, type)

    const treeJson = [
        {
            id: 'root-tables',
            text: 'Tables',
            icon: 'bi-table ydb-purple-fore',
            children: treeData
        },
        {
            id: 'root-views',
            text: 'Views',
            icon: 'bi-table ydb-purple-fore',
            children: viewsData
        },
        {
            id: 'root-functions',
            text: 'Functions',
            icon: 'bi-gear ydb-purple-fore',
            children: functionsData,
            draggable: true
        },
    ]

    // init tree
    const plugins = ['search']

    if (app.serverMode === 'RW') plugins.push('dnd')

    $('#treeOctoTables' + type)
        .jstree("destroy")
        .jstree({
            'core': {
                'check_callback': app.ui.octoTree.callbackCheck,
                'dblclick_toggle': true,
                'multiple': false,
                'data': treeJson,
                'themes': {
                    'name': 'proton',
                },
            },
            plugins: plugins,
            dnd: {
                use_html5: true,
                // here we prevent some nodes from being dragged
                is_draggable: (node, e) => {
                    const id = node[0].id
                    if (id.indexOf('function') > -1 ||
                        id.indexOf('root') > -1 ||
                        id.indexOf('-type') > -1 ||
                        id.indexOf('-global') > -1 ||
                        id.indexOf('-indexes') > -1 ||
                        id.indexOf('-collation') > -1 ||
                        id.indexOf('-default') > -1 ||
                        id.indexOf('-nullable') > -1 ||
                        id.indexOf('-columns') > -1 ||
                        id.indexOf('-def') > -1
                    ) {
                        app.ui.octoTree.dragStart = null
                        e.preventDefault();
                        return false;
                    } else app.ui.octoTree.dragStart = node
                }
            }
        })
        .on('dblclick', '.jstree-anchor', function (e) {
            const instance = $.jstree.reference(this);
            const node = instance.get_node(this);

            // here we check if user double-clicked on a table name
            if (node.id.indexOf('table-') > -1 && node.id.split('-').length === 2 && node.children.length === 1) {
                node.children = []
                app.ui.octoTree.populateTable(node, type)
            }

            // here we check if user double-clicked on a view name
            if (node.id.indexOf('view-') > -1 && node.id.split('-').length === 2 && node.children.length === 1) {
                node.children = []
                app.ui.octoTree.populateView(node, type)
            }
        })
        .on('before_open.jstree', function (e, node) {
            if (node.node.id.indexOf('table-') > -1 && node.node.id.split('-').length === 2 && node.node.children.length === 1) {
                node.node.children = []
                app.ui.octoTree.populateTable(node.node, type)
            }

            if (node.node.id.indexOf('view-') > -1 && node.node.id.split('-').length === 2 && node.node.children.length === 1) {
                node.node.children = []
                app.ui.octoTree.populateView(node.node, type)
            }
        })
        .prev().remove()

    const $tree = $('#treeOctoTables' + type);
    const $input = $(
        '<input id="octoSearchBar" type="search" class="form-control" style="margin: 7px; width: 95%" placeholder="Search in tree..." />'
    );
    $input.insertBefore($tree);
    $input.on("keyup", function () {
        $tree.jstree(true).search($(this).val());
    });

    $('#modalTablesTree').modal({show: true, backdrop: 'static'});

}

// prevent dropping on self (tree)
app.ui.octoTree.callbackCheck = (op, node, parent, pos, more) => {
    if (op === 'move_node' || op === 'copy_node') return false
}

// functions
app.ui.octoTree.createFunctionsNode = (res, type) => {
    const functions = []

    res.data.functions.forEach((func, ix) => {
        let functionDropText = func.name + '('
        let functionDisplayText = func.name + '( '

        const lFunc = {
            id: 'func-' + func.name + ix,
            icon: 'bi-gear ydb-purple-fore',
            children: [
                {
                    id: 'function-' + func.name + ix + '-retType-' + func.returnType,
                    text: 'Return type: ' + func.returnType,
                    icon: 'bi-tags ydb-purple-fore',
                },
            ]
        }

        if (func.numArgs > 0) {
            const lArgs = {
                id: 'function-' + func.name + ix + '-args',
                text: 'Arguments (' + func.numArgs + ')',
                icon: 'bi-tags ydb-purple-fore',
                children: []
            }

            for (let argIx = 0; argIx < func.numArgs; argIx++) {
                functionDropText += func.args[argIx] + ', '
                functionDisplayText += func.args[argIx] + ', '

                lArgs.children.push({
                    id: 'function-' + func.name + ix + '-args-' + func.args[argIx],
                    text: func.args[argIx],
                    icon: 'bi-tags ydb-purple-fore',
                })
            }

            lFunc.children.push(lArgs)

            // here we remove the last two chars (comma, space)
            functionDropText = functionDropText.slice(0, -2)
            functionDisplayText = functionDisplayText.slice(0, -2)
        }

        // here we terminate the strings with a close paren
        functionDropText += ')'
        functionDisplayText += ' )'
        lFunc.data = {selection: ['function', functionDropText], type: type}
        lFunc.text = functionDisplayText,

            lFunc.children.push({
                id: 'function-' + func.name + ix + '-source-' + func.source,
                text: 'Source: ' + func.source,
                icon: 'bi-braces ydb-purple-fore',
            })

        functions.push(lFunc)
    })

    return functions
}

app.ui.octoTree.getViewFields = (view, type) => {
    const columnsTree = []

    view.columns.forEach(column => {
        columnsTree.push({
            id: 'view-' + view.name + '-column-' + column.name,
            text: column.name + ' ( ' + column.type + ' )',
            data: {selection: ['column', column.name, 'view', view.name], type: type},
            icon: 'ydb-purple-fore bi-caret-right',
        })
    })

    return columnsTree
}

app.ui.octoTree.getFields = (table, type) => {
    const columnsTree = []

    table.columns.forEach(column => {
        const isPrimaryKey = table.indexes ? table.indexes.find(index => index.columns.indexOf(column.name) > -1 && index.type.indexOf('PRIMARY KEY') > -1) : undefined

        columnsTree.push({
            id: 'table-' + table.name + '-column-' + column.name,
            text: column.name,
            data: {selection: ['column', column.name, 'table', table.name], type: type},
            icon: 'ydb-purple-fore bi-' + (isPrimaryKey === undefined ? 'caret-right' : 'key-fill'),
            children: [
                {
                    id: 'table-' + table.name + '-column-' + column.name + '-type',
                    text: 'Type: ' + column.type,
                    icon: 'bi-tags ydb-purple-fore',
                },
                {
                    id: 'table-' + table.name + '-column-' + column.name + '-collation',
                    text: 'Collation: ' + column.collation,
                    icon: 'bi-tags ydb-purple-fore',
                },
                {
                    id: 'table-' + table.name + '-column-' + column.name + '-default',
                    text: 'Default: ' + column.default,
                    icon: 'bi-tags ydb-purple-fore',
                },
                {
                    id: 'table-' + table.name + '-column-' + column.name + '-nullable',
                    text: 'Nullable: ' + column.nullable,
                    icon: 'bi-tags ydb-purple-fore',
                },
            ]
        })
    })

    return columnsTree
}

app.ui.octoTree.getIndexes = table => {
    const indexesTree = []

    if (table.indexes !== undefined) {
        table.indexes.forEach(index => {
            indexesTree.push({
                id: 'table-' + table.name + '-indexes-' + index.name,
                text: index.name,
                icon: 'bi-' + (index.type.indexOf('PRIMARY KEY') > -1 ? 'key-fill' : 'key') + ' ydb-purple-fore',
                children: [
                    {
                        id: 'table-' + table.name + '-columns-' + index.name + 'columns',
                        text: 'Columns: ' + index.columns,
                        icon: 'bi-tags ydb-purple-fore',
                    },
                    {
                        id: 'table-' + table.name + '-columns-' + index.name + 'type',
                        text: 'Type: ' + index.type,
                        icon: 'bi-tags ydb-purple-fore',
                    },
                    {
                        id: 'table-' + table.name + '-columns-' + index.name + 'global',
                        text: 'Global: ' + index.global,
                        icon: 'bi-tags ydb-purple-fore',
                    },
                ]
            })
        })
    }

    return indexesTree
}

app.ui.octoTree.populateTable = async (node, type) => {
    const tableName = node.id.split('-')[1]

    try {
        const res = await app.REST._getTableInfo(tableName)
        const table = res.data
        const tree = $('#treeOctoTables' + type)

        const children = [
            {
                id: 'table-' + table.name + '-type-' + table.type,
                text: 'Type: ' + table.type,
                icon: 'bi-' + (table.type === 'READWRITE' ? 'unlock' : 'lock') + ' ydb-purple-fore',
            },
            {
                id: 'table-' + table.name + '-global-' + table.global,
                text: 'Global: ' + table.global,
                icon: 'bi-sd-card ydb-purple-fore',
            },
            {
                id: 'table-' + table.name + '-columns',
                text: 'Columns',
                icon: 'bi-list-ul ydb-purple-fore',
                children: app.ui.octoTree.getFields(table, type)
            },
            {
                id: 'table-' + table.name + '-indexes',
                text: 'Indexes',
                icon: 'bi-sort-down-alt ydb-purple-fore',
                children: app.ui.octoTree.getIndexes(table)
            }
        ]

        tree.jstree(true).create_node(node.id, children[0], 'last')
        tree.jstree(true).create_node(node.id, children[1], 'last')
        tree.jstree(true).create_node(node.id, children[2], 'last')
        tree.jstree(true).create_node(node.id, children[3], 'last')

        tree.jstree(true).open_node(node)

    } catch (err) {
        console.log(app.REST.parseError(err))
    }
}

app.ui.octoTree.populateView = async (node, type) => {
    const viewName = node.id.split('-')[1]

    try {
        const res = await app.REST._getViewInfo(viewName)
        const view = res.data
        const tree = $('#treeOctoTables' + type)

        const children = [
            {
                id: 'view-' + view.name + '-def-' + view.def,
                text: '<span onclick="app.ui.octoTree.displayDefinition(\'' + view.def.replaceAll('\'', '@') + '\')">' +
                    'Definition: ' + (view.def.length < app.userSettings.defaults.octo.treeViewsDefMaxLength ? view.def : view.def.substring(0, app.userSettings.defaults.octo.treeViewsDefMaxLength) + '...') +
                    '</span>',
                icon: 'bi-sd-card ydb-purple-fore',
            },
            {
                id: 'view-' + view.name + '-columns',
                text: 'Columns',
                icon: 'bi-list-ul ydb-purple-fore',
                children: app.ui.octoTree.getViewFields(view, type)
            },
        ]

        tree.jstree(true).create_node(node.id, children[0], 'last')
        tree.jstree(true).create_node(node.id, children[1], 'last')

        tree.jstree(true).open_node(node)

        app.ui.regeneratePopups()

    } catch (err) {
        console.log(app.REST.parseError(err))
    }
}

app.ui.octoTree.displayDefinition = function (def) {
    if ($('#divOctoPopupDefinition').css('display') === 'block') return

    const offset = ($(':hover').last().offset())

    $('#divOctoPopupDefinition')
        .css('display', 'block')
        .css('left', offset.left)
        .css('top', offset.top)
        .html(def.replaceAll('@', '\''))

    app.ui.octoViewer.mountModalHandlers()

}
