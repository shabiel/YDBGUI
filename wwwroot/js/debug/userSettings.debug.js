/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.userSettings.rest.logOnConsole = 'yes'

app.userSettings.defaults.replication = {
    discoveryService: {
        responseType: 'F',
        servers: [
            {
                instance: 'melbourne',
                host: '10.5.0.2',
                port: 8089,
                statsPort: 8090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'santiago',
                host: '10.5.0.4',
                port: 10089,
                statsPort: 10090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'paris',
                host: '10.5.0.3',
                port: 9089,
                statsPort: 9090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'rome',
                host: '10.5.0.5',
                port: 11089,
                statsPort: 11090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'amsterdam',
                host: '10.5.0.6',
                port: 12089,
                statsPort: 12090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'london',
                host: '10.5.0.7',
                port: 13089,
                statsPort: 13090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'tokio',
                host: '10.5.0.8',
                port: 14089,
                statsPort: 14090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: 'madrid',
                host: '10.5.0.9',
                port: 15089,
                statsPort: 15090,
                protocol: 'https',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
            {
                instance: '',
                host: '',
                port: 80,
                statsPort: 81,
                protocol: 'http',
                tlsFileLocation: '',
                verifyCertificates: false,
            },
        ],
        timeouts: {
            onConnect: 500,
            onResponse: 10000,
        }
    },
    topology: {
        refresh: "0",
        backlog: {
            processRange: true
        },
        links: {
            connector: 'straight',
            router: 'normal'
        },
        layout: 'TB',
        undoBufferSize: 200,

    },
    dashboard: {
        refreshBacklog: 'off',
        processRange: true
    }
}

app.userSettings.defaults.mainScreenRefresh = 'off'
