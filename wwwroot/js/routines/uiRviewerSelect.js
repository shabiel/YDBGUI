/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.rViewer.select.init = () => {
    $('#inpRviewerMask')
        .on('keypress', e => app.ui.rViewer.select.keyPress(e))
        .on('keyup', () => app.ui.rViewer.select.keyUp());
    $('#btnRviewerMaskSubmit').on('click', () => app.ui.rViewer.select.submit());
    $('#btnRviewerSelectShow').on('click', () => app.ui.rViewer.select.showClicked());

    $('#modalRviewerSelect').on('shown.bs.modal', () => {
        $('#inpRviewerMask').focus()
    });

    // TEST
    app.ui.setupDialogForTest('modalRviewerSelect')
};

app.ui.rViewer.select.metadata = {
    tabId: null,
    routineName: ''
};
app.ui.rViewer.select.show = (tabId = null) => {
    app.ui.rViewer.select.metadata.tabId = tabId;

    $('#tblRviewerSelect >tbody').empty();
    $('#inpRviewerMask').val('')
    $('#txtRviewerTotalFound').text('0')

    $('#modalRviewerSelect').modal({show: true, backdrop: 'static'});
};

app.ui.rViewer.select.keyPress = async e => {
    const charCode = e.which || e.originalEvent.which;

    if (charCode === 13) {
        if ($('#inpRviewerMask').val().length > 0 && $('#inpRviewerMask').val().replace(/[?*]/g, '').length > 1) {
            app.ui.rViewer.select.submit();

            return

        } else {
            await app.ui.msgbox.show('The search string must have at least two characters, excluding wildcards', app.ui.msgbox.INPUT_ERROR, true)

            $('#inpRviewerMask').focus();

            return
        }
    }
    return (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123 || (charCode > 47 && charCode < 58) || charCode === 42 || charCode === 63 || charCode === 37);
}
;

app.ui.rViewer.select.keyUp = () => {
    if ($('#inpRviewerMask').val().length > 0 && $('#inpRviewerMask').val().replace(/[?*]/g, '').length > 1) {
        $('#btnRviewerMaskSubmit')
            .removeClass('disabled')
            .attr('disabled', false)
    } else {
        $('#btnRviewerMaskSubmit')
            .addClass('disabled')
            .attr('disabled', true)
    }
};

app.ui.rViewer.select.submit = () => {
    app.ui.rViewer.select.findRoutines($('#inpRviewerMask').val())
};

app.ui.rViewer.select.findRoutines = async mask => {
    try {
        await app.ui.wait.show('Fetching routines information...');

        const res = await app.REST._findRoutines(mask);
        const tblRviewerSelect = $('#tblRviewerSelect >tbody');
        let rows = '';

        tblRviewerSelect
            .empty();

        if (res.data === 0) {
            rows += '<tr><td colspan="3" style="font-size: 24px; text-align: center;">No routines found...</td></tr>'

            $('#txtRviewerTotalFoundHeader').css('display', 'none');
            $('#txtRviewerTotalFound').css('display', 'none');

        } else {
            let routinesCounter = 0;
            res.data.forEach(routine => {
                routinesCounter++;

                rows += '<tr id="rou-' + routine.routine + '"' +
                    (routine.source === true ? 'ondblclick="app.ui.rViewer.select.dblClick(this)" ' : 'style="font-style: italic; color: #c3c3c3; cursor: not-allowed"') +
                    ' class="inconsolata">';
                rows += '<td>' + routine.routine + '</td>';
                rows += '<td>' + routine.location + '</td>';

                rows += '</a></tr>';
            })

            $('#txtRviewerTotalFound')
                .text(routinesCounter)
                .css('display', 'block');
            $('#txtRviewerTotalFoundHeader').css('display', 'block')
        }

        tblRviewerSelect.append(rows);

        $('#tblRviewerSelect').select({
            children: "tr",
            className: "rviewer-selected-row",
            onSelect: sel => app.ui.rViewer.select.rowClicked(sel)
        });

        // re-generate the popups
        app.ui.regeneratePopups()

        app.ui.wait.hide();

    } catch (err) {
        app.ui.wait.hide();

        app.ui.msgbox.show(app.REST.parseError(err), 'ERROR');
    }
};

app.ui.rViewer.select.dblClick = that => {
    const routineName = that.id.toString().split('-')[1];
    app.ui.rViewer.addNew({routineName: routineName, tabId: null});

    $('#modalRviewerSelect').modal('hide');
};

app.ui.rViewer.select.selection = [];

app.ui.rViewer.select.rowClicked = sel => {
    const btnRviewerSelectShow = $('#btnRviewerSelectShow');

    btnRviewerSelectShow
        .removeClass('disabled')
        .prop('disabled', false);

    app.ui.rViewer.select.selection = sel
};

app.ui.rViewer.select.showClicked = async () => {
    for (const row of app.ui.rViewer.select.selection) {
        app.ui.rViewer.select.metadata.routineName = row.id.split('-')[1].replace(/%/, '_')
        app.ui.rViewer.select.metadata.tabId = null;
        await app.ui.rViewer.addNew(app.ui.rViewer.select.metadata);
    }

    $('#modalRviewerSelect').modal('hide');
};

(function ($) {
    // select jQuery plugin
    // http://stackoverflow.com/a/35813513/383904
    $.fn.select = function () {

        var settings = $.extend({
            children: "tbody tr",
            className: "selected",
            onSelect: function () {}
        }, arguments[0] || {});

        return this.each(function (_, that) {
            var $ch = $(this).find(settings.children),
                sel = [],
                last;

            $ch = $ch.filter((ix, el) => {
                return el.ondblclick !== null
            })

            $ch.on("mousedown", function (ev) {
                var isCtrl = (ev.ctrlKey || ev.metaKey),
                    isShift = ev.shiftKey,
                    ti = $ch.index(this),
                    li = $ch.index(last),
                    ai = $.inArray(this, sel);

                if (isShift || isCtrl) ev.preventDefault();

                $(sel).removeClass(settings.className);

                if (isCtrl) {
                    if (ai > -1) sel.splice(ai, 1);
                    else sel.push(this);
                } else if (isShift && sel.length > 0) {
                    if (ti > li) ti = [li, li = ti][0];
                    sel = $ch.slice(ti, li + 1);
                } else {
                    sel = ai < 0 || sel.length > 1 ? [this] : [];
                }

                last = this;
                $(sel).addClass(settings.className);
                settings.onSelect.call(that, sel);
            });
        });
    };
}(jQuery));
