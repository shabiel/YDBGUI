/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.login.loggedIn = false
app.config.timeout = 0

app.ui.login.init = () => {
    $('#btnLoginLogin').on('click', () => app.ui.login.okClicked())

    $('#inpLoginPassword').on('keypress', e => app.ui.login.passwordKeyPress(e))

    app.ui.setupDialogForTest('modalLogin')
}

app.ui.login.show = () => {
    app.ui.login.cleanup()

    $('#modalLogin')
        .modal({show: true, backdrop: 'static', keyboard: false})
        .on('shown.bs.modal', () => {
            $('#inpLoginUsername').focus()
        })
}

app.ui.login.passwordKeyPress = e => {
    if (e.which === 13) app.ui.login.okClicked()
}

app.ui.login.okClicked = async () => {
    if ($('#inpLoginUsername').val() === '') {
        await app.ui.msgbox.show('The username field can not be empty', app.ui.msgbox.INPUT_ERROR, true)
        $('#inpLoginUsername').focus()

        return
    }

    if ($('#inpLoginPassword').val() === '') {
        await app.ui.msgbox.show('The password field can not be empty', app.ui.msgbox.INPUT_ERROR, true)
        $('#inpLoginPassword').focus()

        return
    }

    try {
        const res = await app.REST._login({username: $('#inpLoginUsername').val(), password: $('#inpLoginPassword').val()})

        $('#modalLogin').modal('hide')

        app.config.timeout = res.timeout
        app.ui.login.loggedIn = true
        app.ui.login.timeout.setLoginTime()
        app.ui.login.timeout.startup()

        app.ui.dashboard.refresh()

        // set dashboard timeout
        dashboardTimeout.set()

        // process Permalink
        if (permaMode.set === true) app.ui.permalinks.process(permaMode.link)

    } catch (err) {
        if (err.status === 401) {
            await app.ui.msgbox.show('The combination username / password you entered is not valid.', 'ERROR', true)
            $('#inpLoginUsername').focus()

        } else {
            await app.ui.msgbox.show(app.REST.parseError(err), 'WARNING', true)
        }

        app.ui.login.cleanup()

        $('#inpLoginUsername').focus()

        return
    }
}

app.ui.login.cleanup = () => {
    $('#inpLoginUsername').val('')
    $('#inpLoginPassword').val('')
}

app.ui.login.logout = async () => {
    app.ui.inputbox.show('This will log you out from the server.<br><br>Are you sure ?', 'WARNING', async ret => {
        if (ret === 'YES') {
            await app.REST._logout()

            app.ui.login.loggedIn = false

            window.location.replace(window.location.origin)
        }
    })
}

// This is the ACTIVITY timeout (mouse and keyboard) and it is set to the SAME TIME as the Server timeout.
// Additionally, if (mouse or keyboard) activity was detected and this timeout is skipped, an extra check is performed at REST level
// to detect an eventual token_timeout exception (408)
app.ui.login.timeout = {
    lastValue: 0,

    init: function () {
        // initialize screen timeout
        window.addEventListener("keydown", () => {
            this.setLoginTime()
        });
        window.addEventListener("mousemove", () => {
            this.setLoginTime()
        });
    },

    setLoginTime: function () {
        this.lastValue = Math.floor(Date.now() / 1000)
    },

    startup: function () {
        const timerH = setInterval(async function () {
            if (app.ui.login.loggedIn === false || app.config.timeout === 0) return;

            if (Math.floor(Date.now() / 1000) - app.ui.login.timeout.lastValue >= app.config.timeout) {
                clearInterval(timerH)

                await app.ui.msgbox.show('You have been automatically logged out due to no activity.', 'INFO', true);

                window.location.reload()

            }
        }, 1000)
    }
}

