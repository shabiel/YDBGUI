/****************************************************************
 *                                                              *
 * Copyright (c) 2022 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.globalsManager.init = () => {
    $("#ulLastUsedGlobalsManager")
        .sortable({
            placeholder: "ui-state-highlight"
        })
        .disableSelection();

    $('#btnLastUsedGlobalsManagerOk').on('click', () => app.ui.globalsManager.okPressed())
};

app.ui.globalsManager.show = () => {
    const items = [...app.userSettings.globalViewer.lastUsed].reverse();

    let rows = '';
    items.forEach(item => {
        rows += '<li class="ui-state-default inconsolata"><span class="ui-icon ui-icon-arrowthick-2-n-s" ></span>' + item + '</li>'
    });

    $("#ulLastUsedGlobalsManager")
        .empty()
        .append(rows);

    $('#modalLastUsedGlobalsManager')
        .modal({show: true, backdrop: 'static'})
};

app.ui.globalsManager.okPressed = () => {

    $('#modalLastUsedGlobalsManager').modal('hide');


    app.userSettings.globalViewer.lastUsed = [];

    $("#ulLastUsedGlobalsManager").children().each(function () {
        app.userSettings.globalViewer.lastUsed.push($(this).text())
    });

    app.userSettings.globalViewer.lastUsed.reverse();

    app.ui.storage.save('gViewer', app.userSettings.globalViewer);

    //app.userSettings.globalViewer.lastUsed.reverse();
    app.ui.menu.rebuildGlobalViewerLastUsed();
};
