/****************************************************************
 *                                                              *
 * Copyright (c) 2022 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
app.ui.gViewer.moreItems = {};

app.ui.gViewer.moreItems.init = () => {
    $('#btnGviewerOk').on('click', () => { app.ui.gViewer.moreItems.okPressed()})
};

app.ui.gViewer.moreItems.show = () => {
    const divGviewerMore = $('#divGviewerMore');
    const items = [...app.userSettings.globalViewer.lastUsed].reverse();

    divGviewerMore.empty();

    let isFirst = true;
    items.forEach((item, ix) => {
        let row = '<a class="inconsolata bold nav-link ' + (isFirst ? 'active' : '') + '" id="navGviewerMoreItems-' + ix + '" data-toggle="pill" role="tab" ondblclick="app.ui.gViewer.moreItems.okPressed()"><div class="row"><div class="col-12">';
        row += item + '</div></div></a>';

        divGviewerMore.append(row);

        if (isFirst === true) isFirst = false;
    });

    $('#modalGviewerMoreItems').modal({show: true, backdrop: 'static'})
};

app.ui.gViewer.moreItems.okPressed = () => {
    const items = [...app.userSettings.globalViewer.lastUsed].reverse();
    const ix = $('#divGviewerMore').find('.active').attr('id').split('-')[1];

    $('#modalGviewerMoreItems').modal('hide');
    app.ui.gViewer.addLastUsed(items[ix])

};
