/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.gViewer.settings = {};

app.ui.gViewer.settings.currentTabId = '';

app.ui.gViewer.settings.init = () => {
    $('#btnGviewerSettingsOk').on('click', () => app.ui.gViewer.settings.okPressed());

    $('#inpGviewerSettingsTotalBufferSize').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
    $('#inpGviewerSettingsInitialFetchSize').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
    $('#inpGviewerSettingsMaxDataLength').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
    $('#inpGviewerSettingsLazyFetchSize').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
    $('#inpGviewerSettingsLazyFetchTriggerPages').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
    $('#inpGviewerSettingsLastUsedCount').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
    $('#inpGviewerSettingsLastUsedMaxStore').on('keypress', function (e) {app.ui.gViewer.settings.onkeypress(e, this)});
};

app.ui.gViewer.settings.show = currentTabId => {
    const instance = app.ui.gViewer.instance[currentTabId];

    app.ui.gViewer.settings.currentTabId = currentTabId;

    // populate with  values
    app.ui.gViewer.settings.populate(instance);

    // and redraw the previews
    app.ui.gViewer.settings.refreshPathBar();
    app.ui.gViewer.settings.refreshPreviewLight();
    app.ui.gViewer.settings.refreshPreviewDark();

    // reset the tabs
    $('#tabGviewerGlobal').tab('show');

    // reset the ok button
    $('#btnGviewerSettingsOk').attr('disabled', true);

    // display the dialog
    $('#modalGviewerSettings').modal({show: true, backdrop: 'static'});
};


app.ui.gViewer.settings.populateFields = (id, data) => {
    const field = app.ui.gViewer.settings.getField(id);
    const $field = $('#' + id);

    field.newValue = field.oldValue = data;
    $field.val(typeof field.oldValue === 'string' && field.oldValue.indexOf('px') > -1 ? field.oldValue.slice(0, -2) : field.oldValue);

    // values
    if (field.type === 'number') {
        $field
            .attr('min', field.min)
            .attr('max', field.max)
    }

    // popup
    $field.parent().parent().find('a')
        .attr('data-original-title', field.caption)
        .attr('data-content', field.description)
};

app.ui.gViewer.settings.onChange = async obj => {
    const field = app.ui.gViewer.settings.getField(obj.id);

    if (parseInt(obj.value) > parseInt(obj.max) || parseInt(obj.value) < parseInt(obj.min)) {
        await app.ui.msgbox.show('The value you entered is outside the limits allowed.<br><br>The limits for these fields are:<br>Min: ' + obj.min + '<br>Max: ' + obj.max, app.ui.msgbox.INPUT_ERROR, true)

        obj.value = field.newValue;

        return
    }

    field.newValue = obj.value;

    switch (field.refreshTarget) {
        case null: {
            break
        }
        case 'pathbar': {
            app.ui.gViewer.settings.refreshPathBar();

            break;
        }
        case 'previewLight': {
            app.ui.gViewer.settings.refreshPreviewLight();

            break;
        }
        case 'previewDark': {
            app.ui.gViewer.settings.refreshPreviewDark();

        }
    }

    app.ui.gViewer.settings.validateOk()
};

app.ui.gViewer.settings.validateOk = () => {
    let dirty = false;

    app.ui.gViewer.settings.params.forEach(item => {
        if (typeof item.oldValue === 'string' && item.oldValue !== item.newValue ||
            typeof item.oldValue === 'number' && item.oldValue !== parseInt(item.newValue)) {
            dirty = true;
        }
    });

    if (dirty === true) {
        $('#btnGviewerSettingsOk')
            .attr('disabled', false)
            .removeClass('disabled');

    } else {
        $('#btnGviewerSettingsOk')
            .attr('disabled', true)
            .addClass('disabled')

    }
};

app.ui.gViewer.settings.onkeypress = (e, that) => {
    const keyCode = e.keyCode;

    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46 && keyCode !== 8) {
        e.preventDefault();
    }
};

app.ui.gViewer.settings.okPressed = () => {
    // determine what kind of fields are changed
    const isChanged = {
        buffer: false,
        menus: false,
        tabs: false,
        defaults: false,
        pathBar: false,
        themeLight: false,
        themeDark: false

    };

    app.ui.gViewer.settings.params.forEach(item => {
        if (item.oldValue !== item.newValue) {
            switch (item.refreshTarget) {
                case 'buffer': {
                    isChanged.buffer = true;

                    break;
                }
                case 'menus': {
                    isChanged.menus = true;

                    break;
                }
                case 'tabs': {
                    isChanged.tabs = true;

                    break;
                }
                case 'defaults': {
                    isChanged.defaults = true;

                    break;
                }
                case 'pathbar': {
                    isChanged.pathBar = true;

                    break;
                }
                case 'previewLight': {
                    isChanged.themeLight = true;

                    break

                }
                case 'previewDark': {
                    isChanged.themeDark = true;
                }
            }
        }
    });

    // validate buffer if changed
    if (isChanged.buffer) {
        const bufferSize = parseInt(app.ui.gViewer.settings.getField('inpGviewerSettingsTotalBufferSize').newValue);
        const initialFetch = parseInt(app.ui.gViewer.settings.getField('inpGviewerSettingsInitialFetchSize').newValue);
        const lazyFetch = parseInt(app.ui.gViewer.settings.getField('inpGviewerSettingsLazyFetchSize').newValue);

        if ((initialFetch + lazyFetch) > bufferSize) {
            app.ui.msgbox.show('The sum of the Initial Fetch and the Lazy Fetch can not be greater than the Total Buffer Size', app.ui.msgbox.INPUT_ERROR);

            return
        }
    }

    app.ui.gViewer.settings.confirm.show(isChanged);

    // used to ensure that the dialog stands out
    $('.modal-backdrop').css('z-index', 1060);
};

app.ui.gViewer.settings.refreshPreviewLight = () => {
    const settings = app.userSettings.globalViewer.themes['light'].table.content.values;

    // header
    let head = '<th class="gviewer-table-th" scope="col" style="width: 60px; color: blue; background: ' + $('#inpGviewerSettingsBookmarkHeaderBack').val() + '; vertical-align: middle;"></th>' +
        '<th class="gviewer-table-th" scope="col" style="width: 40%; font-size: ' + $('#inpGviewerSettingsFontSize').val() + '; color: ' + $('#inpGviewerSettingsHeadersFore').val() + '; background: ' + $('#inpGviewerSettingsHeadersBack').val() + '; vertical-align: middle;">Nodes</th>' +
        '<th class="gviewer-table-th" scope="col" style="font-size: ' + $('#inpGviewerSettingsFontSize').val() + ';color: ' + $('#inpGviewerSettingsHeadersFore').val() + '; background: ' + $('#inpGviewerSettingsHeadersBack').val() + '; vertical-align: middle;"><span style="padding-top: 5px; display: inline-grid;">Data</span>'

    $('#tblGviewerSettingsPreview > thead')
        .empty()
        .append(head);

    // col1
    $('#td11GviewerSettingsPreview')
        .css('color', $('#inpGviewerSettingsBookmarksFore').val())
        .css('background', $('#inpGviewerSettingsBookmarksBack').val());
    $('#td12GviewerSettingsPreview')
        .css('color', $('#inpGviewerSettingsBookmarksFore').val())
        .css('background', $('#inpGviewerSettingsBookmarksBack').val());
    $('#td13GviewerSettingsPreview')
        .css('color', $('#inpGviewerSettingsBookmarksFore').val())
        .css('background', $('#inpGviewerSettingsBookmarksBack').val());

    // col2
    let path = '<span style="color: ' + $('#inpGviewerSettingsNodesGlobal').val() + '">^GLOBAL</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesParens').val() + '">(</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesNumbers').val() + '">999</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesCommas').val() + '">,</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesStrings').val() + '">"B"</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesCommas').val() + '">,</span>' +
        '<span  style="color: ' + $('#inpGviewerSettingsNodesNumbers').val() + '">123456</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesParens').val() + '">)</span>';

    $('#td21GviewerSettingsPreview').html(path);
    $('#td22GviewerSettingsPreview').html(path);
    $('#td23GviewerSettingsPreview').html(path)

    // col 3
    let data = '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + '">"123456^23^ASPIRINE"</span>';
    $('#td31GviewerSettingsPreview').html(data);

    data = '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + '">"123456</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataPieceFore').val() + '; background: ' + $('#inpGviewerSettingsDataPieceBack').val() + ';">^</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + '">23</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataPieceFore').val() + '; background: ' + $('#inpGviewerSettingsDataPieceBack').val() + ';">^</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + '">ASPIRINE"</span>'
    $('#td32GviewerSettingsPreview').html(data);

    data = '<span style="color: ' + $('#inpGviewerSettingsDataPillFore').val() + '; background: ' + $('#inpGviewerSettingsDataPillBack').val() + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.paddingLr + ';">P1' + '</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + ';">&nbsp;123456&nbsp;</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataPillFore').val() + '; background: ' + $('#inpGviewerSettingsDataPillBack').val() + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.paddingLr + ';">P2' + '</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + ';">&nbsp;23&nbsp;</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataPillFore').val() + '; background: ' + $('#inpGviewerSettingsDataPillBack').val() + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.paddingLr + ';">P3' + '</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataFore').val() + ';">&nbsp;ASPIRINE</span>';
    $('#td33GviewerSettingsPreview').html(data);
};

app.ui.gViewer.settings.refreshPreviewDark = () => {
    const settings = app.userSettings.globalViewer.themes['dark'].table.content.values;

    // header
    let head = '<th class="gviewer-table-th" scope="col" style="width: 60px; color: blue; background: ' + $('#inpGviewerSettingsBookmarkHeaderBack').val() + '; vertical-align: middle;"></th>' +
        '<th class="gviewer-table-th" scope="col" style="width: 40%; font-size: ' + $('#inpGviewerSettingsFontSizeDark').val() + '; color: ' + $('#inpGviewerSettingsHeadersForeDark').val() + '; background: ' + $('#inpGviewerSettingsHeadersBackDark').val() + '; vertical-align: middle;">Nodes</th>' +
        '<th class="gviewer-table-th" scope="col" style="font-size: ' + $('#inpGviewerSettingsFontSizeDark').val() + ';color: ' + $('#inpGviewerSettingsHeadersForeDark').val() + '; background: ' + $('#inpGviewerSettingsHeadersBackDark').val() + '; vertical-align: middle;"><span style="padding-top: 5px; display: inline-grid;">Data</span>'

    $('#tblGviewerSettingsPreviewDark > thead')
        .empty()
        .append(head);

    // col1
    $('#td11GviewerSettingsPreviewDark')
        .css('color', $('#inpGviewerSettingsBookmarksForeDark').val())
        .css('background', $('#inpGviewerSettingsBookmarksBackDark').val());
    $('#td12GviewerSettingsPreviewDark')
        .css('color', $('#inpGviewerSettingsBookmarksForeDark').val())
        .css('background', $('#inpGviewerSettingsBookmarksBackDark').val());
    $('#td13GviewerSettingsPreviewDark')
        .css('color', $('#inpGviewerSettingsBookmarksForeDark').val())
        .css('background', $('#inpGviewerSettingsBookmarksBackDark').val());

    // col2
    let path = '<span style="color: ' + $('#inpGviewerSettingsNodesGlobalDark').val() + '">^GLOBAL</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesParensDark').val() + '">(</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesNumbersDark').val() + '">999</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesCommasDark').val() + '">,</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesStringsDark').val() + '">"B"</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesCommasDark').val() + '">,</span>' +
        '<span  style="color: ' + $('#inpGviewerSettingsNodesNumbersDark').val() + '">123456</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsNodesParensDark').val() + '">)</span>';

    $('#td21GviewerSettingsPreviewDark').html(path);
    $('#td22GviewerSettingsPreviewDark').html(path);
    $('#td23GviewerSettingsPreviewDark').html(path)

    // col 3
    let data = '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + '">"123456^23^ASPIRINE"</span>';
    $('#td31GviewerSettingsPreviewDark').html(data);

    data = '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + '">"123456</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataPieceForeDark').val() + '; background: ' + $('#inpGviewerSettingsDataPieceBackDark').val() + ';">^</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + '">23</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataPieceForeDark').val() + '; background: ' + $('#inpGviewerSettingsDataPieceBackDark').val() + ';">^</span>' +
        '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + '">ASPIRINE"</span>'
    $('#td32GviewerSettingsPreviewDark').html(data);

    data = '<span style="color: ' + $('#inpGviewerSettingsDataPillForeDark').val() + '; background: ' + $('#inpGviewerSettingsDataPillBackDark').val() + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.paddingLr + ';">P1' + '</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + ';">&nbsp;123456&nbsp;</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataPillForeDark').val() + '; background: ' + $('#inpGviewerSettingsDataPillBackDark').val() + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.paddingLr + ';">P2' + '</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + ';">&nbsp;23&nbsp;</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataPillForeDark').val() + '; background: ' + $('#inpGviewerSettingsDataPillBackDark').val() + '; border-radius: ' + settings.piecePills.borderRadius + '; text-align: center; padding: 0 ' + settings.piecePills.paddingLr + ';">P3' + '</span>';
    data += '<span style="color: ' + $('#inpGviewerSettingsDataForeDark').val() + ';">&nbsp;ASPIRINE</span>';
    $('#td33GviewerSettingsPreviewDark').html(data);
};

app.ui.gViewer.settings.refreshPathBar = () => {
    const $pathBar = $('#spanGviewerSettingsPathBar');

    const paren = $('#inpGviewerSettingsPathBarParens').val();
    const comma = $('#inpGviewerSettingsPathBarCommas').val();
    const colon = $('#inpGviewerSettingsPathBarColons').val();
    const string = $('#inpGviewerSettingsPathBarStrings').val();
    const number = $('#inpGviewerSettingsPathBarNumbers').val();

    // ^GLOBAL(100.01,"B","AA":"CC",,1000:5000,*)
    let html = '<span style="color:' + $('#inpGviewerSettingsPathBarGlobal').val() + '">^GLOBAL</span>';
    html += '<span style="color:' + paren + '">(</span>';
    html += '<span style="color:' + number + '">100.01</span>';
    html += '<span style="color:' + comma + '">,</span>';
    html += '<span style="color:' + string + '">"B"</span>';
    html += '<span style="color:' + comma + '">,</span>';
    html += '<span style="color:' + string + '">"AA"</span>';
    html += '<span style="color:' + colon + '">:</span>';
    html += '<span style="color:' + string + '">"CC"</span>';
    html += '<span style="color:' + comma + '">,</span>';
    html += '<span style="color:' + comma + '">,</span>';
    html += '<span style="color:' + number + '">1000</span>';
    html += '<span style="color:' + colon + '">:</span>';
    html += '<span style="color:' + number + '">5000</span>';
    html += '<span style="color:' + comma + '">,</span>';
    html += '<span style="color:' + $('#inpGviewerSettingsPathBarStar').val() + '">*</span>';
    html += '<span style="color:' + paren + '">)</span>';

    $pathBar.html(html);
};

app.ui.gViewer.settings.populate = instance => {
    const settings = app.userSettings.globalViewer;
    let settings2;
    let theme2;

    // adjust according to current selection
    if (instance.viewer.appearance.theme === 'light') {
        settings2 = instance.viewer.themes['light'];
        theme2 = app.userSettings.globalViewer.themes['dark'];

    } else {
        settings2 = app.userSettings.globalViewer.themes['light'];
        theme2 = instance.viewer.themes['dark'];
    }

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsTotalBufferSize', settings.options.table.buffering.maxSize);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsInitialFetchSize', settings.options.table.buffering.initialFetchSize);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsMaxDataLength', settings.options.table.maxDataLength);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsLazyFetchSize', settings.options.table.buffering.lazyFetchSize);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsLazyFetchTriggerPages', settings.options.table.buffering.startFetchingPages);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsLastUsedCount', settings.options.menuLastUsedItemsCount);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsLastUsedMaxStore', settings.options.lastUsedMaxItems);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDefaultTheme', settings.defaultTheme);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDefaultPieceChar', settings2.table.content.values.pieceChar.char);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDefaultPieceView', settings2.table.content.values.showPieces);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDefaultBookmarkColor', settings2.table.bookmarkColor);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarGlobal', settings2.titleBar.globalName);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarParens', settings2.titleBar.parens);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarNumbers', settings2.titleBar.numbers);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarStrings', settings2.titleBar.strings);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarCommas', settings2.titleBar.comma);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarColons', settings2.titleBar.colon);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsPathBarStar', settings2.titleBar.star);

    // light
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsBookmarkHeaderBack', settings2.table.headers.bookmark.back);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsHeadersFore', settings2.table.headers.fore);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsHeadersBack', settings2.table.headers.back);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsBookmarksBack', settings2.table.content.bookmarks.back);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsBookmarksFore', settings2.table.content.bookmarks.fore);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesGlobal', settings2.table.content.globalName);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesParens', settings2.table.content.parens);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesCommas', settings2.table.content.comma);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesNumbers', settings2.table.content.numbers);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesStrings', settings2.table.content.strings);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataFore', settings2.table.content.values.data.fore);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPieceFore', settings2.table.content.values.pieceChar.fore);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPieceBack', settings2.table.content.values.pieceChar.back);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPillBack', settings2.table.content.values.piecePills.back);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPillFore', settings2.table.content.values.piecePills.fore);

    // dark
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsBookmarkHeaderBackDark', theme2.table.headers.bookmark.back);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsHeadersForeDark', theme2.table.headers.fore);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsHeadersBackDark', theme2.table.headers.back);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsBookmarksBackDark', theme2.table.content.bookmarks.back);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsBookmarksForeDark', theme2.table.content.bookmarks.fore);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesGlobalDark', theme2.table.content.globalName);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesParensDark', theme2.table.content.parens);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesCommasDark', theme2.table.content.comma);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesNumbersDark', theme2.table.content.numbers);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsNodesStringsDark', theme2.table.content.strings);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataForeDark', theme2.table.content.values.data.fore);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPieceForeDark', theme2.table.content.values.pieceChar.fore);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPieceBackDark', theme2.table.content.values.pieceChar.back);

    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPillBackDark', theme2.table.content.values.piecePills.back);
    app.ui.gViewer.settings.populateFields('inpGviewerSettingsDataPillForeDark', theme2.table.content.values.piecePills.fore);
};

app.ui.gViewer.settings.confirm = {};

app.ui.gViewer.settings.confirm.isChanged = {};

app.ui.gViewer.settings.confirm.init = () => {
    $('#btnGviewerSettingsConfirmOk').on('click', () => app.ui.gViewer.settings.confirm.okClicked());

    $('#modalGviewerSettingsConfirm').on('hide.bs.modal', () => {
        $('.modal-backdrop').css('z-index', 1040)
    });
};

app.ui.gViewer.settings.confirm.show = isChanged => {
    const chkGviewerSettingsConfirmSettings = $('#chkGviewerSettingsConfirmSettings');
    const chkGviewerSettingsConfirmCurrentTab = $('#chkGviewerSettingsConfirmCurrentTab');
    const chkGviewerSettingsConfirmAllTabs = $('#chkGviewerSettingsConfirmAllTabs');

    app.ui.gViewer.settings.confirm.isChanged = isChanged;

    // uncheck all check boxes
    chkGviewerSettingsConfirmSettings.prop('checked', false);
    chkGviewerSettingsConfirmCurrentTab.prop('checked', false);
    chkGviewerSettingsConfirmAllTabs.prop('checked', false);

    // reset first check box
    chkGviewerSettingsConfirmSettings.attr('disabled', false);

    // determine how many tabs are open
    let tabsCount = 0;
    for (const tabId in app.ui.gViewer.instance) {
        if (tabId.indexOf('G') > -1) tabsCount++
    }

    // enabled now 2nd and 3rd checkbox according to changes
    chkGviewerSettingsConfirmCurrentTab.attr('disabled', !(isChanged.pathBar === true || isChanged.themeDark || isChanged.themeLight || isChanged.buffer));
    chkGviewerSettingsConfirmAllTabs.attr('disabled', !(tabsCount > 1 && (isChanged.pathBar === true || isChanged.themeDark || isChanged.themeLight || isChanged.buffer)));

    // and auto check the first checkbox if the other two are disabled
    if (chkGviewerSettingsConfirmCurrentTab.attr('disabled') === 'disabled' && chkGviewerSettingsConfirmAllTabs.attr('disabled') === 'disabled') {
        chkGviewerSettingsConfirmSettings.prop('checked', true)
    }

    $('#modalGviewerSettingsConfirm').modal({show: true, backdrop: 'static'});
};

app.ui.gViewer.settings.confirm.okClicked = () => {
    const chkGviewerSettingsConfirmSettings = $('#chkGviewerSettingsConfirmSettings');
    const chkGviewerSettingsConfirmCurrentTab = $('#chkGviewerSettingsConfirmCurrentTab');
    const chkGviewerSettingsConfirmAllTabs = $('#chkGviewerSettingsConfirmAllTabs');

    if (chkGviewerSettingsConfirmSettings.is(':checked') === false && chkGviewerSettingsConfirmCurrentTab.is(':checked') === false && chkGviewerSettingsConfirmAllTabs.is(':checked') === false) {
        app.ui.msgbox.show('You must select at least one option.', 'WARNING')

        return
    }

    // process data

    // settings
    if (chkGviewerSettingsConfirmSettings.is(':checked') === true) {
        app.ui.gViewer.settings.updateSettings()
    }

    // current tab
    if (chkGviewerSettingsConfirmCurrentTab.is(':checked') === true) {
        app.ui.gViewer.settings.updateTabs(app.ui.gViewer.settings.currentTabId, app.ui.gViewer.settings.confirm.isChanged)
    }

    // all tabs
    if (chkGviewerSettingsConfirmAllTabs.is(':checked') === true) {
        app.ui.gViewer.settings.updateTabs(null, app.ui.gViewer.settings.confirm.isChanged)
    }

    // close both dialogs
    $('#modalGviewerSettings').modal('hide');
    $('#modalGviewerSettingsConfirm').modal('hide');
};
