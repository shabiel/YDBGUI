/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file cotdanins commonly used functions for the stats

app.ui.stats.utils = {
    getViewFromId: viewId => {
        let currentView

        app.ui.stats.entry.data.forEach(source => {
            source.data.views.forEach(view => {
                if (view.id === viewId) currentView = view
            })
        })

        return currentView
    },

    getSourceFromViewId: viewId => {
        let currentSource

        app.ui.stats.entry.data.forEach(source => {
            source.data.views.forEach(view => {
                if (view.id === viewId) currentSource = source
            })
        })

        return currentSource

    },

    procObjToString: source => {
        let proc
        switch (source.data.processes.type) {
            case 'agg':
                proc = 'Aggregate';
                break
            case 'pids':
                proc = 'PIDs: ' + source.data.processes.pids.join(', ');
                break
            case 'top':
                proc = 'Top ' + source.data.processes.count + ' of ' + source.data.processes.sampleCount
        }

        return proc
    },

    reportHasGraphs: function () {
        let hasGraph = false
        app.ui.stats.entry.data.forEach(source => {
            source.data.views.forEach(view => {
                if (view.type === 'graph') hasGraph = true
            })
        })

        return hasGraph
    },

    mapObjToArray: view => {
        const ret = []

        view.mapping.forEach(map => {
            if (map.abs === true || map.avg === true || map.delta === true || map.min === true || map.max === true) {
                const mapObj = {
                    sample: map.sample,
                    data: []
                }

                if (map.abs === true) mapObj.data.push('abs')
                if (map.avg === true) mapObj.data.push('avg')
                if (map.delta === true) mapObj.data.push('delta')
                if (map.min === true) mapObj.data.push('min')
                if (map.max === true) mapObj.data.push('max')

                ret.push(mapObj)
            }
        })

        return ret
    },

    createNewEntry: function () {
        return JSON.parse(JSON.stringify({
            version: 2.1,
            fileType: 'ydb-stats-report',
            name: '',
            description: '',
            data: [],
            sparkChart: {
                graphsConfig: {
                    orientation: 'v',
                    height: 300
                },
                theme: app.userSettings.stats.sparkLines.theme
            }
        }))
    },

    graph: {
        convertSeriesName: function (seriesName, source = {}) {
            const typeA = seriesName.split('-')
            let sampleType = ''

            switch (typeA[2]) {
                case 'abs':
                    sampleType = typeA[2];
                    break
                case 'delta':
                    sampleType = 'Δ';
                    break
                default:
                    sampleType = 'Δ ' + typeA[2]
            }

            if (source.data && source.data.processes.type === 'pids') {
                typeA[0] = source.data.processes.pids[typeA[0]]
            }

            if (source.type === 'fhead' || source.type === 'peekByName') {
                typeA[1] = typeA[1].split('.')
                typeA[1] = typeA[1][typeA[1].length - 1]
            }

            return typeA[0] === '*' ? typeA[1] + ' ' + sampleType : 'P [' + typeA[0] + '] ' + typeA[1] + ' ' + sampleType
        }
    },

    computeStatusBarSampleRate: function () {
        const sampleRateA = []
        let sampleRateString = 'N/A'
        let sampleRateCaption = 'Sample rate:'

        if (app.ui.stats.entry.data.length > 0) {
            app.ui.stats.entry.data.forEach(source => {
                if (source.data.enabled === true) {
                    sampleRateA.push(source.sampleRate)
                }
            })

            if (sampleRateA.length > 0) {
                sampleRateString = ''
                if (sampleRateA.length > 1) {
                    sampleRateCaption = 'Sample rate(s):'
                }
                sampleRateA.forEach(item => {
                    sampleRateString += item + ' / '
                })
                sampleRateString = sampleRateString.slice(0, -3) + ' secs'
            }
        }

        $('#lblStatsStatusBarSampleRateValue').text(sampleRateString)
        $('#lblStatsStatusBarSampleRateCaption').text(sampleRateCaption)
    }
}
