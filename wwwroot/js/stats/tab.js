/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the stats tollbar

app.ui.stats.tab = {
    toolbar: {
        start: async () => {
            if (app.ui.stats.tab.toolbar.status === 'running') return

            try {
                if (app.ui.stats.tab.toolbar.status === 'stopped') {
                    await app.ui.stats.pids.checkReport()

                    app.ui.stats.results.startAcquisition()

                    await app.statistics.exec.start()
                    $('#lblStatsControllerSamples').text(0)

                } else {
                    await app.statistics.exec.resume()
                }

                $('#lblStatsControllerStatus').text('Running')

                $('#btnStatsPause > i').addClass('bi-pause-circle-fill').removeClass('bi-play-circle-fill')

                app.ui.stats.tab.toolbar.status = 'running'
                app.ui.stats.tab.toolbar.computeToolbarStatus()

            } catch (err) {
                console.log(err)
            }
        },

        stop: async () => {
            try {
                await app.statistics.exec.stop()

                app.ui.stats.results.endAcquisition()

                $('#lblStatsControllerStatus').text('Stopped')

                app.ui.stats.tab.toolbar.status = 'stopped'
                app.ui.stats.tab.toolbar.computeToolbarStatus()
                $('#btnStatsPause > i').addClass('bi-pause-circle-fill').removeClass('bi-play-circle-fill')

            } catch (err) {
                if (err.error && err.error.description === 'Scheduler not running') return

                console.log(err)
            }
        },

        pause: async () => {
            const btnStatsPause = $('#btnStatsPause')
            const btnStatsPauseI = $('#btnStatsPause > i')

            try {
                if (app.ui.stats.tab.toolbar.status == 'paused') {
                    await app.statistics.exec.resume()

                    $('#lblStatsControllerStatus').text('Running')

                    app.ui.stats.tab.toolbar.status = 'running'

                    btnStatsPauseI.addClass('bi-pause-circle-fill').removeClass('bi-play-circle-fill')
                    btnStatsPause.attr('title', 'Pause')

                } else {
                    await app.statistics.exec.pause()

                    $('#lblStatsControllerStatus').text('Paused')

                    app.ui.stats.tab.toolbar.status = 'paused'

                    btnStatsPauseI.removeClass('bi-pause-circle-fill').addClass('bi-pause-circle-fill')
                    btnStatsPause.attr('title', 'Resume')
                }

                app.ui.stats.tab.toolbar.computeToolbarStatus()

                app.ui.stats.tab.toolbar.processExportIcon(true)

            } catch (err) {
                console.log(err)
            }
        },

        showSources: async () => {
            app.ui.stats.sources.show()
        },

        statsDefs: () => {
            app.ui.stats.sparkChart.show()
        },

        switchTheme: () => {
            $('#btnStatsTheme').attr('title', app.ui.stats.entry.sparkChart.theme === 'light' ? 'Switch to light theme' : 'Switch to dark theme')

            $('#btnStatsTheme > i')
                .removeClass(app.ui.stats.entry.sparkChart.theme !== 'dark' ? 'bi-brightness-high-fill' : 'bi-moon-fill')
                .addClass(app.ui.stats.entry.sparkChart.theme === 'dark' ? 'bi-brightness-high-fill' : 'bi-moon-fill')

            app.ui.stats.entry.sparkChart.theme = app.ui.stats.entry.sparkChart.theme === 'light' ? 'dark' : 'light'

            app.ui.stats.sparkChart.renderer.switchTheme()

            // update tab header with dirty status
            $('#lblStatsReportName').html('[' + app.ui.stats.entry.name + '<span style="color: red;">*</span>]')
            app.ui.stats.entryDirty = true
        },

        processExportIcon: function (show) {
            if (app.ui.stats.sparkChart.renderer.sections.graphs.chart1 !== null && app.ui.stats.sparkChart.renderer.sections.graphs.graphData[0].view.defaults.toolbar.show === true) {
                app.ui.stats.sparkChart.renderer.sections.graphs.chart1.updateOptions({
                    chart: {
                        toolbar: {
                            tools: {
                                download: show
                            }
                        }
                    }
                })
            }

            if (app.ui.stats.sparkChart.renderer.sections.graphs.chart2 !== null && app.ui.stats.sparkChart.renderer.sections.graphs.graphData[1].view.defaults.toolbar.show === true) {
                app.ui.stats.sparkChart.renderer.sections.graphs.chart2.updateOptions({
                    chart: {
                        toolbar: {
                            tools: {
                                download: show
                            }
                        }
                    }
                })
            }
        },

        computeToolbarStatus: function () {
            const btnStatsRecord = $('#btnStatsRecord')
            const btnStatsRecordI = $('#btnStatsRecordI')
            const btnStatsPause = $('#btnStatsPause')
            const btnStatsStop = $('#btnStatsStop')

            switch (this.status) {
                case 'noSources': {
                    app.ui.button.disable(btnStatsRecord)
                    btnStatsRecordI.css('color', 'var(--ydb-purple)')

                    app.ui.button.disable(btnStatsPause)
                    app.ui.button.disable(btnStatsStop)

                    $('#spanStatsEmptySparkchart').css('display', 'block')

                    break
                }
                case 'running': {
                    this.flash.stop()

                    btnStatsRecordI.css('color', 'var(--ydb-status-red)')

                    app.ui.button.enable(btnStatsPause)
                    app.ui.button.enable(btnStatsStop)

                    break
                }
                case 'stopped': {
                    this.flash.stop()

                    app.ui.button.enable(btnStatsRecord)
                    btnStatsRecordI.css('color', 'var(--ydb-purple)')

                    app.ui.button.disable(btnStatsPause)
                    app.ui.button.disable(btnStatsStop)

                    break
                }
                case 'paused': {
                    btnStatsRecordI.css('color', 'var(--ydb-status-red)!important')
                    app.ui.button.enable(btnStatsRecord)

                    app.ui.button.enable(btnStatsPause)
                    app.ui.button.enable(btnStatsStop)

                    this.flash.start()

                    break
                }
            }

            this.setRunningMode()
        },

        setRunningMode: function () {
            const btnStatsSources = $('#btnStatsSources')
            const btnStatsDef = $('#btnStatsDef')
            const btnStatsNew = $('#btnStatsNew')
            const btnStatsLoad = $('#btnStatsLoad')
            const btnStatsSave = $('#btnStatsSave')

            switch (this.status) {
                case 'running':
                case 'paused': {
                    btnStatsSources
                        .addClass('disabled')
                        .attr('disabled', true)
                    btnStatsDef
                        .addClass('disabled')
                        .attr('disabled', true)
                    btnStatsNew
                        .addClass('disabled')
                        .attr('disabled', true)
                    btnStatsLoad
                        .addClass('disabled')
                        .attr('disabled', true)
                    btnStatsSave
                        .addClass('disabled')
                        .attr('disabled', true)

                    break
                }

                default: {
                    btnStatsSources
                        .removeClass('disabled')
                        .attr('disabled', false)
                    btnStatsDef
                        .removeClass('disabled')
                        .attr('disabled', false)
                    btnStatsNew
                        .removeClass('disabled')
                        .attr('disabled', false)
                    btnStatsLoad
                        .removeClass('disabled')
                        .attr('disabled', false)
                    btnStatsSave
                        .removeClass('disabled')
                        .attr('disabled', false)
                }
            }
        },

        newStat: async function () {
            let dirtyStatus

            try {
                dirtyStatus = await app.ui.stats.storage.load.processDirty()

            } catch (e) {
                return
            }

            setTimeout(() => {
                app.ui.inputbox.show('This will clear all the sources and related reports. Are you sure ?', 'WARNING', async ret => {
                    if (ret === 'YES') {
                        app.ui.stats.entry = app.ui.stats.utils.createNewEntry()

                        app.statistics.initData()

                        await app.ui.stats.tab.resetGraphs()

                        app.ui.stats.sparkChart.renderer.render($('#divStatsSparkchart'))

                        app.ui.stats.tab.toolbar.status = 'noSources'
                        this.computeToolbarStatus()

                        // update tab header
                        $('#lblStatsReportName').text('')
                        app.ui.stats.entryDirty = false

                        // update status bar
                        $('#lblStatsStatusBarSampleRateValue').text('N/A')
                    }
                })
            }, dirtyStatus === true ? 0 : 500)
        },

        load: function () {
            app.ui.stats.storage.load.show()
        },

        save: function () {
            app.ui.stats.storage.save.show()
        },

        flash: {
            start: function () {
                this.hInterval = setInterval(() => {
                    this.flag = !this.flag

                    $('#btnStatsRecordI').css('color', this.flag === true ? 'var(--ydb-purple)' : 'var(--ydb-status-red')

                }, 350)
            },

            stop: function () {
                clearInterval(this.hInterval)
            },

            flag: false,
            hInterval: null
        },

        status: 'noSources'                                  // can be: stopped, running, paused, noSources
    },

    resetGraphs: async function () {
        if (app.ui.stats.sparkChart.renderer.sections.graphs.chart1 !== null) {
            await app.ui.stats.sparkChart.renderer.sections.graphs.chart1.destroy()
            app.ui.stats.sparkChart.renderer.sections.graphs.chart1 = null
        }

        if (app.ui.stats.sparkChart.renderer.sections.graphs.chart2 !== null) {
            await app.ui.stats.sparkChart.renderer.sections.graphs.chart2.destroy()
            app.ui.stats.sparkChart.renderer.sections.graphs.chart2 = null
        }
    }
}
