/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

app.ui.stats.results.export = {
    init: function () {
        $('#swcStatsResultsExportHeader').on('click', () => this.headerClicked())

        $('#btnStatsResultsExportOk').on('click', () => this.export())
        $('#btnStatsResultExportHelp').on('click', () => this.helpClicked())

        app.ui.setupDialogForTest('modalStatsResultExport')
    },

    show: function () {
        const agent = window.navigator.userAgent.toLowerCase()
        const macosPlatforms = /(macintosh|macintel|macppc|mac68k|macos)/i
        const windowsPlatforms = /(win32|win64|windows|wince)/i
        const iosPlatforms = /(iphone|ipad|ipod)/i
        const selStatsResultsExportRowTerminator = $('#selStatsResultsExportRowTerminator')

        this.source = app.ui.stats.results.tabsData[app.ui.stats.results.tabIx].source

        // detect platform and suggest line separator
        if (macosPlatforms.test(agent)) {
            $('#selStatsResultsExportRowTerminator option[value="LF"]').prop('selected', true)
        } else if (iosPlatforms.test(agent)) {
            $('#selStatsResultsExportRowTerminator option[value="LF"]').prop('selected', true)
        } else if (windowsPlatforms.test(agent)) {
            $('#selStatsResultsExportRowTerminator option[value="CRLF"]').prop('selected', true)
        } else if (/android/.test(agent)) {
            $('#selStatsResultsExportRowTerminator option[value="LF"]').prop('selected', true)
        } else if (/linux/.test(agent)) {
            $('#selStatsResultsExportRowTerminator option[value="LF"]').prop('selected', true)
        }

        // reset the dialog
        $('#swcStatsResultsExportSource').prop('checked', false)
        $('#selStatsResultExportTimestampFormat option[value="ISO8601"]').prop('selected', true)
        this.headerClicked()

        $('#swcStatsResultsExportHeader').prop('checked', true)
        $('#divStatsResultsExportHeaderFields').css('display', this.source.data.regions.length === 1 && this.source.data.processes.type === 'agg' ? 'none' : 'flex')

        $('#modalStatsResultExport')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    headerClicked: function () {
        const display = this.source.data.regions.length === 1 && this.source.data.processes.type === 'agg'

        if (display === true) {
            $('#divStatsResultsExportHeaderFields').css('display', 'none')

        } else {
            const isChecked = $('#swcStatsResultsExportHeader').is(':checked')
            $('#divStatsResultsExportHeaderFields').css('display', isChecked === true ? 'flex' : 'none')
            $('#divStatsResultsExportHeaderRegions').css('display', this.source.data.regions.length === 1 ? 'none' : 'flex')
            $('#divStatsResultsExportHeaderProcesses').css('display', this.source.data.processes.type === 'agg' ? 'none' : 'flex')
            $('#divStatsResultsExportHeaderSampleValue').css('display', this.source.data.regions.length === 1 && this.source.data.processes.type === 'agg' ? 'none' : 'flex')
        }
    },

    helpClicked: function () {
        app.ui.help.show('stats/export')
    },

    export: async function () {
        this.doc = []
        this.setupSeparator()

        // process header if needed
        if ($('#swcStatsResultsExportSource').is(':checked') === true) this.createHeader()

        this.appendData()

        if (navigator.userAgent.indexOf("Firefox") != -1) {
            // *************************************
            // firefox
            // *************************************
            try {
                let blob = new Blob([this.doc.join(this.separator)], {
                    type: "script"
                })
                let a = document.createElement("a")
                a.href = URL.createObjectURL(blob)
                a.download = 'untitled.csv'
                a.hidden = true
                document.body.appendChild(a)
                a.innerHTML =
                    ""
                a.click()
                a.remove()
            } catch (err) {
                app.ui.msgbox.show(app.REST.parseError(err), 'Warning')
            }

        } else if (navigator.userAgent.indexOf("Chrome") != -1) {
            // *************************************
            // chrome
            // *************************************
            try {
                const handle = await showSaveFilePicker({
                    suggestedName: 'untitled.csv',
                    types: [{
                        description: 'YottaDB Statistics Data',
                        accept: {'text/csv': ['.csv']},
                    }],
                });

                const blob = new Blob([this.doc.join(this.separator)]);

                const writableStream = await handle.createWritable();
                await writableStream.write(blob);
                await writableStream.close();

            } catch (err) {
                if (err.toString().indexOf('aborted') > -1) return
                if (err.toString().indexOf('showSaveFilePicker') > -1) return

                const errorMessage = app.REST.parseError(err)

                app.ui.msgbox.show(errorMessage.indexOf('showSaveFilePicker') > -1 ? 'This option is available only when using HTTP. Consider using Firefox for this operation' : errorMessage, 'Warning')

            }
        } else {
            app.ui.msgbox.show('Your browser is not supported for this operation.<br><br>Please report your browser type and vendor to YDB support.', 'Warning')
        }

        $('#modalStatsResultExport').modal('hide')
    },

    createHeader: function () {
        this.doc.push('Source')
        this.doc.push('Type, ' + this.source.type.toUpperCase())
        this.doc.push('Sample rate:, ' + this.source.sampleRate + ' sec' + (parseInt(this.source.sampleRate) > 1 ? 's' : ''))

        if (this.source.type === 'ygbl') {
            this.doc.push('Regions, ' + this.source.data.regions.join(', '))

            let processesText
            switch (this.source.data.processes.type) {
                case 'agg': {
                    processesText = 'All processes'

                    break
                }
                case 'pids': {
                    processesText = this.source.data.processes.pids.join(', ')

                    break
                }
                case 'top': {
                    processesText = 'TOP ' + this.source.data.processes.count + ' of ' + this.source.data.processes.sampleCount

                    break
                }
            }
            this.doc.push('Processes: , ' + processesText)
        }

        this.doc.push('Samples:, ' + this.source.data.samples.join(', '))
        this.doc.push('')
    },

    appendData: function () {
        const data = app.ui.stats.results.tabsData[app.ui.stats.results.tabIx].data

        // add header is requested
        if ($('#swcStatsResultsExportHeader').is(':checked') === true) {
            let rowData = 'Timestamp, '

            data.forEach(dataRow => {
                if ($('#swcStatsResultsExportHeaderRegions').is(':checked') === true) rowData += dataRow.region + '-'
                if ($('#swcStatsResultsExportHeaderProcesses').is(':checked') === true) rowData += dataRow.pid[0].pid + '-'
                rowData += dataRow.sample + '-'
                rowData += dataRow.sampleType

                rowData += ','
            })

            this.doc.push(rowData.slice(0, -1))
        }

        // process data
        data[0].array.forEach((row, rowIx) => {
            let rowData = this.formatTimestamp(row.timestamp) + ','

            data.forEach(dataRow => {
                rowData += dataRow.array[rowIx][dataRow.sampleType] + ','
            })

            this.doc.push(rowData.slice(0, -1))
        })
    },

    setupSeparator: function () {
        switch ($('#selStatsResultsExportRowTerminator').val()) {
            case 'LF': {
                this.separator = '\n'

                break
            }
            case 'CRLF': {
                this.separator = '\r\n'
            }
        }
    },

    formatTimestamp: function (timestamp) {
        const option = $('#selStatsResultExportTimestampFormat').val()
        const date = new Date(timestamp)
        let formattedTimestamp

        switch (option) {
            case 'ISO8601': {
                formattedTimestamp = date.toISOString()

                break
            }
            case 'DateTime': {
                formattedTimestamp = date.getFullYear() + '/' + date.getMonth().toString().padStart(2, '0') + '/' + date.getDay().toString().padStart(2, '0') + ' ' +
                    date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0') + ':' + date.getSeconds().toString().padStart(2, '0')

                break
            }
            case 'DateTimeMilliseconds': {
                formattedTimestamp = date.getFullYear() + '/' + date.getMonth().toString().padStart(2, '0') + '/' + date.getDay().toString().padStart(2, '0') + ' ' +
                    date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0') + ':' + date.getSeconds().toString().padStart(2, '0') + '.' +
                    date.getMilliseconds()

                break
            }
            case 'Time': {
                formattedTimestamp = date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0') + ':' + date.getSeconds().toString().padStart(2, '0')

                break
            }
            case 'TimeMilliseconds': {
                formattedTimestamp = date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0') + ':' + date.getSeconds().toString().padStart(2, '0') + '.' + date.getMilliseconds()

                break
            }
            case 'UnixEpoch': {
                formattedTimestamp = timestamp.toString()
            }
        }

        return formattedTimestamp
    },

    source: {},
    doc: [],
    separator: '',
}
