/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// this is the main file for the stats UI. It manages the tab open and close and the splitter resize used in multiple graphs (sparkChart)

app.ui.stats = {
    init: () => {
        app.ui.stats.entry = app.ui.stats.utils.createNewEntry()
    },

    showTab: async function () {
        if (app.system.node.status === 'N/A') {
            app.ui.msgbox.show('We could not find the node.js framework installed under the Web server username.<br><br></br>' +
                'Please, install the framework and try again', 'Error')

            return
        }

        if (window.location.protocol === 'https:' && navigator.userAgent.toLowerCase().includes('firefox')) {
            await app.ui.msgbox.show('You are using Firefox on an HTTPS connection. If your certificates are self-signed, statistics won\'t work.<br><br>We advise you to use the Chrome browser for this.', '', true)
        }

        // ********************************
        // start, if needed, socket server
        // ********************************
        app.ui.wait.show('Starting the Web Socket server...')

        const serverStatus = await app.websockets.startServer()

        if (serverStatus === -1) {
            app.ui.wait.hide()

            app.ui.msgbox.show('The socket server is in use. Close the other session and try again...', 'Warning')

            return

        } else if (serverStatus === false) {
            app.ui.wait.hide()

            return
        }

        // ********************************
        // initialize web sockets
        // ********************************
        if (app.websockets.connected == false) {
            try {
                app.statistics.init()
                const res = await app.websockets.init(app.REST.host, (parseInt(app.REST.port) + 1), true)

                app.ui.wait.hide()

                console.log(res)

            } catch (err) {
                console.log(err)
                app.ui.msgbox.show('Sockets not initialized. Error is: ' + err, 'ERROR')

                app.ui.wait.hide()

                return
            }

        } else {
            app.ui.wait.hide()
        }

        app.ui.tabs.select('-S');

        // ********************************
        // initialize toolbar
        // ********************************
        app.ui.stats.tab.toolbar.computeToolbarStatus()

        // display message if needed
        if (app.userSettings.stats.displayHelpAtTabOpen === true) {
            const ret = await app.ui.msgboxCheck.show('In order to view YGBLSTAT statistics, you need to enable the statistics collection for your database.<br><br>' +
                'You can find instructions on how to do set it up <br>- using environment variables  <a target="_blank" href="https://docs.yottadb.com/AdminOpsGuide/basicops.html#ydb-statshare">here</a><br>' +
                '- or through M code <a target="_blank" href="https://docs.yottadb.com/ProgrammersGuide/commands.html#no-statshare-region-list">here</a><br><br>' +
                'NOTE: You can always get help by clicking on the help icon, located in the tab toolbar and in each dialog (it will provide context-sensitive help)', 'HELP', 'Don\'t show this again', true)

            if (ret === true) {
                app.userSettings.stats.displayHelpAtTabOpen = false
                app.ui.storage.save('stats', app.userSettings.stats)
            }
        }

        // ensure all HTML is properly set
        app.ui.stats.results.init()
        app.ui.stats.results.show('acq')
        app.ui.stats.results.startAcquisition()

        app.ui.setTestClosure('tab-SDiv')
    },

    switchTheme: function () {
        $('#btnStatsTheme').attr('title', app.ui.stats.entry.sparkChart.theme === 'dark' ? 'Switch to light theme' : 'Switch to dark theme')
        $('#btnStatsTheme > i')
            .removeClass(app.ui.stats.entry.sparkChart.theme === 'dark' ? 'bi-brightness-high-fill' : 'bi-moon-fill')
            .addClass(app.ui.stats.entry.sparkChart.theme !== 'dark' ? 'bi-brightness-high-fill' : 'bi-moon-fill')
    },

    createInstance: function (type) {
        const newViewer = $('#tabStats').clone();
        const newTabId = 'tab' + type + 'Div';

        $('#tabsDivsContainer').append(newViewer);

        $('#tabsDivsContainer > #tabStats').prop('id', newTabId);
        const newTabDiv = $('#' + newTabId);

        newTabDiv
            .attr('aria-labelledby', 'tab' + type)
            .css('display', 'block')

        $('#btnStatsRecord').on('click', () => app.ui.stats.tab.toolbar.start());
        $('#btnStatsPause').on('click', () => app.ui.stats.tab.toolbar.pause());
        $('#btnStatsStop').on('click', () => app.ui.stats.tab.toolbar.stop());

        $('#btnStatsNew').on('click', () => app.ui.stats.tab.toolbar.newStat());
        $('#btnStatsLoad').on('click', () => app.ui.stats.tab.toolbar.load());
        $('#btnStatsSave').on('click', () => app.ui.stats.tab.toolbar.save());

        $('#btnStatsSources').on('click', () => app.ui.stats.tab.toolbar.showSources());
        $('#btnStatsDef').on('click', () => app.ui.stats.tab.toolbar.statsDefs());
        $('#btnStatsTheme').on('click', () => app.ui.stats.tab.toolbar.switchTheme())

        $('#divStatsToolbar').on('click', () => app.ui.stats.sparkChart.renderer.sections.graphs.tools.reset())

        $('#btnStatsGraph1Yaxis').on('click', e => app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.show(1, e))
        $('#btnStatsGraph1Timescale').on('click', e => app.ui.stats.sparkChart.renderer.sections.graphs.tools.range.show(1, e))
        $('#btnStatsGraph2Yaxis').on('click', e => app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.show(2, e))
        $('#btnStatsGraph2Timescale').on('click', e => app.ui.stats.sparkChart.renderer.sections.graphs.tools.range.show(2, e))

        // this callback manages the repositioning of the icons on the graphs when window change size
        new ResizeObserver(e => app.ui.stats.sparkChart.renderer.sections.graphs.resize(e)).observe(document.getElementById('divStatsSparkChartGraphsContainer'));

        this.switchTheme()

        $('#btnStatsHelp').on('click', () => app.ui.help.show('stats/index'));

        $('#divStatsSparkChartGraphsContainer')
            .resizable({
                handleSelector: "#divStatsSparkChartSplitter",
                resizeWidth: false,
                handles: 's',
                // commented out in case we need the function again
                //resize: (event, ui) => this.onSplitterResize(event, ui),
            })

        app.ui.stats.tab.toolbar.status = 'noSource'
        app.ui.stats.tab.toolbar.computeToolbarStatus()

        $('#divStatsScroller').css('height', 'calc(100vh - ' + 211 + 'px)')
    },

    onSplitterResize: function (event, ui) {
        // update reportLine container height
        app.ui.stats.entry.sparkChart.graphsConfig.height = ui.size.height
        $('#lblStatsReportName').html('[' + app.ui.stats.entry.name + '<span style="color: red;">*</span>]')
        app.ui.stats.entryDirty = true

        const offset = ui.size.height + 238
        //($('#divStatsSparkChartSplitter').css('display', 'block') ? 238 : 238)
        //$('#divStatsSparkChartSparkChartContainer').css('height', 'calc(100vh - ' + offset + 'px)')
        //$('#divStatsScroller').css('height', 'calc(100vh - ' + (ui.size.height - 75) + 'px)')

        // update graph(s)
        if ($('#divStatsSparkChartGraph1').css('display') !== 'none') {
            let chart1 = app.ui.stats.sparkChart.renderer.sections.graphs.chart1
            let chart2 = app.ui.stats.sparkChart.renderer.sections.graphs.chart2
            const orientation = app.ui.stats.entry.sparkChart.graphsConfig.orientation

            if (chart1 !== null) {
                chart1.canvas.parentNode.style.height = (parseInt($('#divStatsSparkChartGraphsContainer').css('height')) - 7) / (orientation === 'h' && chart2 !== null ? 2 : 1)
                chart1.canvas.width = chart1.canvas.parentNode.style.width
            }

            if (chart2 !== null) {
                chart2.canvas.parentNode.style.height = (parseInt($('#divStatsSparkChartGraphsContainer').css('height')) - 7) / (orientation === 'h' ? 2 : 1)
                chart2.canvas.width = chart1.canvas.parentNode.style.width
            }
        }
    },

    closeTab: async function () {
        return new Promise(async function (resolve, reject) {
            if (app.ui.stats.entryDirty === true) {
                app.ui.inputbox.show('The report file has been modified.<br><br>Do you want to save it ?', 'WARNING', ret => {
                    if (ret === 'YES') {
                        app.ui.stats.tab.toolbar.save()

                        reject()
                    } else {
                        app.ui.stats.resetTab()

                        resolve()
                    }
                })

            } else {
                app.ui.stats.resetTab()

                resolve()
            }
        })
    },

    resetTab: async function () {
        if (app.websockets.connected === true) {
            await app.ui.stats.tab.toolbar.stop()

            if (app.websockets.connected === true) {
                app.websockets.terminating = true
                await app.websockets.terminate()
                // the flag reset is delayed to prevent msgbox to be displayed on: on('close')
                setTimeout(() => {app.websockets.terminating = false}, 1000)

                app.websockets.connected = false
            }

            app.websockets.serverPid = 0

            // release all memory from captured data
            app.ui.stats.entry = app.ui.stats.utils.createNewEntry()
            app.ui.stats.entryDirty = false
            app.statistics.data = []
            app.statistics.sourceMapping = []
            await app.ui.stats.tab.resetGraphs()
        }
    },

    sources: {},
    tab: {},

    entry: {},
    entryDirty: false,

    findEntryById: function (id, entry) {
        return entry.data.find(entry => entry.id === id)
    },
}

