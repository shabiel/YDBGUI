/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the sparkChart report mapping

app.ui.stats.sparkChart.mapping = {
    init: function () {
        $('#btnStatsSparkChartMappingOk').on('click', () => this.okPressed())
        $('#btnStatsSparkChartMappingHelp').on('click', () => app.ui.help.show('stats/spark-chart/mapping'))

        $('#chkStatsSparkcChartMappingAbs').on('click', () => this.selectAll('Abs'))
        $('#chkStatsSparkcChartMappingDelta').on('click', () => this.selectAll('Delta'))
        $('#chkStatsSparkcChartMappingMin').on('click', () => this.selectAll('Min'))
        $('#chkStatsSparkcChartMappingMax').on('click', () => this.selectAll('Max'))
        $('#chkStatsSparkcChartMappingAvg').on('click', () => this.selectAll('Avg'))

        app.ui.setupDialogForTest('modalStatsSparkChartMapping')
    },

    show: function (source, viewId) {
        this.source = source
        const view = this.view = source.data.views.find(view => view.id === viewId)
        const tblStatsSparkcChartMapping = $('#tblStatsSparkcChartMapping > tbody')
        let rows = ''
        this.mappingColorIx = 0

        if (view.type === 'graph' && source.data.regions.length > 1) {
            const selStatsSparkChartMappingRegion = $('#selStatsSparkChartMappingRegion')
            let rows = ''

            $('#divStatsSparkChartMappingRegion').css('display', 'flex')
            selStatsSparkChartMappingRegion.empty()

            source.data.regions.forEach(region => {
                rows += '<option value="' + region + '">' + region + '</option>'
            })
            selStatsSparkChartMappingRegion.append(rows)

            this.region = ''

        } else {
            $('#divStatsSparkChartMappingRegion').css('display', 'none')

            this.region = source.data.regions[0]
        }

        tblStatsSparkcChartMapping.empty()

        $('#chkStatsSparkcChartMappingAbs').prop('checked', false)
        $('#chkStatsSparkcChartMappingDelta').prop('checked', false)
        $('#chkStatsSparkcChartMappingMin').prop('checked', false)
        $('#chkStatsSparkcChartMappingMax').prop('checked', false)
        $('#chkStatsSparkcChartMappingAvg').prop('checked', false)

        // loop through the samples
        source.data.samples.forEach(sample => {
            const mapping = this.findMapping(view, sample)

            // adjust the headers is necessary
            $('#chkStatsSparkcChartMappingDelta').attr('disabled', mapping.delta === null)
            $('#chkStatsSparkcChartMappingMin').attr('disabled', mapping.min === null)
            $('#chkStatsSparkcChartMappingMax').attr('disabled', mapping.max === null)
            $('#chkStatsSparkcChartMappingAvg').attr('disabled', mapping.avg === null)

            // create the rows with the input checkboxes
            rows += '<tr><td>' + sample + '</td>'

            rows += '<td style="width: 54px; text-align: center;">'
            rows += '<input class="form-check-input" type="checkbox" ' + (mapping.abs === true ? 'checked' : '') + ' style="margin-left: -6px;">'
            rows += '</td>'

            rows += '<td style="width: 54px; text-align: center;">'
            if (mapping.delta !== null) {
                rows += '<input class="form-check-input" type="checkbox" ' + (mapping.delta === true ? 'checked' : '') + ' style="margin-left: -6px;">'
            }
            rows += '</td>'

            rows += '<td style="width: 54px; text-align: center;">'
            if (mapping.min !== null) {
                rows += '<input class="form-check-input" type="checkbox" ' + (mapping.min === true ? 'checked' : '') + ' style="margin-left: -6px;">'
            }
            rows += '</td>'

            rows += '<td style="width: 54px; text-align: center;">'
            if (mapping.max !== null) {
                rows += '<input class="form-check-input" type="checkbox" ' + (mapping.max === true ? 'checked' : '') + ' style="margin-left: -6px;">'
            }
            rows += '</td>'

            rows += '<td style="width: 54px; text-align: center;">'
            if (mapping.avg !== null) {
                rows += '<input class="form-check-input" type="checkbox" ' + (mapping.avg === true ? 'checked' : '') + ' style="margin-left: -6px;">'
            }
            rows += '</td>'

            rows += '</tr>'
        })

        tblStatsSparkcChartMapping.append(rows)

        $('#modalStatsSparkChartMapping')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    findMapping: function (view, sample) {
        return view.mapping.find(map => map.sample === sample)
    },

    selectAll: function (type) {
        const isChecked = $('#chkStatsSparkcChartMapping' + type).is(':checked')
        let typeIx

        switch (type) {
            case 'Abs': {
                typeIx = 1;
                break
            }
            case 'Delta': {
                typeIx = 2;
                break
            }
            case 'Min': {
                typeIx = 3;
                break
            }
            case 'Max': {
                typeIx = 4;
                break
            }
            case 'Avg': {
                typeIx = 5;
                break
            }
            default:
                typeIx = null
        }

        const rows = $('#tblStatsSparkcChartMapping > tbody ').children()

        $(rows).each((ix, row) => {
            const cols = $(row).children()
            $(cols[typeIx]).children().attr('checked', isChecked)
        })
    },

    okPressed: function () {
        const rows = $('#tblStatsSparkcChartMapping > tbody').children()
        const view = this.view
        const isGraph = view.type === 'graph'

        // update params on view for titles and data fetching
        view.region = this.region !== '' ? this.region : $('#selStatsSparkChartMappingRegion').val()
        view.processes = app.ui.stats.utils.procObjToString(this.source)
        if (view.type === 'graph') {
            view.defaults.title.text = view.region
            view.defaults.subTitle.text = view.processes
        }
        view.infoSet = false

        $(rows).each(function () {
            const map = {}

            $(this).children().each(function (index, entry) {
                if (index === 0) map.sample = $(entry).text()

                if (index === 1) {
                    map.abs = $(entry).children().is(':checked')

                    // update graph if needed
                    if (isGraph === true) app.ui.stats.sparkChart.mapping.setGraphMappingEntry(this.source, view, map.sample, 'abs', map.abs)
                }
                if (index === 2) {
                    if ($(entry).children().val() === undefined) {
                        map.delta = null

                    } else {
                        map.delta = $(entry).children().is(':checked')

                        // update graph if needed
                        if (isGraph === true) app.ui.stats.sparkChart.mapping.setGraphMappingEntry(this.source, view, map.sample, 'delta', map.delta)
                    }
                }
                if (index === 3) {
                    if ($(entry).children().val() === undefined) {
                        map.min = null

                    } else {
                        map.min = $(entry).children().is(':checked')

                        // update graph if needed
                        if (isGraph === true) app.ui.stats.sparkChart.mapping.setGraphMappingEntry(this.source, view, map.sample, 'min', map.min)
                    }
                }
                if (index === 4) {
                    if ($(entry).children().val() === undefined) {
                        map.max = null

                    } else {
                        map.max = $(entry).children().is(':checked')

                        // update graph if needed
                        if (isGraph === true) app.ui.stats.sparkChart.mapping.setGraphMappingEntry(this.source, view, map.sample, 'max', map.max)
                    }
                }
                if (index === 5) {
                    if ($(entry).children().val() === undefined) {
                        map.avg = null

                    } else {
                        map.avg = $(entry).children().is(':checked')

                        // update graph if needed
                        if (isGraph === true) app.ui.stats.sparkChart.mapping.setGraphMappingEntry(this.source, view, map.sample, 'avg', map.avg)
                    }
                }
            })

            const ix = view.mapping.findIndex(oldMap => oldMap.sample === map.sample)
            view.mapping[ix] = map
        })

        // ensure only 4 y-axis are visible and set the title
        if (view.graphMapping && view.defaults.yAxis.common === false) {
            let seriesCount = 0

            view.graphMapping.forEach(map => {
                map.settings.yAxis.title.text = app.ui.stats.utils.graph.convertSeriesName(map.type)
                if (this.source.type !== 'ygbl') map.settings.yAxis.title.text = app.ui.stats.sparkChart.renderer.sections.convertLongSampleNames(map.settings.yAxis.title.text)
                if (map.settings.yAxis.show === true) {
                    seriesCount++

                } else {
                    seriesCount++

                    map.settings.yAxis.show = seriesCount <= 4
                }
            })
        }

        if (view.graphMapping && view.graphMapping.length > 7) {
            app.ui.msgbox.show('You have selected entries that will result in 8 or more graph series.<br><br>It is possible that the graph animation will perform poorly and / or the graph will disconnect from the server.', 'Warning')
        }

        $('#modalStatsSparkChartMapping').modal('hide')
    },

    setGraphMappingEntry: function (source, view, sample, sampleType, mapValue) {
        const isTop = this.source.data.processes.type === 'top' || this.source.data.processes.type === 'pids'
        const processCount = isTop === true ? (this.source.data.processes.type === 'top' ? this.source.data.processes.count : this.source.data.processes.pids.length) : 1

        for (let processIx = 0; processIx < processCount; processIx++) {
            const graphMappingId = (processIx === 0 && isTop === false ? '*' : processIx) + '-' + sample + '-' + sampleType

            if (mapValue === true) {
                // set value if doesn't exists already
                if (view.graphMapping.find(graphMap => graphMap.type === graphMappingId) === undefined) {
                    const settings = JSON.parse(JSON.stringify(app.userSettings.stats.sparkLines.sections.graph.series))

                    // colors
                    settings.line.stroke.color = settings.area.stroke.color = app.userSettings.stats.sparkLines.sections.graph.series.colors[this.mappingColorIx]
                    this.mappingColorIx++
                    if (this.mappingColorIx === 12) this.mappingColorIx = 0
                    delete settings.colors

                    // y-axis
                    settings.yAxis = JSON.parse(JSON.stringify(app.userSettings.stats.sparkLines.sections.graph.chart.yAxis.defaultsSeries))

                    view.graphMapping.push({
                        type: graphMappingId,
                        settings: settings
                    })
                }

            } else {
                // remove the value if it exists
                const itemIx = view.graphMapping.findIndex(graphMap => graphMap.type === graphMappingId)
                if (itemIx > -1) view.graphMapping.splice(itemIx, 1)
            }
        }
    },

    view: [],
    source: {},
    mappingColorIx: 0,
    region: '',
}


