/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the sparkChart Graph dialog

app.ui.stats.sparkChart.graph = {
    init: function () {
        $('#btnStatsSparkChartGraphOk').on('click', () => this.okPressed())
        $('#btnStatsSparkChartGraphHelp').on('click', () => app.ui.help.show('stats/spark-chart/graph'))

        $('#btnStatsSparkChartGraphTitle').on('click', () => this.titlePressed())
        $('#btnStatsSparkChartGraphSubTitle').on('click', () => this.subTitlePressed())
        $('#btnStatsSparkChartGraphLegend').on('click', () => this.legendPressed())
        $('#btnStatsSparkChartGraphGrid').on('click', () => this.gridPressed())
        $('#btnStatsSparkChartGraphTooltip').on('click', () => this.tooltipPressed())
        $('#btnStatsSparkChartGraphXaxis').on('click', () => this.xAxisPressed())
        $('#btnStatsSparkChartGraphYaxis').on('click', () => this.yAxisPressed())

        app.ui.setupDialogForTest('modalStatsSparkChartGraph')
    },

    show: function (source, viewId) {
        this.source = source
        this.viewId = viewId
        const view = this.view = JSON.parse(JSON.stringify(source.data.views.find(view => view.id === viewId)))

        if (view.infoSet === false) {
            view.defaults.title.text = view.region
            view.defaults.subTitle.text = view.processes
            view.infoSet = true
        }

        // if fhead or peekByName, disable Y axis button
        app.ui.button.set(
            $('#btnStatsSparkChartGraphYaxis'),
            source.type === 'fhead' || source.type === 'peekByName'
                ? app.ui.button.DISABLED
                : app.ui.button.ENABLED)

        // build sortable list
        let rows = ''
        this.graph.series.count = 0

        view.graphMapping.forEach(map => {
            // build the correct label
            const typeA = map.type.split('-')
            const fullName = app.ui.stats.utils.graph.convertSeriesName(map.type, source)
            const fullNameA = fullName.split(' ')
                let sampleCaption = ''

                switch (source.type) {
                    case 'ygbl': {
                        sampleCaption = app.ui.stats.sources.ygblstats.findSampleByMnemonic(typeA[1]).caption

                        break
                    }
                    case 'fhead': {
                        sampleCaption = app.ui.stats.sources.ygblstats.findSampleByFhead(typeA[1]).caption

                        break
                    }
                    case 'peekByName': {
                        sampleCaption = app.ui.stats.sources.ygblstats.findSampleByPeekByName(typeA[1]).caption
                    }
                }

                const sampleMapHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="' + fullName +
                    '" data-content="<strong>' + typeA[1] + '</strong><br>' + sampleCaption +
                    '<br><br><strong>' + fullNameA[fullNameA.length - 1] + '</strong><br>' + app.statistics.samplesDescription[typeA[2]] + '" '

                rows += '<li class="ui-state-default" id="StatsSparkChartGraphSortable-' + map.type + '" style="color: ' + (map.settings.default === 'line' ? map.settings.line.stroke.color : map.settings.area.stroke.color) + '; cursor: pointer;" ' +
                    sampleMapHelp + '" >'
                rows += '&nbsp;&nbsp;' + fullName + '&nbsp;&nbsp;' +
                    '<button class="btn btn-outline-info btn-sm" tabIndex="10" type="button" style="height: 22px; padding-bottom: 20px; float: right;"' +
                    ' onclick="app.ui.stats.sparkChart.graph.series.show(' + this.graph.series.count + ')">...</button>'
                rows += '</li>'

                this.graph.series.count++
            }
        )

        $('#StatsSparkChartGraphSortable')
            .empty()
            .append(rows)

        app.ui.regeneratePopups()

        $('#StatsSparkChartGraphPreview').empty()

        $('#StatsSparkChartGraphPreview').parent().css('background-color', app.ui.stats.entry.sparkChart.theme === 'light' ? 'white' : 'black')

        $('#modalStatsSparkChartGraph')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})

        // init graph preview
        this.graph.init()
    },

    titlePressed: function () {
        app.ui.prefs.show({
            title: 'Graph title',
            manifest: app.ui.stats.manifest.graph.chart.title,
            target: this.view.defaults.title,
            name: 'statsSparkChartGraphTitle'
        })
    },

    subTitlePressed: function () {
        app.ui.prefs.show({
            title: 'Graph sub title',
            manifest: app.ui.stats.manifest.graph.chart.subTitle,
            target: this.view.defaults.subTitle,
            name: 'statsSparkChartGraphSubTitle'
        })
    },

    legendPressed: function () {
        app.ui.prefs.show({
            title: 'Graph Legend',
            manifest: app.ui.stats.manifest.graph.chart.legend,
            target: this.view.defaults.legend,
            name: 'statsSparkChartGraphLegend'
        })
    },

    gridPressed: function () {
        app.ui.prefs.show({
            title: 'Graph Grid',
            manifest: app.ui.stats.manifest.graph.chart.grid,
            target: this.view.defaults.grid,
            name: 'statsSparkChartGraphGrid'
        })
    },

    xAxisPressed: function () {
        app.ui.prefs.show({
            title: 'Timescale',
            manifest: app.ui.stats.manifest.graph.chart.xaxis,
            target: this.view.defaults.xaxis,
            name: 'statsSparkChartGraphxAxis'
        })

    },

    yAxisPressed: function () {
        if (this.view.defaults.yAxis.common === false) {
            app.ui.msgbox.show('The view is configured to have separate Y-axis for each series.<br><br>Go to the individual series setting to change the Y-axis<br>or use this dialog to switch the \'Shared across series\'', 'Warning')
        }

        app.ui.prefs.show({
            title: 'Y axis',
            manifest: app.ui.stats.manifest.graph.chart.yAxisCommon,
            target: this.view.defaults.yAxis,
            name: 'statsSparkChartGraphYaxisCommon'
        })
    },

    tooltipPressed: function () {
        app.ui.prefs.show({
            title: 'Graph Tooltips',
            manifest: app.ui.stats.manifest.graph.chart.tooltips,
            target: this.view.defaults.tooltips,
            name: 'statsSparkChartGraphTooltips'
        })
    },

    okPressed: function () {
        this.source.data.views.forEach((view, ix) => {
            if (view.id === this.viewId) {
                this.source.data.views[ix] = this.view
            }
        })

        $('#modalStatsSparkChartGraph').modal('hide')
    },

    graph: {
        init: async function () {
            await this.update()
        }
        ,

        update: async function () {
            const seriesData = this.seriesData = []

            app.ui.stats.sparkChart.graph.view.graphMapping.forEach(map => {
                seriesData.push(
                    this.series.createDummySeries(new Date())
                )
            })

            const preview = {series: seriesData}

            if (app.ui.stats.sparkChart.graph.graph.chart !== null) {
                app.ui.stats.sparkChart.graph.graph.chart.destroy()
            }

            const el = document.getElementById('StatsSparkChartGraphPreview')
            app.ui.stats.sparkChart.graph.graph.chart = await app.ui.stats.sparkChart.renderer.sections.graphs.init(el, app.ui.stats.sparkChart.graph.view, preview, 0)
        },

        series: {
            count: 0,

            createDummySeries: function (timestamp) {
                const retArray = []

                timestamp -= 1000

                for (let ix = 0; ix < 10; ix++) {
                    timestamp += 1000

                    retArray.push(
                        {x: timestamp, y: Math.floor(Math.random() * 1500 * ix)}
                    )
                }

                return retArray
            },
        },

        chart: null
    },

    view: [],
    viewId: '',
    source:
        {}
    ,
}
