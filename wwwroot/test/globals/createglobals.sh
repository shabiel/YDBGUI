#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#	This source code contains the intellectual property	        #
#	of its copyright holder(s), and is made available	        #
#	under a license.  If you do not know the terms of	        #
#	the license, please stop and do not read further.	        #
#                                                               #
#################################################################

cd /tmp
wget https://raw.githubusercontent.com/WorldVistA/VistA-M/master/Packages/National%20Drug%20File/Globals/50.6%2BVA%20GENERIC.zwr
wget https://gitlab.com/YottaDB/DBMS/YDBOcto/-/raw/master/tests/fixtures/vista-mini.zwr

mv '50.6+VA GENERIC.zwr' PSNDF.zwr
$ydb_dist/mupip load -ignorechset PSNDF.zwr
$ydb_dist/mupip load -ignorechset vista-mini.zwr
$ydb_dist/mupip load -ignorechset /YDBGUI/wwwroot/test/globals/horologtest.zwr
$ydb_dist/mupip load -ignorechset /YDBGUI/wwwroot/test/globals/utf8test.zwr

