/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const {randomBytes} = require('crypto');
const https = require('https');
const fs = require('fs');
const path = require('path');
const {env} = require('process')
const {browserPorts} = require('./test_files/replication/helper.js')

const delay = (time) => {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
};

const getCssDisplay = async (element) => {
    return await page.$eval(element, function (elem) {
        return window.getComputedStyle(elem).getPropertyValue('display')
    });
};

const getCssColor = async (element) => {
    return await page.$eval(element, function (elem) {
        return window.getComputedStyle(elem).getPropertyValue('color')
    });
};

const getCssBackground = async (element) => {
    return await page.$eval(element, function (elem) {
        return window.getComputedStyle(elem).getPropertyValue('background-color')
    });
};

async function clickOnElement(elem, x = null, y = null) {
    const rect = await page.evaluate(el => {
        const {top, left, width, height} = el.getBoundingClientRect();
        return {top, left, width, height};
    }, elem);
    const _x = x !== null ? x : rect.width / 2;
    const _y = y !== null ? y : rect.height / 2;

    await page.mouse.click(rect.left + _x, rect.top + _y);
}

async function dblClickOnElement(elem, x = null, y = null) {
    const rect = await page.evaluate(el => {
        const {top, left, width, height} = el.getBoundingClientRect();
        return {top, left, width, height};
    }, elem);
    const _x = x !== null ? x : rect.width / 2;
    const _y = y !== null ? y : rect.height / 2;

    await page.mouse.click(rect.left + _x, rect.top + _y, {clickCount: 2});
}

const randomRegionName = () => {
    return 'R' + randomBytes(6).toString('hex').toUpperCase()
};

const waitForDialog = (dialogId, waitFor = 'open') => {
    // waitFor can be either 'open' or 'close'
    return new Promise(async function (resolve) {
        const hTimer = setInterval(async () => {
            let val;
            if (dialogId === 'modalInputbox') {
                val = await page.evaluate(() => $('#modalInputbox').css('animation-name'))
            } else {
                val = await page.$eval(dialogId, function (elem) {
                    return window.getComputedStyle(elem).getPropertyValue('animation-name')
                });
            }
            if (val === (waitFor === 'open' ? 'shown' : 'hidden')) {
                clearInterval(hTimer);
                resolve()
            }
        }, 50)
    });
};

const _REST = (path, token, port = 8089) => {
    return new Promise(async function (resolve, reject) {
        const options = {
            host: 'localhost',
            port: port,
            path: 'api/' + path
        }
        if (token !== undefined) {
            options.headers = {
                Authorization: "Bearer " + token
            }
        }

        https.get(options, res => {
            let data = '';

            res.on('data', chunk => {
                data += chunk;
            });

            res.on('end', () => {
                try {
                    resolve(JSON.parse(data))
                } catch (err) {
                    reject(err)
                }
            });

            res.on('error', err => {
                reject(err)
            })
        })
    });
};

const _RESTlogin = (userName, password) => {
    const fetch = require("node-fetch");
    const data = {
        username: userName,
        password: password
    }

    return new Promise(async function (resolve, reject) {
        fetch('https://localhost:8089/api/login',
            {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(data),
            }).then(async response => resolve(JSON.parse(await response.text()))).catch(err => reject(err))
    })
};

const _RESTlogout = (token) => {
    const fetch = require("node-fetch");
    const data = {token: token}

    return new Promise(async function (resolve, reject) {
        fetch('https://localhost:8089/api/logout',
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Bearer ' + token
                },
            }).then(async response => resolve(JSON.parse(await response.text()))).catch(err => reject(err))
    })
};

const _RESTpost = (path, data = {}) => {
    const fetch = require("node-fetch");

    return new Promise(async function (resolve, reject) {
        fetch('https://localhost:8089/api/' + path,
            {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(data),
            }).then(async response => resolve(JSON.parse(await response.text()))).catch(err => reject(err))
    })
};

const _RESTdelete = (path, data = {}) => {
    const fetch = require("node-fetch");

    return new Promise(async function (resolve, reject) {
        fetch('https://localhost:8089/api/' + path,
            {
                method: "DELETE",
                body: JSON.stringify(data),
            }).then(async response => resolve(JSON.parse(await response.text()))).catch(err => reject(err))
    })
};

const walkSync = (currentDirPath, filesList) => {
    fs.readdirSync(currentDirPath).forEach(function (name) {
        let filePath = path.join(currentDirPath, name);
        let stat = fs.statSync(filePath);

        if (stat.isFile()) {
            filesList.push(filePath)

        } else if (stat.isDirectory()) {
            walkSync(filePath, filesList);
        }
    });
};

const getServerMode = async () => {
    let port = 8089

    if (env.ydb_in_repl !== undefined) {
        port = browserPorts[env.ydb_in_repl.toUpperCase()]
    }

    const res = await _REST('server/mode', undefined, port);

    return res.serverMode
};

module.exports.delay = delay;
module.exports.getCssDisplay = getCssDisplay;
module.exports.getCssColor = getCssColor;
module.exports.getCssBackground = getCssBackground;
module.exports.randomRegionName = randomRegionName;
module.exports._REST = _REST;
module.exports._RESTpost = _RESTpost;
module.exports._RESTdelete = _RESTdelete;
module.exports._RESTlogin = _RESTlogin;
module.exports._RESTlogout = _RESTlogout;
module.exports.clickOnElement = clickOnElement;
module.exports.dblClickOnElement = dblClickOnElement;
module.exports.waitForDialog = waitForDialog;
module.exports.walkSync = walkSync;
module.exports.getServerMode = getServerMode;
