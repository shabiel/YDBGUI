/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("CLIENT: Globals > Tabs management", async () => {
    it("Test # 700: Create a new tab: verify default text", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=700`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        const cell = await page.$('#tab-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('New global');
    });

    it("Test # 705: Create 1 new tab: verify that you can switch tabs using the menu", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=705`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        // create a new tab
        await page.evaluate(() => app.ui.gViewer.addNew());

        // select the first one
        await page.evaluate(() => app.ui.tabs.select('-G-1'));

        await libs.delay(200);

        const isVisible = await libs.getCssDisplay('#tab-G-1Div');
        expect(isVisible === 'block').to.be.true;
    });

    /*
    it("Test # 706: Create a new tab with very long name: verify that caption gets truncated as \"settings - maxLengthCaption\" and popup is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=706`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        //type a new name
        await page.keyboard.type('TEST(123456789,123456789,123456789)');
        await page.keyboard.press('Enter');

        await libs.delay(2500);

        const cell = await page.$('#tab-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('^TEST(123456789,12');

        const title = await page.evaluate(() => document.getElementById('tab-G-1').title);
        expect(title).to.have.string('^TEST(123456789,123456')
    });

    it("Test # 707: Create a new tab with long name and change the \"settings - maxLengthCaption\" : verify that caption gets truncated as \"settings - maxLengthCaption\" and popup is correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=707`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.userSettings.globalViewer.options.tab.maxLengthCaption = 10);

        //type a new name
        await page.keyboard.type('TEST(123456789,123456789,123456789)');
        await page.keyboard.press('Enter');

        await libs.delay(2500);

        const cell = await page.$('#tab-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('^TEST(1234...   x');

        const title = await page.evaluate(() => document.getElementById('tab-G-1').title);
        expect(title).to.have.string('^TEST(123456789,123456789,123456789)')
    });

     */
});

describe("CLIENT: Globals > Menu management", async () => {
    /*
    it("Test # 750: Verify that the Last Used list gets populated with default # of items", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=750`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const numItems = await page.evaluate(() => app.userSettings.globalViewer.options.menuLastUsedItemsCount);

        const entries = (await page.evaluate(() => document.getElementById('menuDevelopmentGlobalsViewerRecentListEntries').childElementCount)) - 3;

        expect(parseInt(entries) === (parseInt(numItems) - 10)).to.be.true;
    });

    it("Test # 751: Verify that the Last Used list gets populated with different # of items && more is displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=751`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const entries = (await page.evaluate(() => document.getElementById('menuDevelopmentGlobalsViewerRecentListEntries').childElementCount)) / 2;
        const itemsInArray = await page.evaluate(() => app.userSettings.globalViewer.lastUsed.length);

        if (itemsInArray > entries) {
            const menuMore = await page.evaluate(() => document.getElementById('menuDevelopmentGlobalsViewerRecentListManagerMore').style.display);

            expect(menuMore === '').to.be.true

        } else {
            expect(entries === 0).to.be.false;

        }
    });

     */

    it("Test # 752: Verify that the More dialog is properly populated", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=752`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.gViewer.moreItems.show());

        await libs.delay(200);

        const entriesInList = await page.evaluate(() => document.getElementById('divGviewerMore').childElementCount);
        const itemsInArray = await page.evaluate(() => app.userSettings.globalViewer.lastUsed.length);

        expect(entriesInList === itemsInArray).to.be.true
    });

    it("Test # 754: Type a valid path and verify that gets added to the list", async () => {
        // create test global
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s ^TEST=0 h\'');

        await page.goto(`https://localhost:${MDevPort}//index.html?test=754`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // create a new tab
        await page.evaluate(() => app.ui.gViewer.addNew());

        await libs.delay(100);

        //type a new name
        await page.keyboard.type('TEST(500)');
        await page.keyboard.press('Enter');

        await libs.delay(500)

        const itemsInArray = await page.evaluate(() => app.userSettings.globalViewer.lastUsed);
        expect(itemsInArray[itemsInArray.length - 1]).to.have.string('TEST(500)');
    });

    it("Test # 755: Type an existing path and verify that it doesn't get added to the list", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=755`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // create a new tab
        await page.evaluate(() => app.ui.gViewer.addNew());

        await libs.delay(100);

        const itemsInArrayBefore = await page.evaluate(() => app.userSettings.globalViewer.lastUsed.length);

        //type a new name
        await page.keyboard.type('TEST(500)');
        await page.keyboard.press('Enter');

        await libs.delay(500);

        const itemsInArrayAfter = await page.evaluate(() => app.userSettings.globalViewer.lastUsed.length);
        expect(itemsInArrayBefore !== itemsInArrayAfter).to.be.false;
    });
});
