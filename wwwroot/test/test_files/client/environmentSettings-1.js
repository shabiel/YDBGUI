/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");
const {execSync} = require('child_process');
const fs = require('fs')
const {env} = require('process')

describe("CLIENT: Global Directory", async () => {
    it("Test # 2900: Create the all-good.gld file, select it, open it and validate it", async () => {
        // create a new test gld by copying first the original gld
        execSync('cp $ydb_gbldir /YDBGUI/wwwroot/test/test-gld/all-good.gld')
        // and then executing the gde file
        execSync('ydb_gbldir="/YDBGUI/wwwroot/test/test-gld/all-good.gld" yottadb -run GDE @/YDBGUI/wwwroot/test/test-gld/gld-all-good.gde');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/all-good.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        const msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('You can now press "Set" to apply the changes')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false
    })

    it("Test # 2901: Create the cwd.gld file, select it, open it and validate it", async () => {
        // create a new test gld by copying first the original gld
        execSync('cp $ydb_gbldir /YDBGUI/wwwroot/test/test-gld/cwd.gld')
        // and then executing the gde file
        execSync('ydb_gbldir="/YDBGUI/wwwroot/test/test-gld/cwd.gld" yottadb -run GDE @/YDBGUI/wwwroot/test/test-gld/gld-cwd.gde');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('The following path could not be resolved:yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsCwd must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsCwd').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the cwd to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsCwdInvalid').text())
        expect(msg).to.have.string('You need to set up the current working directory to proceed')

    })

    it("Test # 2902: Create the envvars.gld file, select it, open it and validate it", async () => {
        // create a new test gld by copying first the original gld
        execSync('cp $ydb_gbldir /YDBGUI/wwwroot/test/test-gld/envvars.gld')
        // and then executing the gde file
        execSync('ydb_gbldir="/YDBGUI/wwwroot/test/test-gld/envvars.gld" yottadb -run GDE @/YDBGUI/wwwroot/test/test-gld/gld-envvars.gde');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the env vars to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

    })

    it("Test # 2903: Create the cwd-envvars.gld file, select it, open it and validate it", async () => {
        // create a new test gld by copying first the original gld
        execSync('cp $ydb_gbldir /YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')
        // and then executing the gde file
        execSync('ydb_gbldir="/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld" yottadb -run GDE @/YDBGUI/wwwroot/test/test-gld/gld-cwd-envvars.gde');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd-envvars.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('$ydb_rel2')
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.true

        // and message should be: You need to set up the cwd to proceed
        msg = await page.evaluate(() => $('#valEnvSettingsEnvVarsInvalid').text())
        expect(msg).to.have.string('You need to set up the environment variables to proceed')

    })

    it("Test # 2904: Select bad.gld, open it and validate it", async () => {
        // create the file
        fs.writeFileSync('/YDBGUI/wwwroot/test/test-gld/bad.gld', 'zzzz');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/bad.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/bad.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(200)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/bad.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('The file is not a valid gld file')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // lblEnvSettingsEnvVars must NOT have is-invalid class
        status = await page.evaluate(() => $('#lblEnvSettingsEnvVars').hasClass('is-invalid'))
        expect(status).to.be.false
    })

    it("Test # 2905: Select all-good.gld, hit Set and verify, then open again, hit Reset and verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/all-good.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(500)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        const msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('You can now press "Set" to apply the changes')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // press Set
        btnClick = await page.$("#btnEnvSettingsSet");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        let regions = await page.evaluate(() => app.system.regions)
        let regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 3).to.be.true

        // verify text in alerts
        const file = await page.evaluate(() => $('#txtGldAltFilename').text())
        expect(file).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        const extra = await page.evaluate(() => $('#txtGldAltExtra').text())
        expect(extra).to.have.string('/YDBGUI/wwwroot/')

        // press Change / Reset
        btnClick = await page.$(".dash-gld-link");
        await btnClick.click();

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // press Reset
        btnClick = await page.$("#btnEnvSettingsReset");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        regions = await page.evaluate(() => app.system.regions)
        regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 4).to.be.true
    })

    it("Test # 2906: Select all-good.gld, change the cwd, hit Set and verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/all-good.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('You can now press "Set" to apply the changes')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');


        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/all-good.gld')

        // change cwd

        // press Set cwd to submit it
        btnClick = await page.$("#btnEnvSettingsCwd");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalCwdSelector');

        // set focus on path input field
        await page.evaluate(() => $('#lblCwdSelectorPath').focus())
        // and type it in
        await page.evaluate(() => $('#lblCwdSelectorPath').val(''))
        await page.keyboard.type('/YDBGUI');

        await libs.delay(100)

        // press Validate
        btnClick = await page.$("#btnCwdSelectorValidate");
        await btnClick.click();

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('The path has been validated...')

        // close msg box
        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'close');

        // close CWD selector
        btnClick = await page.$("#btnCwdSelectorOk");
        await btnClick.click();

        // wait for dialog to close
        await libs.waitForDialog('#modalCwdSelector', 'close');

        // verify that path has been set
        const cwd = await page.evaluate(() => $('#lblEnvSettingsCwd').val())
        expect(cwd).to.have.string('/YDBGUI')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('You can now press "Set" to apply the changes')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // press Set
        btnClick = await page.$("#btnEnvSettingsSet");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        let regions = await page.evaluate(() => app.system.regions)
        let regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 3).to.be.true

        // press Change / Reset
        btnClick = await page.$(".dash-gld-link");
        await btnClick.click();

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // press Reset
        btnClick = await page.$("#btnEnvSettingsReset");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        regions = await page.evaluate(() => app.system.regions)
        regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 4).to.be.true
    })

    it("Test # 2907: Select cwd.gld, verify, change the cwd to a bad one, verify again", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(500)

        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('The following path could not be resolved:yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // change cwd

        // press Set cwd to submit it
        btnClick = await page.$("#btnEnvSettingsCwd");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalCwdSelector');

        // set focus on path input field
        await page.evaluate(() => $('#lblCwdSelectorPath').focus())
        // and type it in
        await page.evaluate(() => $('#lblCwdSelectorPath').val(''))
        await page.keyboard.type('/YDBGUI');

        await libs.delay(100)

        // press Validate
        btnClick = await page.$("#btnCwdSelectorValidate");
        await btnClick.click();

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('The path has been validated...')

        // close msg box
        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'close');

        // close CWD selector
        btnClick = await page.$("#btnCwdSelectorOk");
        await btnClick.click();

        // wait for dialog to close
        await libs.waitForDialog('#modalCwdSelector', 'close');

        // verify that path has been set
        const cwd = await page.evaluate(() => $('#lblEnvSettingsCwd').val())
        expect(cwd).to.have.string('/YDBGUI')

        // it has invalid class
        const invalidClass = await page.evaluate(() => $('#lblEnvSettingsCwd').hasClass('is-invalid'))
        expect(invalidClass).to.be.true

        // and help text appears
        const invalidText = await page.evaluate(() => $('#valEnvSettingsCwdInvalid').text())
        expect(invalidText).to.have.string('You need to set up the current working')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false
    })

    it("Test # 2908: Select cwd.gld, verify, change the cwd to a good one, verify, hit Set and verify, open again, hit Reset and verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.envSettings.show())

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // display the gld file selector
        await page.evaluate(() => app.ui.gldFileSelect.show())

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalGldFileSelector');

        // select the newly created gld file
        await page.evaluate(() => app.ui.gldFileSelect.fileSelectedReturn('/YDBGUI/wwwroot/test/test-gld/cwd.gld'))

        // verify it is all good
        const setPath = await page.evaluate(() => $('#lblGldFilePath').val())
        expect(setPath).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd.gld')

        // press ok to submit it
        btnClick = await page.$("#btnGldFilePathOk");
        await btnClick.click();

        await libs.delay(500)
        // verify that path is displayed in the Env Settings dialog
        const caption = await page.evaluate(() => $('#lblEnvSettingsGld').val())
        expect(caption).to.have.string('/YDBGUI/wwwroot/test/test-gld/cwd.gld')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        let msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('The following path could not be resolved:yottadb.dat')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set and Reset are disabled
        let status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // change cwd

        // press Set cwd to submit it
        btnClick = await page.$("#btnEnvSettingsCwd");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalCwdSelector');

        // set focus on path input field
        await page.evaluate(() => $('#lblCwdSelectorPath').focus())
        // and type it in
        await page.evaluate(() => $('#lblCwdSelectorPath').val(''))
        await page.keyboard.type('/data/' + env.ydb_rel + '/g/')

        await libs.delay(100)

        // press Validate
        btnClick = await page.$("#btnCwdSelectorValidate");
        await btnClick.click();

        // wait for msgbox to appear
        await libs.waitForDialog('#modalMsgbox');

        // verify validation
        const validationText = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(validationText).to.have.string('The path has been validated...')

        // close msg box
        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'close');

        // close CWD selector
        btnClick = await page.$("#btnCwdSelectorOk");
        await btnClick.click();

        // wait for dialog to close
        await libs.waitForDialog('#modalCwdSelector', 'close');

        // verify that path has been set
        const cwd = await page.evaluate(() => $('#lblEnvSettingsCwd').val())
        expect(cwd).to.have.string('/data/' + env.ydb_rel + '/g/')

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
        msg = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(msg).to.have.string('You can now press "Set" to apply the changes')

        btnClick = await page.$("#btnMsgboxOk");
        await btnClick.click();

        // wait for msgbox to close
        await libs.waitForDialog('#modalMsgbox', 'hide');

        // ensure that Set button is enabled, Reset is disabled
        status = await page.evaluate(() => $('#btnEnvSettingsSet').prop('disabled'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#btnEnvSettingsReset').prop('disabled'))
        expect(status).to.be.false

        // press Set
        btnClick = await page.$("#btnEnvSettingsSet");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        let regions = await page.evaluate(() => app.system.regions)
        let regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 3).to.be.true

        // verify text in alerts
        const file = await page.evaluate(() => $('#txtGldAltFilename').text())
        expect(file).to.have.string('YDBGUI/wwwroot/test/test-gld/cwd.gld')

        const extra = await page.evaluate(() => $('#txtGldAltExtra').text())
        expect(extra).to.have.string('Working directory: /data/' + env.ydb_rel + '/g')


        // press Change / Reset
        btnClick = await page.$(".dash-gld-link");
        await btnClick.click();

        // wait for envSettings to be set by the async call
        await libs.waitForDialog('#modalEnvSettings');

        // press Reset
        btnClick = await page.$("#btnEnvSettingsReset");
        await btnClick.click();

        // wait for envSettings to be closed
        await libs.waitForDialog('#modalEnvSettings', 'close');

        await libs.delay(500)

        // count the regions
        regions = await page.evaluate(() => app.system.regions)
        regionsCount = 0

        for (const region in regions) {
            regionsCount++
        }
        expect(regionsCount === 4).to.be.true
    })
})

