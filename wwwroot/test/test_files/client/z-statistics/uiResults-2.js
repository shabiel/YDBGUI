/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");
const {exec} = require('child_process');

describe("Statistics: Stats: Results: Source properties", async () => {
    it("Test # 2675: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 1", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // open properties dialog
        btnClick = await page.$("#btnStatsToolbarResultsProperties");
        await btnClick.click();

        // and wait for dialog to appear
        await libs.waitForDialog('#modalStatsResultSourceProperties');

        // check values
        const type = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesType').text())
        expect(type).to.have.string('YGBL')

        const sampleRate = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSampleRate').text())
        expect(sampleRate).to.have.string('1 sec')

        const regions = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesRegions').text())
        expect(regions).to.have.string('DEFAULT')

        const processes = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesProcesses').text())
        expect(processes).to.have.string('All processes')

        const samples = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSamples').text())
        expect(samples).to.have.string('SET')

        await libs.delay(3000)
    })
/*
    it("Test # 2676: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 2", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 2nd source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_5fd15ec9-c8af-48e1-8692-389e0b0f9fc8"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // open properties dialog
        btnClick = await page.$("#btnStatsToolbarResultsProperties");
        await btnClick.click();

        // and wait for dialog to appear
        await libs.waitForDialog('#modalStatsResultSourceProperties');

        // check values
        const type = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesType').text())
        expect(type).to.have.string('YGBL')

        const sampleRate = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSampleRate').text())
        expect(sampleRate).to.have.string('2 secs')

        const regions = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesRegions').text())
        expect(regions).to.have.string('DEFAULT')

        const processes = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesProcesses').text())
        expect(processes).to.have.string('TOP 4 of KIL')

        const samples = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSamples').text())
        expect(samples).to.have.string('KIL')

        await libs.delay(3000)
    })

 */

    it("Test # 2677: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 3", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 3rd source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_3946988c-6ef6-494d-8e01-a83d09138bb9"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // open properties dialog
        btnClick = await page.$("#btnStatsToolbarResultsProperties");
        await btnClick.click();

        // and wait for dialog to appear
        await libs.waitForDialog('#modalStatsResultSourceProperties');

        // check values
        const type = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesType').text())
        expect(type).to.have.string('YGBL')

        const sampleRate = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSampleRate').text())
        expect(sampleRate).to.have.string('1 sec')

        const regions = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesRegions').text())
        expect(regions).to.have.string('DEFAULTYDBAIM')

        const processes = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesProcesses').text())
        expect(processes).to.have.string('All processes')

        const samples = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSamples').text())
        expect(samples).to.have.string('GET')

        await libs.delay(3000)
    })

    it("Test # 2678: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display properties for source 4", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 4th source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_47891c28-6b71-4b8c-bc69-671d9840aac6"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // open properties dialog
        btnClick = await page.$("#btnStatsToolbarResultsProperties");
        await btnClick.click();

        // and wait for dialog to appear
        await libs.waitForDialog('#modalStatsResultSourceProperties');

        // check values
        const type = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesType').text())
        expect(type).to.have.string('YGBL')

        const sampleRate = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSampleRate').text())
        expect(sampleRate).to.have.string('1 sec')

        const regions = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesRegions').text())
        expect(regions).to.have.string('DEFAULTYDBAIM')

        const processes = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesProcesses').text())
        expect(processes).to.have.string('TOP 5 of SET')

        const samples = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSamples').text())
        expect(samples).to.have.string('SET')

        await libs.delay(3000)
    })

    it("Test # 2679: Load test-1, run bgnd program, acquire 5 secs of samples, stop, display properties for source 1", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // open properties dialog
        btnClick = await page.$("#btnStatsToolbarResultsProperties");
        await btnClick.click();

        // and wait for dialog to appear
        await libs.waitForDialog('#modalStatsResultSourceProperties');

        // check values
        const type = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesType').text())
        expect(type).to.have.string('YGBL')

        const sampleRate = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSampleRate').text())
        expect(sampleRate).to.have.string('0.5 sec')

        const regions = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesRegions').text())
        expect(regions).to.have.string('DEFAULT')

        const processes = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesProcesses').text())
        expect(processes).to.have.string('All processes')

        const samples = await page.evaluate(() => $('#txtStatsResultsSourcePropertiesSamples').text())
        expect(samples).to.have.string('SET, KIL, GET, ORD, DRD, DWT, JFL, JFS, JBB, JFB, JFW')

        await libs.delay(3000)
    })
})

describe("Statistics: Stats: Results: Paging, page navigation", async () => {
    it("Test # 2680: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, check # of pages", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // check pages
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('1 of 3')

        await libs.delay(3000)
    })

    it("Test # 2681: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, check enabled status of buttons", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // check values
        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.true

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.true

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.false

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2682: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to page 2, check enabled status and pages caption", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to page 2
        btnClick = await page.$("#btnStatsResultsNext-0");
        await btnClick.click();

        // check values
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('2 of 3')

        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.false

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.false

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.false

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2683: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to page 3 (last), navigate back to 2, check enabled status and pages caption", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to last page
        btnClick = await page.$("#btnStatsResultsEnd-0");
        await btnClick.click();

        // navigate to prev
        btnClick = await page.$("#btnStatsResultsPrev-0");
        await btnClick.click();

        // check values
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('2 of 3')

        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.false

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.false

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.false

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2684: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to page 3 (last), navigate back to 1,, check enabled status and pages caption", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to last page
        btnClick = await page.$("#btnStatsResultsEnd-0");
        await btnClick.click();

        // navigate to prev twice
        btnClick = await page.$("#btnStatsResultsPrev-0");
        await btnClick.click();
        btnClick = await page.$("#btnStatsResultsPrev-0");
        await btnClick.click();

        // check values
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('1 of 3')

        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.true

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.true

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.false

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2685: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to last, check enabled status and pages caption", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to last page
        btnClick = await page.$("#btnStatsResultsEnd-0");
        await btnClick.click();

        // check values
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('3 of 3')

        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.false

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.false

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.true

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.true

        await libs.delay(3000)
    })

    it("Test # 2686: Load test-8, run bgnd program, acquire 13 secs of samples, stop, display result, navigate to last, then first, check enabled status and pages caption", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,16)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(13000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to last page, then first
        btnClick = await page.$("#btnStatsResultsEnd-0");
        await btnClick.click();
        btnClick = await page.$("#btnStatsResultsHome-0");
        await btnClick.click();

        // check values
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('1 of 3')

        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.true

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.true

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.false

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.false

        await libs.delay(3000)
    })

    it("Test # 2687: Load test-8, run bgnd program, acquire 3 secs of samples, stop, display result, check # of pages", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,6)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(3000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to last page, then first
        btnClick = await page.$("#btnStatsResultsEnd-0");
        await btnClick.click();
        btnClick = await page.$("#btnStatsResultsHome-0");
        await btnClick.click();

        // check values
        const caption = await page.evaluate(() => $('#lblStatsResultsLabel-0').text())
        expect(caption).to.have.string('1 of 1')

        await libs.delay(3000)
    })

    it("Test # 2688: Load test-8, run bgnd program, acquire 3 secs of samples, stop, display result, check enabled status of buttons", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,6)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(3000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // navigate to last page, then first
        btnClick = await page.$("#btnStatsResultsEnd-0");
        await btnClick.click();
        btnClick = await page.$("#btnStatsResultsHome-0");
        await btnClick.click();

        // check values
        const first = await page.evaluate(() => $('#btnStatsResultsHome-0').prop('disabled'))
        expect(first).to.be.true

        const prev = await page.evaluate(() => $('#btnStatsResultsPrev-0').prop('disabled'))
        expect(prev).to.be.true

        const next = await page.evaluate(() => $('#btnStatsResultsNext-0').prop('disabled'))
        expect(next).to.be.true

        const last = await page.evaluate(() => $('#btnStatsResultsEnd-0').prop('disabled'))
        expect(last).to.be.true

        await libs.delay(3000)
    })
})

describe("Statistics: Stats: Results: Zoom", async () => {
    it("Test # 2690: Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom in, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // check the font size before the change
        const fontSizeOld = await page.evaluate(() => $('#tblStatsResults-0 > tbody > tr:nth-child(1) > td').css('font-size'))

        // select the zoom in
        btnClick = await page.$("#btnStatsToolbarResultsZoomIn");
        await btnClick.click();

        const fontSizeNew = await page.evaluate(() => $('#tblStatsResults-0 > tbody > tr:nth-child(1) > td').css('font-size'))
        expect(fontSizeNew !== fontSizeOld).to.be.true
    })

    it("Test # 2691: Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom out, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // check the font size before the change
        const fontSizeOld = await page.evaluate(() => $('#tblStatsResults-0 > tbody > tr:nth-child(1) > td').css('font-size'))

        // select the zoom out
        btnClick = await page.$("#btnStatsToolbarResultsZoomOut");
        await btnClick.click();

        const fontSizeNew = await page.evaluate(() => $('#tblStatsResults-0 > tbody > tr:nth-child(1) > td').css('font-size'))
        expect(fontSizeNew !== fontSizeOld).to.be.true
    })

    it("Test # 2692: Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom in 20 times, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the zoom in
        for (let ix = 0; ix < 20; ix++) {
            btnClick = await page.$("#btnStatsToolbarResultsZoomIn");
            await btnClick.click();
        }

        const fontSizeNew = await page.evaluate(() => $('#tblStatsResults-0 > tbody > tr:nth-child(1) > td').css('font-size'))
        expect(fontSizeNew).to.have.string('21px')
    })

    it("Test # 2693: Load test-1, run bgnd program, acquire 5 secs of samples, stop, display result, zoom out 20 times, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the zoom out
        for (let ix = 0; ix < 20; ix++) {
            btnClick = await page.$("#btnStatsToolbarResultsZoomOut");
            await btnClick.click();
        }

        const fontSizeNew = await page.evaluate(() => $('#tblStatsResults-0 > tbody > tr:nth-child(1) > td').css('font-size'))
        expect(fontSizeNew).to.have.string('7px')
    })
})

describe("Statistics: Stats: Results: Export dialog", async () => {
    it("Test # 2720: Load test-8, run bgnd program, acquire 5 secs of samples, stop, display result, click export, dialog should appear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the export
        btnClick = await page.$("#btnStatsToolbarResultsExport");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsResultExport');
    })

    it("Test # 2721: Load test-8, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select header, no region, process and sample/data should appear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(8))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(8))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the export
        btnClick = await page.$("#btnStatsToolbarResultsExport");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsResultExport');

        const checked = await page.evaluate(() => $('#swcStatsResultsExportHeader').prop('checked'))
        expect(checked).to.be.true
    })

    it("Test # 2722: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 2, header should be selected, no region should appear, process and sample/data should appear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 2nd source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_5fd15ec9-c8af-48e1-8692-389e0b0f9fc8"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // select the export
        btnClick = await page.$("#btnStatsToolbarResultsExport");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsResultExport');

        const checked = await page.evaluate(() => $('#swcStatsResultsExportHeader').prop('checked'))
        expect(checked).to.be.true

        let display = await page.evaluate(() => $('#divStatsResultsExportHeaderRegions').css('display'))
        expect(display).to.have.string('none')

        display = await page.evaluate(() => $('#divStatsResultsExportHeaderProcesses').css('display'))
        expect(display).to.have.string('flex')

        display = await page.evaluate(() => $('#divStatsResultsExportHeaderSampleValue').css('display'))
        expect(display).to.have.string('flex')
    })

    it("Test # 2723: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 3, header should be selected, no process should appear, regions and sample/data should appear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 3rd source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_3946988c-6ef6-494d-8e01-a83d09138bb9"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // select the export
        btnClick = await page.$("#btnStatsToolbarResultsExport");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsResultExport');

        const checked = await page.evaluate(() => $('#swcStatsResultsExportHeader').prop('checked'))
        expect(checked).to.be.true

        let display = await page.evaluate(() => $('#divStatsResultsExportHeaderRegions').css('display'))
        expect(display).to.have.string('flex')

        display = await page.evaluate(() => $('#divStatsResultsExportHeaderProcesses').css('display'))
        expect(display).to.have.string('none')

        display = await page.evaluate(() => $('#divStatsResultsExportHeaderSampleValue').css('display'))
        expect(display).to.have.string('flex')
    })

    it("Test # 2724: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 4, header should be selected, regions, process and sample/data should appear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 4th source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_47891c28-6b71-4b8c-bc69-671d9840aac6"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // select the export
        btnClick = await page.$("#btnStatsToolbarResultsExport");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsResultExport');

        const checked = await page.evaluate(() => $('#swcStatsResultsExportHeader').prop('checked'))
        expect(checked).to.be.true

        let display = await page.evaluate(() => $('#divStatsResultsExportHeaderRegions').css('display'))
        expect(display).to.have.string('flex')

        display = await page.evaluate(() => $('#divStatsResultsExportHeaderProcesses').css('display'))
        expect(display).to.have.string('flex')

        display = await page.evaluate(() => $('#divStatsResultsExportHeaderSampleValue').css('display'))
        expect(display).to.have.string('flex')
    })

    it("Test # 2725: Load test-6, run bgnd program, acquire 5 secs of samples, stop, display result, click export, select source 4, deselect header, regions, process and sample/data should disappear", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // run the background random generator program
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD "do statsGen^%ydbguiStatsGenerator(100,400,7)"');

        // start recording
        let btnClick = await page.$("#btnStatsRecord");
        await btnClick.click();

        // wait for samples to be accumulated
        await libs.delay(5000)

        // stop recording
        btnClick = await page.$("#btnStatsStop");
        await btnClick.click();

        await libs.delay(200)

        // select the tab
        btnClick = await page.$("#tabStatsResult");
        await btnClick.click();

        await libs.delay(500)

        // select the 4th source
        await page.evaluate(() => $('#selStatsResultsSource option[value="optStatsResultSource_47891c28-6b71-4b8c-bc69-671d9840aac6"]').attr('selected', 'selected'))
        btnClick = await page.$("#selStatsResultsSource");
        await btnClick.click();

        await libs.delay(500)

        // select the export
        btnClick = await page.$("#btnStatsToolbarResultsExport");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsResultExport');

        // deselect header
        await page.evaluate(() => $('#swcStatsResultsExportHeader').prop('checked', false))
        await page.evaluate(() => app.ui.stats.results.export.headerClicked())

        let display = await page.evaluate(() => $('#divStatsResultsExportHeaderFields').css('display'))
        expect(display).to.have.string('none')
    })
})
