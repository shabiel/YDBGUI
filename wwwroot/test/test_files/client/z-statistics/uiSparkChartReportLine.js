/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Stats: Sparkchart: Report Line", async () => {
    it("Test # 2350: Create report line, open directly the properties, should display input box, select no, should close", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create report line view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox')

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('There are no mappings specified for this view. Do you want to create new mappings')
    })

    it("Test # 2351: Create report line, open directly the properties, should display input box, select yes, should open the mapping", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create report line view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox')

        const val = await page.evaluate(() => $('#txtInputboxText').text())
        expect(val).to.have.string('There are no mappings specified for this view. Do you want to create new mappings')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsSparkChartMapping')
    })

    it("Test # 2352: Ensure correct information is displayed in the header", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        const regions = await page.evaluate(() => $('#lblStatsSparkChartReportLineRegions').text())
        expect(regions).to.have.string('DEFAULT')

        const processes = await page.evaluate(() => $('#lblStatsSparkChartReportLineProcesses').text())
        expect(processes).to.have.string('Aggregate')

        const samples = await page.evaluate(() => $('#lblStatsSparkChartReportLineSamples').text())
        expect(samples).to.have.string('JFL ( abs,avg,delta,min,max ), JFS ( abs,avg,delta,min,max ), JBB ( abs,avg,delta,min,max ), JFB ( abs,avg,delta,min,max ), JFW ( abs,avg,delta,min,max )')
    })

    it("Test # 2353: Ensure preview matches the orientation", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        const orientationV = await page.evaluate(() => $('#optStatsSparkChartReportLineV').is(':checked'))
        expect(orientationV).to.be.true

        const sample = await page.evaluate(() => $('#divStatsSparkChartReportLinePreview table:nth-child(2) tbody tr:nth-child(2) td:first').text())
        expect(sample).to.have.string('JFL')
    })

    it("Test # 2354: Change orientation: ensure preview matches orientation", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.hClicked())
        await page.evaluate(() => $('#optStatsSparkChartReportLineH').prop('checked', true))
        const orientationH = await page.evaluate(() => $('#optStatsSparkChartReportLineH').is(':checked'))
        expect(orientationH).to.be.true

        const sample = await page.evaluate(() => $('#divStatsSparkChartReportLinePreview table:nth-child(2) tbody tr:nth-child(2) td').text())
        expect(sample).to.have.string('abs')
    })

    it("Test # 2355: Change orientation, press cancel, open again, should have NOT saved it", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.hClicked())
        await page.evaluate(() => $('#optStatsSparkChartReportLineH').prop('checked', true))
        const orientationH = await page.evaluate(() => $('#optStatsSparkChartReportLineH').is(':checked'))
        expect(orientationH).to.be.true

        const sample = await page.evaluate(() => $('#divStatsSparkChartReportLinePreview table:nth-child(2) tbody tr:nth-child(2) td').text())
        expect(sample).to.have.string('abs')

        elem = await page.$('#btnStatsSparkChartReportLineCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine', 'close')

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        const orientationV = await page.evaluate(() => $('#optStatsSparkChartReportLineV').is(':checked'))
        expect(orientationV).to.be.true
    })

    it("Test # 2356: Change orientation, press ok, open again, should have saved it", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.hClicked())
        await page.evaluate(() => $('#optStatsSparkChartReportLineH').prop('checked', true))
        const orientationH = await page.evaluate(() => $('#optStatsSparkChartReportLineH').is(':checked'))
        expect(orientationH).to.be.true

        const sample = await page.evaluate(() => $('#divStatsSparkChartReportLinePreview table:nth-child(2) tbody tr:nth-child(2) td').text())
        expect(sample).to.have.string('abs')

        elem = await page.$('#btnStatsSparkChartReportLineOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine', 'close')

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        const orientationV = await page.evaluate(() => $('#optStatsSparkChartReportLineV').is(':checked'))
        expect(orientationV).to.be.false
    })

    it("Test # 2357: Click on top left ... button in the preview, it should display the highlights dialog", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')
    })
})

describe("Statistics: Stats: Sparkchart: Report Line: Highlights", async () => {
    it("Test # 2360:select a aggregate Report Line, open highlights, max of processes should be disabled", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true
    })

    it("Test # 2361:select a aggregate Report Line, open highlights, change the option into Range, no input data, hit ok, should display msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox')
    })

    it("Test # 2362: select a aggregate Report Line, open highlights, change the option into Range, input only greater than, hit ok, should display msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeRed').val(5))
        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox')
    })

    it("Test # 2363: select a aggregate Report Line, open highlights, change the option into Range, click cancel, verify it didn't change in the Report Line", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartHighlightsCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const checked = await page.evaluate(() => $('#optStatsSparkChartHighlightsRegular').prop('checked'))
        expect(checked).to.be.true
    })

    it("Test # 2364: select a aggregate Report Line, open highlights, change the option into Range, click ok, verify it did change in the Report Line", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeRed').val(5))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeYellow').val(3))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const checked = await page.evaluate(() => $('#optStatsSparkChartHighlightsRegular').prop('checked'))
        expect(checked).to.be.false
    })

    it("Test # 2365: select a aggregate Report Line, open highlights, change the option into Range, click ok, verify verify that Report Line preview changed the cell caption", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeRed').val(5))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeYellow').val(3))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        const cell = await page.evaluate(() => $('#divStatsSparkChartReportLinePreview table:nth-child(2) tbody tr:nth-child(2) td:nth-child(2) ').text())
        expect(cell).to.have.string('range')
    })

    it("Test # 2366: select a aggregate Report Line, open highlights, change the option into Range, enter first value smaller than second, expect msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeRed').val(5))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeYellow').val(7))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox')
    })

    it("Test # 2367: select a aggregate Report Line, open highlights, change the option into Range, enter first value equals to second, expect NO msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeRed').val(5))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeYellow').val(5))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')
    })

    it("Test # 2368: select a aggregate Report Line, open highlights, change the option into Range, enter first value and 0 as second value, expect NO msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(4) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.true

        await page.evaluate(() => $('#optStatsSparkChartHighlightsRange').prop('checked', true))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeRed').val(5))

        await page.evaluate(() => $('#inpStatsSparkChartHighlightsRangeYellow').val(0))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('f780cb81-359c-4c8f-8e54-9623965326b4', 'JFL', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')
    })

    it("Test # 2371: select a max Report Line, open highlights, verify that Max of processes is ENABLED", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_ca29d215-8680-435f-8d10-af4b31f4f139 > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('15a5c669-352c-43ca-89f8-cf84b3a7b6c5', 'SET', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const disabled = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('disabled'))
        expect(disabled).to.be.false
    })

    it("Test # 2372: select a max Report Line, open highlights, select Max of processes, hit Cancel, verify it didn't get stored", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_ca29d215-8680-435f-8d10-af4b31f4f139 > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('15a5c669-352c-43ca-89f8-cf84b3a7b6c5', 'SET', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartHighlightsCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('15a5c669-352c-43ca-89f8-cf84b3a7b6c5', 'SET', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const checked = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('checked'))
        expect(checked).to.be.false
    })

    it("Test # 2373: select a max Report Line, open highlights, select Max of processes, hit Ok, verify it did get stored", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_ca29d215-8680-435f-8d10-af4b31f4f139 > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('15a5c669-352c-43ca-89f8-cf84b3a7b6c5', 'SET', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartHighlightsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartHighlights', 'close')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.highlighterClicked('15a5c669-352c-43ca-89f8-cf84b3a7b6c5', 'SET', 'abs'))

        await libs.waitForDialog('#modalStatsSparkChartHighlights')

        const checked = await page.evaluate(() => $('#optStatsSparkChartHighlightsMax').prop('checked'))
        expect(checked).to.be.true
    })
})

describe("Statistics: Stats: Sparkchart: Report Line: properties", async () => {
    it("Test # 2380: Click on properties, verify Preferences opens with new title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_ca29d215-8680-435f-8d10-af4b31f4f139 > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        elem = await page.$('#btnStatsSparkChartReportLineSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Table settings')
    })

    it("Test # 2381: Click on Reset properties, verify msgbox is displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_ca29d215-8680-435f-8d10-af4b31f4f139 > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.resetSettingsClicked())

        await libs.waitForDialog('modalInputbox')
    })

    it("Test # 2382: Click on Save properties as default: verifiy msgbox is displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(3))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(3))

        await utils.openSparkchart()

        // click on properties
        elem = await page.$('#options_ca29d215-8680-435f-8d10-af4b31f4f139 > tbody > tr:nth-child(2) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartReportLine')

        await page.evaluate(() => app.ui.stats.sparkChart.reportLine.saveSettingsClicked())

        await libs.waitForDialog('modalInputbox')
    })
})
