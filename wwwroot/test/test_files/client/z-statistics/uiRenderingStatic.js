/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Stats: Rendering: Static: Layout", async () => {
    it("Test # 2600: Load report 6, verify 1st view Header and fields (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        const header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:first > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the samples
        const sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('SET')

        // check the samples types
        const sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleStat1).to.have.string('abs')

        const sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleStat2).to.have.string('Δ')

        const sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleStat3).to.have.string('Δ avg')
    })

    it("Test # 2601: Load report 6, verify 1st view Header and fields (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        const header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(3) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the samples types
        const sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('Δ')

        const sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample2).to.have.string('Δ min')

        const sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample3).to.have.string('Δ max')

        // check the samples
        const sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleStat1).to.have.string('SET')
    })

    it("Test # 2602: Load report 6, verify 2nd view Header and fields (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        const header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(5) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')


        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[0]')

        // check the samples
        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('KIL')

        // check the samples types
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleStat1).to.have.string('Δ')

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleStat2).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[1]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('KIL')

        // check the samples types
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleStat1).to.have.string('Δ')

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleStat2).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[2]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('KIL')

        // check the samples types
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleStat1).to.have.string('Δ')

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleStat2).to.have.string('Δ avg')


        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[3]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('KIL')

        // check the samples types
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleStat1).to.have.string('Δ')

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleStat2).to.have.string('Δ avg')
    })

    it("Test # 2603: Load report 6, verify 2nd view Header and fields (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        const header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(10) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')


        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[0]')

        // check the samples types
        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        let sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ')

        let sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample4).to.have.string('Δ min')

        let sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample5).to.have.string('Δ max')

        let sample6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample6).to.have.string('Δ avg')

        // check the samples
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('KIL')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[1]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ')

        sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample4).to.have.string('Δ min')

        sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample5).to.have.string('Δ max')

        sample6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample6).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('KIL')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[2]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ')

        sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample4).to.have.string('Δ min')

        sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample5).to.have.string('Δ max')

        sample6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample6).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('KIL')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[3]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ')

        sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample4).to.have.string('Δ min')

        sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample5).to.have.string('Δ max')

        sample6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample6).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('KIL')
    })

    it("Test # 2604: Load report 6, verify 3rd view Header and fields (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        let header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(15) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the samples
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('GET')

        // check the samples types
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        let sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ min')

        let sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(17) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('YDBAIM')

        // check the samples
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('GET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ min')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')
    })

    it("Test # 2605: Load report 6, verify 3rd view Header and fields (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        let header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(19) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the samples types
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('abs')

        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample2).to.have.string('Δ')

        let sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample3).to.have.string('Δ min')

        let sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample4).to.have.string('Δ max')

        let sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample5).to.have.string('Δ avg')

        // check the samples
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('GET')


        header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(21) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('YDBAIM')

        // check the samples types
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('abs')

        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample2).to.have.string('Δ')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample3).to.have.string('Δ min')

        sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample4).to.have.string('Δ max')

        sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample5).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('GET')
    })

    it("Test # 2606: Load report 6, verify 4th view Header and fields (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        let header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(23) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[0]')

        // check the samples
        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        let sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        let sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[1]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[2]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[3]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[4]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')



        header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(29) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('YDBAIM')

        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[0]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[1]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[2]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[3]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[4]')

        // check the samples
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('SET')

        // check the samples types
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')
    })

    it("Test # 2607: Load report 6, verify 4th view Header and fields (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        let header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(35) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the processes
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[0]')

        // check the samples types
        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        let sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[1]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[2]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[3]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[4]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')



        header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(41) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('YDBAIM')

        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[0]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[1]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[2]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[3]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')


        // check the processes
        sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(1) > td:nth-child(1)').text())
        expect(sample1).to.have.string('P[4]')

        // check the samples types
        sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample2).to.have.string('abs')

        sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample3).to.have.string('Δ avg')

        // check the samples
        sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')
    })

    it("Test # 2608: Load report 6, verify 5th view Header and fields (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        let header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(47) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the samples
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('SET')

        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample2).to.have.string('KIL')

        let sample3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(4)').text())
        expect(sample3).to.have.string('GET')

        let sample4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(5)').text())
        expect(sample4).to.have.string('DTA')

        let sample5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(6)').text())
        expect(sample5).to.have.string('ORD')

        let sample6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(7)').text())
        expect(sample6).to.have.string('ZPR')

        let sample7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(8)').text())
        expect(sample7).to.have.string('QRY')

        let sample8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(9)').text())
        expect(sample8).to.have.string('LKS')

        let sample9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(10)').text())
        expect(sample9).to.have.string('LKF')

        let sample10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(11)').text())
        expect(sample10).to.have.string('DRD')

        let sample11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(1) > td:nth-child(12)').text())
        expect(sample11).to.have.string('DWT')

        // check the samples types
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('abs')

        let sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('Δ')

        let sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('Δ avg')
    })

    it("Test # 2609: Load report 6, verify 5th view Header and fields (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // check the header
        let header = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(49) > tbody > tr:nth-child(2) > td').text())
        expect(header).to.have.string('DEFAULT')

        // check the samples types
        let sample1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(1) > td:nth-child(2)').text())
        expect(sample1).to.have.string('Δ min')

        let sample2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(1) > td:nth-child(3)').text())
        expect(sample2).to.have.string('Δ max')

        // check the samples
        let sampleMap1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(1)').text())
        expect(sampleMap1).to.have.string('SET')

        let sampleMap2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(1)').text())
        expect(sampleMap2).to.have.string('KIL')

        let sampleMap3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(1)').text())
        expect(sampleMap3).to.have.string('GET')

        let sampleMap4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(1)').text())
        expect(sampleMap4).to.have.string('DTA')

        let sampleMap5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(1)').text())
        expect(sampleMap5).to.have.string('ORD')

        let sampleMap6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(1)').text())
        expect(sampleMap6).to.have.string('ZPR')

        let sampleMap7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(1)').text())
        expect(sampleMap7).to.have.string('QRY')

        let sampleMap8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(1)').text())
        expect(sampleMap8).to.have.string('LKS')

        let sampleMap9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(1)').text())
        expect(sampleMap9).to.have.string('LKF')

        let sampleMap10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(1)').text())
        expect(sampleMap10).to.have.string('DRD')

        let sampleMap11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(1)').text())
        expect(sampleMap11).to.have.string('DWT')
    })
})

describe("Statistics: Stats: Rendering: Static: Data", async () => {
    it("Test # 2610: Load report 6, verify 1st view Data (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        const sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        const sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        const sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        const sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        const sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        const sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true
    })

    it("Test # 2611: Load report 6, verify 1st view Data (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        let sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true
    })

    it("Test # 2612: Load report 6, verify 2nd view Data (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(6) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        // verify that cell has 0 and has a GUID as id
        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(9) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true
    })

    it("Test # 2613: Load report 6, verify 2nd view Data (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        let sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        let sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        let sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        let sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        let sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(13) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(14) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true
    })

    it("Test # 2614: Load report 6, verify 3rd view Data (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        let sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(16) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(18) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true
    })

    it("Test # 2615: Load report 6, verify 3rd view Data (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        let sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        let sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        let sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        let sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        let sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(22) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true
    })

    it("Test # 2616: Load report 6, verify 4th view Data (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        let sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(24) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(25) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(26) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(27) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(28) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(30) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(31) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(32) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(33) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(34) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true
    })

    it("Test # 2617: Load report 6, verify 4th view Data (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(36) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(37) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(38) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(39) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(40) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(42) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(43) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(44) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(45) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(46) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true
    })

    it("Test # 2618: Load report 6, verify 5th view Data (H)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        let sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        let sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        let sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        let sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        let sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        let sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true

        let sampleStat6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(7)').text())
        expect(sampleStat6).to.have.string('0')
        let sampleStatId6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(7) > span').attr('id'))
        expect(sampleStatId6.split('-').length === 5).to.be.true

        let sampleStat7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(8)').text())
        expect(sampleStat7).to.have.string('0')
        let sampleStatId7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(8) > span').attr('id'))
        expect(sampleStatId7.split('-').length === 5).to.be.true

        let sampleStat8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(9)').text())
        expect(sampleStat8).to.have.string('0')
        let sampleStatId8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(9) > span').attr('id'))
        expect(sampleStatId8.split('-').length === 5).to.be.true

        let sampleStat9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(10)').text())
        expect(sampleStat9).to.have.string('0')
        let sampleStatId9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(10) > span').attr('id'))
        expect(sampleStatId9.split('-').length === 5).to.be.true

        let sampleStat10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(11)').text())
        expect(sampleStat10).to.have.string('0')
        let sampleStatId10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(11) > span').attr('id'))
        expect(sampleStatId10.split('-').length === 5).to.be.true

        let sampleStat11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(12)').text())
        expect(sampleStat11).to.have.string('0')
        let sampleStatId11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(2) > td:nth-child(12) > span').attr('id'))
        expect(sampleStatId11.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true

        sampleStat6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(7)').text())
        expect(sampleStat6).to.have.string('0')
        sampleStatId6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(7) > span').attr('id'))
        expect(sampleStatId6.split('-').length === 5).to.be.true

        sampleStat7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(8)').text())
        expect(sampleStat7).to.have.string('0')
        sampleStatId7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(8) > span').attr('id'))
        expect(sampleStatId7.split('-').length === 5).to.be.true

        sampleStat8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(9)').text())
        expect(sampleStat8).to.have.string('0')
        sampleStatId8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(9) > span').attr('id'))
        expect(sampleStatId8.split('-').length === 5).to.be.true

        sampleStat9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(10)').text())
        expect(sampleStat9).to.have.string('0')
        sampleStatId9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(10) > span').attr('id'))
        expect(sampleStatId9.split('-').length === 5).to.be.true

        sampleStat10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(11)').text())
        expect(sampleStat10).to.have.string('0')
        sampleStatId10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(11) > span').attr('id'))
        expect(sampleStatId10.split('-').length === 5).to.be.true

        sampleStat11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(12)').text())
        expect(sampleStat11).to.have.string('0')
        sampleStatId11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(3) > td:nth-child(12) > span').attr('id'))
        expect(sampleStatId11.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true

        sampleStat3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(4)').text())
        expect(sampleStat3).to.have.string('0')
        sampleStatId3 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(4) > span').attr('id'))
        expect(sampleStatId3.split('-').length === 5).to.be.true

        sampleStat4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(5)').text())
        expect(sampleStat4).to.have.string('0')
        sampleStatId4 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(5) > span').attr('id'))
        expect(sampleStatId4.split('-').length === 5).to.be.true

        sampleStat5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(6)').text())
        expect(sampleStat5).to.have.string('0')
        sampleStatId5 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(6) > span').attr('id'))
        expect(sampleStatId5.split('-').length === 5).to.be.true

        sampleStat6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(7)').text())
        expect(sampleStat6).to.have.string('0')
        sampleStatId6 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(7) > span').attr('id'))
        expect(sampleStatId6.split('-').length === 5).to.be.true

        sampleStat7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(8)').text())
        expect(sampleStat7).to.have.string('0')
        sampleStatId7 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(8) > span').attr('id'))
        expect(sampleStatId7.split('-').length === 5).to.be.true

        sampleStat8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(9)').text())
        expect(sampleStat8).to.have.string('0')
        sampleStatId8 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(9) > span').attr('id'))
        expect(sampleStatId8.split('-').length === 5).to.be.true

        sampleStat9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(10)').text())
        expect(sampleStat9).to.have.string('0')
        sampleStatId9 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(10) > span').attr('id'))
        expect(sampleStatId9.split('-').length === 5).to.be.true

        sampleStat10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(11)').text())
        expect(sampleStat10).to.have.string('0')
        sampleStatId10 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(11) > span').attr('id'))
        expect(sampleStatId10.split('-').length === 5).to.be.true

        sampleStat11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(12)').text())
        expect(sampleStat11).to.have.string('0')
        sampleStatId11 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(48) > tbody > tr:nth-child(4) > td:nth-child(12) > span').attr('id'))
        expect(sampleStatId11.split('-').length === 5).to.be.true
    })

    it("Test # 2619: Load report 6, verify 5th view Data (V)", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(6))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(6))

        // verify that cell has 0 and has a GUID as id
        let sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        let sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        let sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        let sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(2) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(3) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(4) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(5) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(6) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(7) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        // verify that cell has 0 and has a GUID as id
        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(8) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(9) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(10) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(11) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true


        sampleStat1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(2)').text())
        expect(sampleStat1).to.have.string('0')
        sampleStatId1 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(2) > span').attr('id'))
        expect(sampleStatId1.split('-').length === 5).to.be.true

        sampleStat2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(3)').text())
        expect(sampleStat2).to.have.string('0')
        sampleStatId2 = await page.evaluate(() => $('#divStatsSparkchartReport:first > table:nth-child(50) > tbody > tr:nth-child(12) > td:nth-child(3) > span').attr('id'))
        expect(sampleStatId2.split('-').length === 5).to.be.true
    })
})

describe("Statistics: Stats: Rendering: Static: Graph", async () => {
    it("Test # 2630: Single graph", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // select the mapping button
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        // creates a valid mapping
        await utils.createMapping()

        // save the changes in spark
        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close')

        // graph container should be visible
        let display = await page.evaluate(() => $('#divStatsSparkChartGraphsContainer').css('display'))
        expect(display).to.have.string('flex')

        // second graph container should not be visible
        display = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('display'))
        expect(display).to.have.string('none')
    })

    it("Test # 2631: Double graph V orientation", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // select the mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // save the changes in spark
        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close')

        // ensure both graphs are visible
        let display = await page.evaluate(() => $('#divStatsSparkChartGraphsContainer').css('display'))
        expect(display).to.have.string('flex')

        display = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('display'))
        expect(display).to.have.string('block')

        // ensure orientation is V
        let width = await page.evaluate(() => $('#divStatsSparkChartGraph1').css('width'))
        expect(width).to.have.string('491px')

        width = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('width'))
        expect(width).to.have.string('491px')

        let height = await page.evaluate(() => $('#divStatsSparkChartGraph1').css('height'))
        expect(height).to.have.string('288px')

        height = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('height'))
        expect(height).to.have.string('288px')
    })

    it("Test # 2632: Double graph H orientation", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // select the mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // select the mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // change the graph settings to H
        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').prop('checked', true))
        let checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').is(':checked'))
        expect(checked).to.be.true

        elem = await page.$('#btnStatsSparkChartGraphSettingsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings', 'close');

        // save the changes in spark
        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close')

        let display = await page.evaluate(() => $('#divStatsSparkChartGraphsContainer').css('display'))
        expect(display).to.have.string('block')

        display = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('display'))
        expect(display).to.have.string('block')

        // ensure orientation is H
        let width = await page.evaluate(() => $('#divStatsSparkChartGraph1').css('width'))
        expect(width).to.have.string('982px')

        width = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('width'))
        expect(width).to.have.string('982px')

        let height = await page.evaluate(() => $('#divStatsSparkChartGraph1').css('height'))
        expect(height).to.have.string('144px')

        height = await page.evaluate(() => $('#divStatsSparkChartGraph2').css('height'))
        expect(height).to.have.string('144px')
    })

    it("Test # 2633: No graph", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // graph container should not be visible
        let display = await page.evaluate(() => $('#divStatsSparkChartGraphsContainer').css('display'))
        expect(display).to.have.string('none')
    })
})
