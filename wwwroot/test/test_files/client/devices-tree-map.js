/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("CLIENT: Devices Tree map", async () => {
    it("Test # 2930: Open the dialog, verify that the space object gets properly populated", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=190`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashStorageView0");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceInfo');

        // open the tree map
        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        const obj = await page.evaluate(() => app.ui.deviceInfo.treeMap.space)
        expect(obj.databases.list.length > 0).to.be.true
        expect(obj.journals.list.length > 0).to.be.true
        expect(obj.free > 0).to.be.true
    })

    it("Test # 2931: Open the dialog, click on Databases, By Regions should become disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=190`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashStorageView0");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceInfo');

        // open the tree map
        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        // click on dbs
        btnClick = await page.$("#checkDeviceTreeMapDb");
        await btnClick.click();

        const disabled = await page.evaluate(() => $('#checkDeviceTreeMapDbDetails').prop('disabled'))
        expect(disabled).to.be.true
    })

    it("Test # 2932: Open the dialog, click on Journals, By Regions should become disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=190`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashStorageView0");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceInfo');

        // open the tree map
        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        // click on journals
        btnClick = await page.$("#checkDeviceTreeMapJournals");
        await btnClick.click();

        const disabled = await page.evaluate(() => $('#checkDeviceTreeMapJournalsDetails').prop('disabled'))
        expect(disabled).to.be.true
    })

    it("Test # 2933: Open the dialog, click on Databases / By Regions, then 'Databases', By regions should become unchecked", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=190`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashStorageView0");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceInfo');

        // open the tree map
        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        // click on db details
        btnClick = await page.$("#checkDeviceTreeMapDbDetails");
        await btnClick.click();

        await libs.delay(250)

        // click on db
        btnClick = await page.$("#checkDeviceTreeMapDb");
        await btnClick.click();

        const checked = await page.evaluate(() => $('#checkDeviceTreeMapDbDetails').is(':checked'))
        expect(checked).to.be.false
    })

    it("Test # 2934: Open the dialog, click on Journals / By Regions, then 'Journals', By regions should become unchecked", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=190`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashStorageView0");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceInfo');

        // open the tree map
        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        // open the journal details
        btnClick = await page.$("#checkDeviceTreeMapJournalsDetails");
        await btnClick.click();

        await libs.delay(250)

        // open the journal
        btnClick = await page.$("#checkDeviceTreeMapJournals");
        await btnClick.click();

        const checked = await page.evaluate(() => $('#checkDeviceTreeMapJournalsDetails').is(':checked'))
        expect(checked).to.be.false
    })

    it("Test # 2935: Open the dialog, select all checkboxes, close dialog, open it again, only Database and Journals should be selected", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=190`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#btnDashStorageView0");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceInfo');

        // open the tree map
        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        // click on other
        btnClick = await page.$("#checkDeviceTreeMapOther");
        await btnClick.click();

        // click on free space
        btnClick = await page.$("#checkDeviceTreeMapFreeSpace");
        await btnClick.click();

        // click on close
        btnClick = await page.$("#btnDeviceTreeMapCancel");
        await btnClick.click();

        // wait for dialog to be closed by the async call
        await libs.waitForDialog('#modalDeviceTreeMap', 'close');


        btnClick = await page.$("#btnDeviceInfoTreeMap");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalDeviceTreeMap');

        let checked = await page.evaluate(() => $('#checkDeviceTreeMapOther').is(':checked'))
        expect(checked).to.be.false

        checked = await page.evaluate(() => $('#checkDeviceTreeMapFreeSpace').is(':checked'))
        expect(checked).to.be.false
    })
})
