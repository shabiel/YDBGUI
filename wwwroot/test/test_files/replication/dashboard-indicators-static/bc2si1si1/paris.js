/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Dashboard static > bc2si1si1 > Paris", async () => {
    it("Test # 3028: verify repl pill, no backlog", async () => {
        await page.goto(`https://localhost:${browserPorts.PARIS}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(500)

        // Pill has the GREEN color
        let pillColor = await libs.getCssBackground('#lblDashStatusReplication');
        expect(pillColor).to.have.string('rgb(100, 165, 85)')
    })

    it("Test # 3029: verify backlog pill, turn backlog off, verify backlog pill is gone", async () => {
        await page.goto(`https://localhost:${browserPorts.PARIS}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // should be visible
        let display = await page.evaluate(() => $('#lblDashStatusReplication2').css('display'))
        expect(display).to.have.string('inline')

        // switch backlog off
        await page.evaluate(() => app.userSettings.defaults.replication.dashboard.refreshBacklog = 'off')
        await page.evaluate(() => app.ui.dashboard.refresh())

        // should be hidden
        display = await page.evaluate(() => $('#spanDashboardStatusReplicationBacklog').css('display'))
        expect(display).to.have.string('none')
    })

    it("Test # 3030: verify instance name", async () => {
        await page.goto(`https://localhost:${browserPorts.PARIS}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // text is correct
        const cell = await page.$('#lblDashStatusReplication');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('Enabled');
    })

    it("Test # 3031: verify popup", async () => {
        await page.goto(`https://localhost:${browserPorts.PARIS}/index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // text is correct
        const popup = await page.evaluate(() => $('#lblDashStatusReplicationPopup').attr('data-content'))

        expect(popup).to.have.string('paris')
        expect(popup).to.have.string('Supplementary')
        expect(popup).to.have.string('Yes')
    })
})
