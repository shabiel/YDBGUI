/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')
const {exec} = require('child_process');

describe("CLIENT: REPL > Dashboard dynamic > bc1 > Melbourne", async () => {
    it("Test # 3100: verify sources backlog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}/index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(500)

        // speed up the refresh to 1 second
        await page.evaluate(() => {
            app.userSettings.defaults.replication.dashboard.refreshBacklog = "1"
            app.ui.dashboard.refreshBacklog.stop()
            app.ui.dashboard.refreshBacklog.start()
        })

        // capture many samples to ensure at least one is > 0
        let backlogCounter = 0
        for (let cnt = 0; cnt < 30; cnt++) {
            await libs.delay(200)

            let backlogData = await page.evaluate(() => app.system.replication.backlog)
            if (backlogData.source.data.streams[0] > backlogCounter) {
                backlogCounter = backlogData.source.data.streams[0]

                break
            }
        }

        expect(backlogCounter > 0).to.be.true
    })

    it("Test # 3101: verify there is no receiver backlog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}/index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(500)

        // speed up the refresh to 1 second
        await page.evaluate(() => {
            app.userSettings.defaults.replication.dashboard.refreshBacklog = "1"
            app.ui.dashboard.refreshBacklog.stop()
            app.ui.dashboard.refreshBacklog.start()
        })

        let backlogData = await page.evaluate(() => app.system.replication.backlog)
        expect(backlogData.receiver === undefined).to.be.true
    })
})
