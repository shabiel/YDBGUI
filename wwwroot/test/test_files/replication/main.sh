#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

exitCode=0
if ! $PWD/wwwroot/test/test_files/replication/dashboard-indicators-dynamic/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/dashboard-indicators-static/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/pull-down-menus/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/topology-static/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/topology-tls/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/topology-static-model-details/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/topology-ui/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/topology-dynamic/main.sh; then
	exitCode=1
fi
exit $exitCode
