#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '****************************'
echo 'Dashboard indicators dynamic'
echo '****************************'
echo
exitCode=0
if ! $PWD/wwwroot/test/test_files/replication/topology-dynamic/bc1/main.sh; then
	exitCode=1
fi
if ! $PWD/wwwroot/test/test_files/replication/topology-dynamic/bc2bc4/main.sh; then
	exitCode=1
fi
exit $exitCode
