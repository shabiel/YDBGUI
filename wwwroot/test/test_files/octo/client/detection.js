/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');
const {env} = require('process')

describe("CLIENT: Octo / Rocto detection", async () => {
    // this helps prevent errors in the job due to ERR:NETWORK:CHANGED returned (for some mysterious reason) by some jobs
    it("Test # 2000: Dummy", async () => {
        try {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=2000`, {
                waitUntil: "domcontentloaded"
            });
        } catch (err) {
        }

    })

    it("Test # 2000: Ensure menus are enabled when octo is detected", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2000`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const dis = await page.evaluate(() => $('#menuDevelopmentOcto').hasClass('disabled'));

        expect(dis).to.be.false
    })

    it("Test # 2001: Ensure menus are disabled when octo is NOT detected", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2001`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const dis = await page.evaluate(() => $('#menuDevelopmentOcto').hasClass('disabled'));

        expect(dis).to.be.true
    })

    it("Test # 2003: Ensure tab menu hasn't Octo enabled when octo is detected", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2003`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const dis = await page.evaluate(() => $('#menuTabsOcto').hasClass('disabled'));

        expect(dis).to.be.true
    })

    it("Test # 2004: No Octo, ensure no frame is displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2004`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#menuSystemInfo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalSystemInfo');


        const dis = await page.evaluate(() => $('#divSystemInfoRocto').css('display'));

        expect(dis === 'none').to.be.true
    })

    it("Test # 2005: Rocto stopped, ensure correct pill text and color are displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2005`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#menuSystemInfo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalSystemInfo');


        const dis = await page.evaluate(() => $('#divSystemInfoRocto').css('display'));
        expect(dis === 'block').to.be.true

        const text = await page.evaluate(() => $('#lblDashStatusRocto').text());
        expect(text === 'Stopped').to.be.true

    })

    it("Test # 2006: Rocto running, ensure correct pill text and color are displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2006`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#menuSystemInfo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalSystemInfo');


        const dis = await page.evaluate(() => $('#divSystemInfoRocto').css('display'));
        expect(dis === 'block').to.be.true

        const text = await page.evaluate(() => $('#lblDashStatusRocto').text());
        expect(text === 'Running').to.be.true

    })

    it("Test # 2007: Rocto running, ensure IP #, port and params are displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=2007`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let btnClick = await page.$("#menuSystemInfo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalSystemInfo');


        const dis = await page.evaluate(() => $('#divSystemInfoRocto').css('display'));
        expect(dis === 'block').to.be.true

        const ip = await page.evaluate(() => $('#lblSystemInfoRoctoIp').text());
        expect(ip === '0.0.0.0').to.be.true

        const port = await page.evaluate(() => $('#lblSystemInfoRoctoPort').text());
        expect(port === '1337').to.be.true

        const params = await page.evaluate(() => $('#lblSystemInfoRoctoParams').text());
        expect(params === '-w').to.be.true
    })

})
