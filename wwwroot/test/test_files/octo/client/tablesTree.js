/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");

describe("CLIENT: Tree", async () => {
    it("Test # 2100: pop the tree, wait and check tables population", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-tables_anchor').text());
            expect(text).to.have.string('Tables')

            let elem = await page.$('#root-tables_anchor');
            await libs.dblClickOnElement(elem)

            text = await page.evaluate(() => $('#table-categories').text());
            expect(text).to.have.string('categories')
        }
    })

    it("Test # 2101: pop the tree, wait and check functions population", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-functions').text());
            expect(text).to.have.string('Functions')

            let elem = await page.$('#root-functions_anchor');
            await libs.dblClickOnElement(elem)

            text = await page.evaluate(() => $('#func-abs0').text());
            expect(text).to.have.string('abs')
        }
    })

    it("Test # 2102: pop the tree, check population, hide it", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-tables').text());
            expect(text).to.have.string('Tables')

            let elem = await page.$('#root-tables_anchor');
            await libs.dblClickOnElement(elem)

            text = await page.evaluate(() => $('#table-categories').text());
            expect(text).to.have.string('categories')

            // hide it
            btnClick = await page.$("#btnOctoTree-O-1");
            await btnClick.click();

            await libs.delay(500)

            const display = await page.evaluate(() => $('#divOctoTables-O-1').css('display'));
            expect(display === 'block').to.be.true
        }
    })

    it("Test # 2103: pop the tree, check population, hide it, show it again, it shouldn't refresh", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-tables').text());
            expect(text).to.have.string('Tables')

            let elem = await page.$('#root-tables_anchor');
            await libs.dblClickOnElement(elem)

            text = await page.evaluate(() => $('#table-categories').text());
            expect(text).to.have.string('categories')

            // hide it
            btnClick = await page.$("#btnOctoTree-O-1");
            await btnClick.click();

            await libs.delay(500)

            const display = await page.evaluate(() => $('#divOctoTables-O-1').css('display'));
            expect(display === 'block').to.be.true

            await page.evaluate(() => app.REST.testExecuted = false);

            // show it again
            btnClick = await page.$("#btnOctoTree-O-1");
            await btnClick.click();

            await libs.delay(500)

            const wasExecuted = await page.evaluate(() => app.REST.testExecuted);
            expect(wasExecuted).to.be.false
        }
    })

    it("Test # 2104: select first table, double click, ensure the node gets populated", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-tables').text());
            expect(text).to.have.string('Tables')

            let elem = await page.$('#root-tables_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#table-categories').text());
            expect(text).to.have.string('categories')

            elem = await page.$('#table-categories_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#table-categories-columns_anchor').text());
            expect(text).to.have.string("Columns")
        }
    })

    it("Test # 2105: select first table, double click, double click again, it shouldn't fetch data again", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-tables').text());
            expect(text).to.have.string('Tables')

            let elem = await page.$('#root-tables_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#table-categories').text());
            expect(text).to.have.string('categories')

            elem = await page.$('#table-categories_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#table-categories-columns_anchor').text());
            expect(text).to.have.string("Columns")

            // close the node
            elem = await page.$('#table-categories_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            // reset the flag
            await page.evaluate(() => app.REST.testExecuted = false);

            // open it again
            elem = await page.$('#table-categories_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(750)

            // and check the flag
            const wasExecuted = await page.evaluate(() => app.REST.testExecuted);
            expect(wasExecuted).to.be.false
        }
    })

    it("Test # 2106: Pop the tree, wait and check views population", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-views_anchor').text());
            expect(text).to.have.string('Views')

            let elem = await page.$('#root-views_anchor');
            await libs.dblClickOnElement(elem)

            text = await page.evaluate(() => $('#view-v1').text());
            expect(text).to.have.string('v1')
        }
    })

    it("Test # 2107: Select first view, double click, ensure it gets populated", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-views').text());
            expect(text).to.have.string('Views')

            let elem = await page.$('#root-views_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#view-v1').text());
            expect(text).to.have.string('v1')

            elem = await page.$('#view-v1_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#view-v1-columns_anchor').text());
            expect(text).to.have.string("Columns")
        }
    })

    it("Test # 2108: Select first view, double click, double click again, it shouldn't fetch again", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            let text = await page.evaluate(() => $('#root-tables').text());
            expect(text).to.have.string('Tables')

            let elem = await page.$('#root-views_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#view-v1').text());
            expect(text).to.have.string('v1')

            elem = await page.$('#view-v1_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            text = await page.evaluate(() => $('#view-v1-columns_anchor').text());
            expect(text).to.have.string("Columns")

            // close the node
            elem = await page.$('#view-v1_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(500)

            // reset the flag
            await page.evaluate(() => app.REST.testExecuted = false);

            // open it again
            elem = await page.$('#view-v1_anchor');
            await libs.dblClickOnElement(elem)

            await libs.delay(750)

            // and check the flag
            const wasExecuted = await page.evaluate(() => app.REST.testExecuted);
            expect(wasExecuted).to.be.false
        }
    })
})
