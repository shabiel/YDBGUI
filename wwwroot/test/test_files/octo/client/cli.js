/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");

describe("CLIENT: CLI view", async () => {
    it("Test # 2125: generate an error with select, verify error gets colored", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('select * from idontexist');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const isActive = await page.evaluate(() => $('#divOctoText-O-1').hasClass('active'));
            expect(isActive).to.be.true

            let text = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(text).to.have.string('ERR_UNKNOWN_TABLE')
        }
    })

    it("Test # 2126: generate an error with \\d idontexist, verify error is returned", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d idontexist');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(200)

            const isActive = await page.evaluate(() => $('#divOctoText-O-1').hasClass('active'));
            expect(isActive).to.be.true

            let text = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(text).to.have.string('ERR_UNKNOWN_TABLE')
        }
    })

    it("Test # 2127: enter \\d suppliers, verify response", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d suppliers');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const isActive = await page.evaluate(() => $('#divOctoText-O-1').hasClass('active'));
            expect(isActive).to.be.true

            let text = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(text).to.have.string('Table "suppliers" stored in Global')
        }
    })
})
