/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("SERVER: Traverse globals >>> Records validation", async () => {
    it("Test # 5551: Ask for ^PSNDF, no subscripts, 10 entries", async () => {
        let body = {
            namespace: {
                global: '^PSNDF',
                subscripts: [
                    {
                        type: '*'
                    }
                ]
            },
            size: 10
        };
        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check  the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 10).to.be.true;
        expect(res.data.list.length === 10).to.be.true;

    });

    it("Test # 5552: Ask for ^PSNDF, no subscripts, 1000 entries", async () => {
        let body = {
            namespace: {
                global: '^PSNDF',
                subscripts: [
                    {
                        type: '*'
                    }
                ]
            },
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 1000).to.be.true;
        expect(res.data.list.length === 1000).to.be.true;

    });

    it("Test # 5553: Ask for ^PSNDF, no subscripts, 10000 entries", async () => {
        let body = {
            namespace: {
                global: '^PSNDF',
                subscripts: [
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 10000).to.be.true;
        expect(res.data.list.length === 10000).to.be.true;

    });
});

describe("SERVER: Traverse globals >>> Commas and star after global: regular", async () => {
    it("Test # 5570: ^ORD(,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    }
                ]
            },
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;

    });

    it("Test # 5571: ^ORD(,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 1).to.be.true;

    });

    it("Test # 5572: ^ORD(,,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        expect(res.data.recordCount === 48).to.be.true;

    });

    it("Test # 5573: ^ORD(*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: '*'
                    },
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 201).to.be.true;

    });

    it("Test # 5574: ^ORD(,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 201).to.be.true;

    });

    it("Test # 5575: ^ORD(,,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        expect(res.data.recordCount === 200).to.be.true;

    });

});

describe("SERVER: Traverse globals >>> Commas and star after global: All records", async () => {
    it("Test # 5580: ^ORD(,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    }
                ]
            },
            returnAll: true,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 1).to.be.true;

    });

    it("Test # 5581: ^ORD(,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 10).to.be.true;

    });

    it("Test # 5582: ^ORD(,,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        expect(res.data.recordCount === 128).to.be.true;

    });

    it("Test # 5583: ^ORD(*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        expect(res.data.recordCount === 406).to.be.true;

    });

    it("Test # 5584: ^ORD(,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 404).to.be.true;

    });

    it("Test # 5585: ^ORD(,,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 384).to.be.true;

    });

});

describe("SERVER: Traverse globals >>> Commas and start after 1st subscript: Regular", async () => {
    it("Test # 5590: ^ORD(100.01)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                ]
            },
            returnAll: false,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5591: ^ORD(100.01,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        expect(res.data.recordCount === 1).to.be.true;
    });

    it("Test # 5592: ^ORD(100.01,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        expect(res.data.recordCount === 48).to.be.true;
    });

    it("Test # 5593: ^ORD(100.01,,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 64).to.be.true;
    });

    it("Test # 5594: ^ORD(100.01,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: false,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 201).to.be.true;
    });

    it("Test # 5595: ^ORD(100.01,,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: false,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 200).to.be.true;
    });

    it("Test # 5596: ^ORD(100.01,,,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: false,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 152).to.be.true;
    });

});

describe("SERVER: Traverse globals >>> Commas and start after 1st subscript: All records", async () => {
    it("Test # 5600: ^ORD(100.01)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                ]
            },
            returnAll: true,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 1).to.be.true;
    });

    it("Test # 5601: ^ORD(100.01,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 10).to.be.true;
    });

    it("Test # 5602: ^ORD(100.01,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 128).to.be.true;
    });

    it("Test # 5603: ^ORD(100.01,,,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 152).to.be.true;
    });

    it("Test # 5604: ^ORD(100.01,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 404).to.be.true;
    });

    it("Test # 5605: ^ORD(100.01,,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 384).to.be.true;
    });

    it("Test # 5606: ^ORD(100.01,,,*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 256).to.be.true;
    });

});

describe("SERVER: Traverse globals >>>  2 subscripts, numeric and string: Regular", async () => {
    it("Test # 5610: ^ORD(100.01,\"B\")", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    }
                ]
            },
            returnAll: false,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5611: ^ORD(100.01,\"B\",)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5612: ^ORD(100.01,\"B\",,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5613: ^ORD(100.01,\"B\",*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: false,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5614: ^ORD(100.01,\"AMASTERVUID\")", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    }
                ]
            },
            returnAll: false,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5615: ^ORD(100.01,\"AMASTERVUID\",)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5616: ^ORD(100.01,\"AMASTERVUID\",,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: false,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5617: ^ORD(100.01,\"AMASTERVUID\",*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: false,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

});

describe("SERVER: Traverse globals >>>  2 subscripts, numeric and string: All records", async () => {
    it("Test # 5620: ^ORD(100.01,\"B\")", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    }
                ]
            },
            returnAll: true,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 1).to.be.true;
    });

    it("Test # 5621: ^ORD(100.01,\"B\",)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5622: ^ORD(100.01,\"B\",,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5623: ^ORD(100.01,\"B\",*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"B"'
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5624: ^ORD(100.01,\"AMASTERVUID\")", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    }
                ]
            },
            returnAll: true,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 1).to.be.true;
    });

    it("Test # 5625: ^ORD(100.01,\"AMASTERVUID\",)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5626: ^ORD(100.01,\"AMASTERVUID\",,)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    },
                    {
                        type: ','
                    },
                    {
                        type: ','
                    },
                ]
            },
            returnAll: true,
            size: 1000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });

    it("Test # 5627: ^ORD(100.01,\"AMASTERVUID\",*)", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: 'V',
                        value: '"AMASTERVUID"'
                    },
                    {
                        type: '*'
                    },
                ]
            },
            returnAll: true,
            size: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.recordCount === 0).to.be.true;
    });
});
