/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("SERVER: get globals size", async () => {
    it("Test # 6120: execute globals/size with no region, verify that error is returned", async () => {
        // execute the call
        let res = await libs._REST('globals/size').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'ERROR').to.be.true;

        // and verify that the fields are there
        expect(res.error.description).to.have.string('You need to supply a region')
    });

    it("Test # 6121: execute globals/size with no type, verify that error is returned", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'ERROR').to.be.true;

        // and verify that the fields are there
        expect(res.error.description).to.have.string('You need to supply a type')
    });

    it("Test # 6122: execute globals/size with no param, verify that error is returned", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=scan').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'ERROR').to.be.true;

        // and verify that the fields are there
        expect(res.error.description).to.have.string('You need to supply a param value')
    });

    it("Test # 6123: execute globals/size with arsample, 1000, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=arsample&param=1000').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('Total')
    });

    it("Test # 6124: execute globals/size with arsample, 10000, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=arsample&param=10000').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('Total')
    });

    it("Test # 6125: execute globals/size with arsample, 10, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=arsample&param=10').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('Total')
    });

    it("Test # 6126: execute globals/size with impsample, 1000, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=impsample&param=1000').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('Total')
    });

    it("Test # 6127: execute globals/size with impsample, 10000, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=impsample&param=10000').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('Total')
    });

    it("Test # 6128: execute globals/size with impsample, 10, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=impsample&param=10').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('Total')
    });

    it("Test # 6129: execute globals/size with scan, 0, DEFAULT, verify response", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            let res = await libs._REST('globals/size?region=DEFAULT&type=scan&param=0').catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            expect(res.data.dump[res.data.dump.length - 1]).to.have.string('1  ')
        }
    });

    it("Test # 6130: execute globals/size with scan, -1, DEFAULT, verify response", async () => {
        if (global.serverMode === 'RO') {
            // execute the call
            let res = await libs._REST('globals/size?region=DEFAULT&type=scan&param=-1').catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            expect(res.data.dump[res.data.dump.length - 1]).to.have.string('1 ')
        }
    });

    it("Test # 6131: execute globals/size with scan, 1, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=scan&param=1').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.dump[res.data.dump.length - 1]).to.have.string('1  ')
    });

    it("Test # 6132: execute globals/size with scan, 2, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=scan&param=2').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'ERROR').to.be.true;

        expect(res.error.dump[res.error.dump.length - 1]).to.have.string('MUPIP unable')
    });

    it("Test # 6133: execute globals/size with scan, 3, DEFAULT, verify response", async () => {
        // execute the call
        let res = await libs._REST('globals/size?region=DEFAULT&type=scan&param=3').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'ERROR').to.be.true;

        expect(res.error.dump[res.error.dump.length - 1]).to.have.string('MUPIP unable')
    });

})

describe("SERVER: get globals by region", async () => {
    it("Test # 6150: execute globals/by-region/{region} on DEFAULT and verify that there are globals", async () => {
        // execute the call
        let res = await libs._REST('globals/by-region/DEFAULT').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        // and verify that the fields are there
        let val = res.data[0].name
        expect(val === '^%').to.be.true;

        // and verify that the fields are there
        val = res.data[0].status
        expect(val === 'ok').to.be.true;
    });

    it("Test # 6151: execute globals/by-region/{region} on YDBOCTO and verify that there are NO globals", async () => {
        // execute the call
        let res = await libs._REST('globals/by-region/YDBOCTO').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and verify that the fields are there
        expect(res.data === undefined).to.be.false;
    });

    it("Test # 6152: generate a zombie, execute globals/by-region/{region} on DEFAULT and verify that there are zombies", async () => {
        // execute the call
        let res = await libs._REST('globals/by-region/DEFAULT').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
        // and verify that the fields are there
        expect(res.data[2].status).to.have.string('zombie')
    });

    it("Test # 6153: execute globals/by-region/{region} on an invalid region, verify that error is returned", async () => {
        // execute the call
        let res = await libs._REST('globals/by-region/NOEXISTS').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'ERROR').to.be.true;

        // and verify that the fields are there
        expect(res.error.description).to.have.string('The region NOEXISTS doesn\'t exist')
    });
})

