/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("SERVER: routines/find", async () => {
    it("Test # 5900: submit with no mask, expect error", async () => {

        // create routine
        execSync('cd /YDBGUI/routines && echo " new a\n" > _ydb.m');

        // execute the call
        const res = await libs._RESTpost('routines/find').catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5901: Submit with mask=a, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: 'a'}).catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5902: Submit with mask=a*, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: 'a*'}).catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5903: Submit with mask=%*, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: '%*'}).catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5904: Submit with mask=%?, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: '%?'}).catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5905: Submit with mask=a?, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: 'a?'}).catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5906: Submit with mask=%ydb*, expect ok", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: '_yd*'}).catch(() => {});

        expect(res.result === 'OK').to.be.true;
    });

    it("Test # 5907: Submit with mask=%ydb*, verify payload", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/find', {mask: '_yd*'}).catch(() => {});

        expect(res.result === 'OK').to.be.true;

        expect(res.data[0].location === '/YDBGUI/routines/').to.be.true;
    });
});

describe("SERVER: routines/{routine}}", async () => {
    it("Test # 5925: submit no routine, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/').catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5926: submit bad routine, expect error", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/', {noroutine: 'noroutine'}).catch(() => {});

        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5927: submit good routine, expect ok", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/', {routine: '_ydb'}).catch(() => {});

        expect(res.result === 'OK').to.be.true;
    });

    it("Test # 5928: submit good routine with no object, verify payload", async () => {
        // execute the call
        const res = await libs._RESTpost('routines/', {routine: '_ydb'}).catch(() => {});

        expect(res.result === 'OK').to.be.true;
        expect(res.data.props.size === 8).to.be.true;
        expect(res.data.file[0] === ' new a').to.be.true;
    });


    it("Test # 5929: submit good routine, verify payload", async () => {
        // compile the routine
        execSync('$ydb_dist/yottadb -run %ydb');

        // execute the call
        const res = await libs._RESTpost('routines/', {routine: '_ydb'}).catch(() => {});

        expect(res.result === 'OK').to.be.true;
        expect(res.data.object.props.accessRightsOctal === 33188).to.be.true;
        expect(res.data.object.props.filename === '_ydb.o').to.be.true;
    });
});
