/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("SERVER: Maintenance: REORG", async () => {
    it("Test # 6000: call /api/regions/maintenance/defrag with no body", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', {}).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The parameter \'fillFactor\' is missing or empty').to.be.true
        }
    })

    it("Test # 6001: call /api/regions/maintenance/defrag with no fillFactor", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                indexFillFactor: 100,
                regions: [
                    'DEFAULT'
                ]
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The parameter \'fillFactor\' is missing or empty').to.be.true
        }
    })

    it("Test # 6002: call /api/regions/maintenance/defrag with no indexFillFactor", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                fillFactor: 100,
                regions: [
                    'DEFAULT'
                ]
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The parameter \'indexFillFactor\' is missing or empty').to.be.true
        }
    })

    it("Test # 6003: call /api/regions/maintenance/defrag with no regions", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                fillFactor: 100,
                indexFillFactor: 100
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The array \'regions\' is either missing, empty or the wrong data type').to.be.true
        }
    })

    it("Test # 6005: call /api/regions/maintenance/defrag with valid body and YDBAIM and YDBJNLF, should return no globals found on both", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                fillFactor: 100,
                indexFillFactor: 100,
                regions: [
                    'YDBAIM',
                    'YDBJNLF'
                ]
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'OK').to.be.true;
            expect(res.data === 'noGlobals').to.be.true
        }
    })

    it("Test # 6006: create globals on DEFAULT, call /api/regions/maintenance/defrag with valid body and DEFAULT and YDBJNLF, should return no globals found on YDBOCTO and valid data on 'DEFAULT'", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                fillFactor: 100,
                indexFillFactor: 100,
                regions: [
                    'DEFAULT',
                    'YDBJNLF'
                ]
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'OK').to.be.true;
            expect(res.data.YDBOCTO === undefined).to.be.true
            expect(res.data.DEFAULT.byGlobal !== undefined).to.be.true
            expect(res.data.DEFAULT.totals !== undefined).to.be.true
        }
    })

    it("Test # 6007: call /api/regions/maintenance/defrag with valid body and DEFAULT, verify that globals list is returned as array in response payloadRegions", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                fillFactor: 100,
                indexFillFactor: 100,
                regions: [
                    'DEFAULT'
                ]
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/defrag', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'OK').to.be.true;
            expect(res.data.DEFAULT.byGlobal !== undefined).to.be.true
            const count = Object.keys(res.data.DEFAULT.byGlobal).length
            expect(count > 0).to.be.true
        }
    })

    it("Test # 6008: call /api/regions/maintenance/defrag in RO mode, expect HTTP 403", async () => {
        if (global.serverMode === 'RO') {
            res = await libs._RESTpost('regions/maintenance/defrag').catch(() => {});
            expect(res.error.code === 403)
        }
    })
})

describe("SERVER: Maintenance: INTEG", async () => {
    it("Test # 6025: call /api/regions/maintenance/integ with no body", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', {}).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The array \'regions\' is either missing, empty or the wrong data type').to.be.true
        }
    })

    it("Test # 6026: call /api/regions/maintenance/integ with no regions", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: []
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The array \'regions\' is either missing, empty or the wrong data type').to.be.true
        }
    })

    it("Test # 6027: call /api/regions/maintenance/integ with no bad type", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                type: 'bad'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'type\' must be either \'complete\' or \'fast\'').to.be.true
        }
    })

    it("Test # 6028: call /api/regions/maintenance/integ with no bad reporting", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                reporting: 'bad'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'reporting\' must be either \'summary\' or \'full\'').to.be.true
        }
    })

    it("Test # 6029: call /api/regions/maintenance/integ with keyRange different than boolean", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                keyRange: 'bad'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'keyRange\' must be either true or false').to.be.true
        }
    })

    it("Test # 6030: call /api/regions/maintenance/integ with stat different than boolean", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                stat: 'bad'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'stat\' must be either true or false').to.be.true
        }
    })

    it("Test # 6031: call /api/regions/maintenance/integ with mapErrors with negative number", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                mapErrors: -1
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'mapErrors\' must be a numeric value between 0 and 1,000,000').to.be.true
        }
    })

    it("Test # 6032: call /api/regions/maintenance/integ with mapErrors with number > 1,000,000", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                mapErrors: 1000001
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'mapErrors\' must be a numeric value between 0 and 1,000,000').to.be.true
        }
    })

    it("Test # 6033: call /api/regions/maintenance/integ with keySizeErrors with negative number", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                keySizeErrors: -1
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'keySizeErrors\' must be a numeric value between 0 and 1,000,000').to.be.true
        }
    })

    it("Test # 6034: call /api/regions/maintenance/integ with keySizeErrors with number > 1,000,000", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                keySizeErrors: 1000001
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'keySizeErrors\' must be a numeric value between 0 and 1,000,000').to.be.true
        }
    })

    it("Test # 6035: call /api/regions/maintenance/integ with transactionErrors with negative number", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                transactionErrors: -1
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'transactionErrors\' must be a numeric value between 0 and 1,000,000').to.be.true
        }
    })

    it("Test # 6036: call /api/regions/maintenance/integ with transactionErrors with number > 1,000,000", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                transactionErrors: 1000001
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'transactionErrors\' must be a numeric value between 0 and 1,000,000').to.be.true
        }
    })

    it("Test # 6037: call /api/regions/maintenance/integ with reporting = full, verify", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                reporting: 'full'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'OK').to.be.true;
            expect(res.data.dump.length > 5).to.be.true
        }
    })

    it("Test # 6038: call /api/regions/maintenance/integ with reporting = summary, verify", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                reporting: 'summary'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/integ', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'OK').to.be.true;
            expect(res.data.dump.length > 5).to.be.true
        }
    })

    it("Test # 6039: call /api/regions/maintenance/integ in RO mode, expect HTTP 403", async () => {
        if (global.serverMode === 'RO') {
            res = await libs._RESTpost('regions/maintenance/integ').catch(() => {});
            expect(res.error.code === 403)
        }
    })
})

describe("SERVER: Maintenance: BACKUP", async () => {
    it("Test # 6050: call /api/regions/maintenance/backup with no body", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', {}).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The array \'regions\' is either missing, empty or the wrong data type').to.be.true
        }
    })

    it("Test # 6051: call /api/regions/maintenance/backup with no regions", async () => {
        if (global.serverMode === 'RW') {
            const body = {}

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The array \'regions\' is either missing, empty or the wrong data type').to.be.true
        }
    })

    it("Test # 6052: call /api/regions/maintenance/backup with no targetPath", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default']
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'targetPath\' is empty').to.be.true
        }
    })

    it("Test # 6053: call /api/regions/maintenance/backup with no replace", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'replace\' must be either true or false').to.be.true
        }
    })

    it("Test # 6054: call /api/regions/maintenance/backup with no disableJournaling", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'disableJournaling\' must be either true or false').to.be.true
        }
    })

    it("Test # 6055: call /api/regions/maintenance/backup with no record", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'record\' must be either true or false').to.be.true
        }
    })

    it("Test # 6056: call /api/regions/maintenance/backup with no createNewJournalFiles", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false,
                record: false
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'createNewJournalFiles\' must be either link, nolink or no').to.be.true
        }
    })

    it("Test # 6057: call /api/regions/maintenance/backup with createNewJournalFiles = 'link'", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false,
                record: false,
                createNewJournalFiles: 'link'
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'syncIo\' must be either true or false').to.be.true
        }
    })

    it("Test # 6058: call /api/regions/maintenance/backup with no includeReplicatedInstances", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false,
                record: false,
                createNewJournalFiles: 'link',
                syncIo: false
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'includeReplicatedInstances\' must be either true or false').to.be.true
        }
    })

    it("Test # 6059: call /api/regions/maintenance/backup with includeReplicatedInstances = true", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false,
                record: false,
                createNewJournalFiles: 'link',
                syncIo: false,
                includeReplicatedInstances: true
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'replUseSamePath\' is empty').to.be.true
        }
    })

    it("Test # 6060: call /api/regions/maintenance/backup with includeReplicatedInstances = true, replUseSamePath = false", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false,
                record: false,
                createNewJournalFiles: 'link',
                syncIo: false,
                includeReplicatedInstances: true,
                replUseSamePath: false
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'ERROR').to.be.true;
            expect(res.error.description === 'The field \'replTargetPath\' is empty').to.be.true
        }
    })

    it("Test # 6061: call /api/regions/maintenance/backup in RO mode, expect HTTP 403", async () => {
        if (global.serverMode === 'RO') {
            res = await libs._RESTpost('regions/maintenance/backup').catch(() => {});
            expect(res.error.code === 403)
        }
    })

    it("Test # 6062: perform a backup and confirm response", async () => {
        if (global.serverMode === 'RW') {
            const body = {
                regions: ['default'],
                targetPath: '/',
                replace: true,
                disableJournaling: false,
                record: false,
                createNewJournalFiles: 'link',
                syncIo: false,
                includeReplicatedInstances: false
            }

            // execute the call
            let res = await libs._RESTpost('regions/maintenance/backup', body).catch(() => {});

            // and check the result to be error
            expect(res.result === 'OK').to.be.true;
            expect(res.data.length > 3).to.be.true
        }
    })

})
