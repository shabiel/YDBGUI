#!/bin/sh

#################################################################
#								                                                #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						                              #
#								                                                #
#	This source code contains the intellectual property	          #
#	of its copyright holder(s), and is made available	            #
#	under a license.  If you do not know the terms of	            #
#	the license, please stop and do not read further.	            #
#								                                                #
#################################################################

set -eu

if ! [ $# = 1 ]; then
	echo "usage: $0 <filename>"
	exit 2
fi

file="$1"

# Don't require deleted files to have a copyright
if ! [ -e "$file" ]; then
       exit 1
fi

skipextensions="ref png zwr html ci gif eot woff woff2 ico svg ttf ysr"	# List of extensions that cannot have copyrights.
	# .png .ico .svg gif-> these are images (i.e. binary files) used in the documentation.
	#		Same reason as .rst for not requiring a copyright.
	# .ref  -> reference files used by the test cases (e.g. tests/outref/T0001.ref).
	#		Those have a fixed format and should not go through copyright changes.
	# .zwr  -> zwrite format extract file (does not currently allow a comment character).
	# .html -> there are a couple of files currently under doc/templates which don't need copyrights.
	# .ci   -> e.g. calltab.ci stores the call-in table which does not currently have a provision for comment characters.
	# .eot .woff .woff2 .ttf are font files
	# .ysr -> YottaDB Statistics Report (used in test)
if echo "$skipextensions" | grep -q -w "$(echo "$file" | awk -F . '{print $NF}')"; then
	exit 1
fi

# Determines whether a file should need a copyright by its name
# Returns 0 if it needs a copyright and 1 otherwise.
skiplist="COPYING
    .gitignore
    wwwroot/test/users.json
    _ydbgui.manifest.json
    wwwroot/test/ydbgui-range-settings.json
    wwwroot/libs/bootstrap-icons.1.11.3/bootstrap-icons.css
    wwwroot/libs/bootstrap.4.6.2/default.min.css
    wwwroot/libs/bootstrap.4.6.2/bootstrap.bundle.min.js
    wwwroot/libs/jquery.3.7.1/jquery.min.js
    wwwroot/libs/jstree-3.3.15/jstree.min.js
    wwwroot/libs/jstree-3.3.15/themes/proton/style.css
    wwwroot/libs/jquery-ui-1.13.2/jquery-ui.min.css
    wwwroot/libs/jquery-ui-1.13.2/jquery-ui.min.js
    wwwroot/libs/code-mirror.5.65.9/codemirror.js
    wwwroot/libs/code-mirror.5.65.9/mumps.js
    wwwroot/libs/code-mirror.5.65.9/sql.js
    wwwroot/libs/code-mirror.5.65.9/codemirror.css
    wwwroot/libs/code-mirror.5.65.9/addons/annotatescrollbar.js
    wwwroot/libs/code-mirror.5.65.9/addons/dialog.css
    wwwroot/libs/code-mirror.5.65.9/addons/dialog.js
    wwwroot/libs/code-mirror.5.65.9/addons/jump-to-line.js
    wwwroot/libs/code-mirror.5.65.9/addons/matchesonscrollbar.css
    wwwroot/libs/code-mirror.5.65.9/addons/matchesonscrollbar.js
    wwwroot/libs/code-mirror.5.65.9/addons/search.js
    wwwroot/libs/code-mirror.5.65.9/addons/searchcursor.js
    wwwroot/libs/code-mirror.5.65.9/theme/night.css
    wwwroot/libs/code-mirror.5.65.9/addons/show-hint.css
    wwwroot/libs/code-mirror.5.65.9/addons/sql-hint.js
    wwwroot/libs/code-mirror.5.65.9/addons/show-hint.js
    wwwroot/test/filesHistory.csv
    wwwroot/libs/node/ws.7.4.6/LICENSE
    wwwroot/libs/node/ws.7.4.6/README.md
    wwwroot/libs/node/ws.7.4.6/browser.js
    wwwroot/libs/node/ws.7.4.6/index.js
    wwwroot/libs/node/ws.7.4.6/lib/buffer-util.js
    wwwroot/libs/node/ws.7.4.6/lib/constants.js
    wwwroot/libs/node/ws.7.4.6/lib/event-target.js
    wwwroot/libs/node/ws.7.4.6/lib/extension.js
    wwwroot/libs/node/ws.7.4.6/lib/limiter.js
    wwwroot/libs/node/ws.7.4.6/lib/permessage-deflate.js
    wwwroot/libs/node/ws.7.4.6/lib/receiver.js
    wwwroot/libs/node/ws.7.4.6/lib/sender.js
    wwwroot/libs/node/ws.7.4.6/lib/stream.js
    wwwroot/libs/node/ws.7.4.6/lib/validation.js
    wwwroot/libs/node/ws.7.4.6/lib/websocket-server.js
    wwwroot/libs/node/ws.7.4.6/lib/websocket.js
    wwwroot/libs/node/ws.7.4.6/package.json
    wwwroot/libs/apex-charts.3.41.0/apexcharts.css
    wwwroot/libs/apex-charts.3.41.0/apexcharts.min.js
    wwwroot/libs/showdown.2.1.0/showdown.min.js
    wwwroot/libs/chartjs.4.4.1/chart.min.js
    wwwroot/libs/luxon.3.4.4/luxon.min.js
    wwwroot/libs/chartjs-adapter-luxon.1.3.1/chartjs-adapter-luxon.min.js
    wwwroot/libs/jointjs.4.0.1/joint.js
    wwwroot/libs/dagrejs.1.0.4/dagre.min.js
    wwwroot/libs/graphlibjs.2.1.13/graphlib.min.js
    wwwroot/libs/jointjs.layoutDirectedGraph.4.0.3/jointjs.layer-directed-graph.js
    replication/last_profile.dat
    package.json"
    for skipfile in $skiplist; do
	if [ "$file" = "$skipfile" ]; then
		exit 1
	fi
done
