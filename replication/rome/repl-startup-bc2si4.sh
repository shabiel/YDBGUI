#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

cp /YDBGUI/certs/ydbgui.pem /repl/backups/rome.ydbgui.pem

$ydb_dist/mupip set -replication=on -region "*"
# sleep 3 is needed or replication will error
sleep 3

date=`date +%Y%m%d:%H:%M:%S`
$ydb_dist/mupip replicate -source -start -passive  -log=/tmp/dummy"$date".log -instsecondary=dummy -updok
sleep 1
echo '*********************'
echo 'Source checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -source -checkhealth

$ydb_dist/mupip replicate -receive -start -listenport=3003 -log=/tmp/rome"$date".log -updateresync=/repl/backups/paris.repl -initialize
sleep 1
echo '*********************'
echo 'Receive checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -receive -checkhealth

echo '*********************'
echo 'Dummy log'
echo '*********************'
tail -30 /tmp/dummy"$date".log

echo '*********************'
echo 'rome log'
echo '*********************'
tail -30 /tmp/rome"$date".log

# after startup, copy the new repl-startup with different parameters, as initialization is not needed anymore
cp /repl/repl-startup.sh.after /repl/repl-startup.sh

